/*
 * manager.cpp
 *
 *  Created on: Mar 13, 2014
 *      Author: root
 */

#include "manager.h"
#include <sstream>
#include <boost/filesystem.hpp>
// C includes (Aculab headers)
#include <res_lib.h>

using namespace std;
using namespace faxd;
using namespace log4cxx;
using namespace log4cxx::xml;
using namespace log4cxx::helpers;
using namespace boost::filesystem;

ACU_OS_ERR acu_os_wait_for_wait_object2(ACU_WAIT_OBJECT* wait_object, int timeout);

Manager::Manager(Config &cfg) :
		FSM<ManagerStates, ManagerEvents, Manager>(STATE_MGR_NEW),
		cfg_(cfg),
		logger(log4cxx::Logger::getLogger("main")),		// internal logger initialization
		logHeader_("MANAGER: "),
		outQueue(cfg_, cfg::outQueue),
		workerExit_(false)
{
	std::stringstream ss;
	ss << "current config: " << cfg::toString();
	LOG4CXX_INFO(logger, ss.str());

	/*
	 * prepare all directories
	 */
	// Input
	if ( !boost::filesystem::exists( cfg::inDir ) ) {
		boost::filesystem::path dir( cfg::inDir );
		if (boost::filesystem::create_directory(dir)) {
			LOG4CXX_DEBUG (logger, "Input FAX Directory " << cfg::inDir << " CREATED");
		} else {
			LOG4CXX_ERROR (logger, "Input FAX Directory " << cfg::inDir << " COULDN'T BE CREATED");
			exit(1);
		}
	}
	// Output
	if ( !boost::filesystem::exists( cfg::outDir ) ) {
		boost::filesystem::path dir( cfg::outDir );
		if (boost::filesystem::create_directory(dir)) {
			LOG4CXX_DEBUG (logger, "Output FAX Directory " << cfg::outDir << " CREATED");
		} else {
			LOG4CXX_ERROR (logger, "Output FAX Directory " << cfg::outDir << " COULDN'T BE CREATED");
			exit(1);
		}
	}
	// Temporary
	if ( !boost::filesystem::exists( cfg::tmpDir ) ) {
		boost::filesystem::path dir( cfg::tmpDir );
		if (boost::filesystem::create_directory(dir)) {
			LOG4CXX_DEBUG (logger, logHeader_ << "Temporary FAX Directory " << cfg::tmpDir << " CREATED");
		} else {
			LOG4CXX_ERROR (logger, logHeader_ << "Temporary FAX Directory " << cfg::tmpDir << " COULDN'T BE CREATED");
			exit(1);
		}
	}

	// fax out queue property
	LOG4CXX_INFO(logger, outQueue.toString());

	/*
	 * Get Aculab Prosody system snapshot
	 */
	if( false == getAcuSnapshot() ) {
		LOG4CXX_ERROR(logger, logHeader_ << "CAN'T GET ACULAB SNAPSHOT. EXITTING");
		exit(1);
	}


	/*
	 * stuffing transition table. Look fsm.h
	 */
	addTransition(STATE_MGR_NEW,			EVENT_MGR_START,	STATE_MGR_START,	&Manager::actionRunWorker);
	addTransition(STATE_MGR_START,			EVENT_MGR_EXIT,		STATE_MGR_EXIT,		&Manager::actionSetExit);
	addTransition(STATE_MGR_START,			EVENT_MGR_EXIT,		STATE_MGR_EXIT,		&Manager::actionWaitWorkerExit);


	/*
	 * start manager worker thread
	 */
	proccessEvent(EVENT_MGR_START);
	LOG4CXX_DEBUG (logger, logHeader_ << "initialization FINISHED");
}

Manager::~Manager() {
	/*
	 * cleanup Manager resources
	 */
	proccessEvent(EVENT_MGR_EXIT);
	LOG4CXX_DEBUG(logger, logHeader_ << " ~Manager()");
}


void Manager::actionRunWorker() {
	LOG4CXX_DEBUG (logger, logHeader_ << "FSM actionRunWorker() -> START thread worker()");
	spWorkerThread.reset(new boost::thread(boost::bind(&Manager::worker, this)));
}


void Manager::actionSetExit() {
	workerExit_ = true;
	LOG4CXX_DEBUG (logger, logHeader_ << "FSM actionSetExit() - signal worker() to STOP");
};


void Manager::actionWaitWorkerExit() {
	spWorkerThread->join();
	LOG4CXX_DEBUG (logger, logHeader_ << "FSM actionWaitWorkerExit() - worker() TERMINATED");
}

void Manager::worker() {
	ACU_INT		rc;

	/*
	 * Create Event Queue
	 */
	ACU_ALLOC_EVENT_QUEUE_PARMS		rAllocEventQueue;
	INIT_ACU_STRUCT(&rAllocEventQueue);
	rc = acu_allocate_event_queue(&rAllocEventQueue);	// get new event queue
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "WRK acu_allocate_event_queue() " << acu_error_desc(rc));
	} else {
		LOG4CXX_INFO (logger, logHeader_ << "acu_allocate_event_queue() OK");
	}

	/*
	 * Attach Queue to card
	 */
	ACU_QUEUE_PARMS					systemQueueParms;
	INIT_ACU_CL_STRUCT(&systemQueueParms);
	systemQueueParms.queue_id		= rAllocEventQueue.queue_id;
	rc = acu_set_system_notification_queue(&systemQueueParms);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "acu_set_system_notification_queue() " << acu_error_desc(rc));
	} else {
		LOG4CXX_INFO (logger, logHeader_ << "acu_set_system_notification_queue() OK");
	}


	/*
	 * get wait object for whole aculab system
	 */
	ACU_GET_SYS_WAIT_OBJECT_PARMS woParms;
	INIT_ACU_STRUCT(&woParms);
	rc = acu_get_system_notification_wait_object(&woParms);
	if (rc){
		LOG4CXX_ERROR (logger, logHeader_ << "acu_get_system_notification_wait_object() ERROR " << acu_error_desc(rc));
	} else {
		LOG4CXX_DEBUG (logger, logHeader_ << "acu_get_system_notification_wait_object() OK ");
	}

	/*
	 * main loop circle
	 */
	ACU_SYSTEM_NOTIFICATION_PARMS sysEventParms;
	while(!workerExit_) {

		rc = acu_os_wait_for_wait_object2(&woParms.wait_object, 100);
		if ( rc == ERR_ACU_OS_NO_ERROR ) {	// read some event without error or timeout
			/*
			 * Read event from queue.
			 * For each signal from wait object we ca get few events from event queue.
			 * Last event will be ACU_SYS_EVT_NO_EVENT
			 */
			ACU_INT eventCode = ACU_SYS_EVT_NO_EVENT;
			do {

				INIT_ACU_STRUCT(&sysEventParms);
				rc = acu_get_system_notification(&sysEventParms);
				if(rc) {
					LOG4CXX_ERROR (logger, logHeader_ << "acu_get_system_notification() " << acu_error_desc(rc));
					continue;
				}
				eventCode = sysEventParms.event;

				// process events
				switch( eventCode ) {
				case ACU_SYS_EVT_CARD_ADDED:
					LOG4CXX_INFO (logger, logHeader_ << "RX <- ACU_SYS_EVT_CARD_ADDED s/n:" << sysEventParms.data.insert_data.serial_no);
					sleep(5);

					////getAcuSnapshot();
					// create new card and add it to map
					if( cfg::cardsPorts.find(sysEventParms.data.insert_data.serial_no) != cfg::cardsPorts.end() ) {		// if serial number of the founded card are in config file
						LOG4CXX_INFO (logger, logHeader_ << "CARD s/n:" << sysEventParms.data.insert_data.serial_no << " FOUNDED in config");
						boost::shared_ptr<Card> spCard( new Card(sysEventParms.data.insert_data.serial_no, outQueue) );	// create Card
						cards[sysEventParms.data.insert_data.serial_no] = spCard;
					} else {
						LOG4CXX_INFO (logger, logHeader_ << "CARD s/n:" << sysEventParms.data.insert_data.serial_no << " NOT FOUNDED in config");
					}

					break;

				case ACU_SYS_EVT_CARD_REMOVED:
					LOG4CXX_INFO (logger, logHeader_ << "RX <- ACU_SYS_EVT_CARD_REMOVED s/n:" << sysEventParms.data.remove_data.serial_no);
					////getAcuSnapshot();

					// delete pointer with subsequent invoke of Card object destructor
					cards.erase(sysEventParms.data.remove_data.serial_no);
					break;

				case ACU_SYS_EVT_NO_EVENT:
					//LOG4CXX_INFO (logger, logHeader_ << "RX <- ACU_SYS_EVT_NO_EVENT");
					break;

				case ACU_SYS_EVT_RESMGR_RUNNING:
					LOG4CXX_INFO (logger, logHeader_ << "RX <- ACU_SYS_EVT_RESMGR_RUNNING");
					break;

				case ACU_SYS_EVT_RESMGR_STOPPED:
					LOG4CXX_INFO (logger, logHeader_ << "RX <- ACU_SYS_EVT_RESMGR_STOPPED");
					break;

				case ACU_SYS_EVT_PROSODY_IP_CARD_REGISTERED:
					LOG4CXX_INFO (logger, logHeader_ << "RX <- ACU_SYS_EVT_PROSODY_IP_CARD_REGISTERED");
					break;

				case ACU_SYS_EVT_PROSODY_IP_CARD_UNREGISTERED:
					LOG4CXX_INFO (logger, logHeader_ << "RX <- ACU_SYS_EVT_PROSODY_IP_CARD_UNREGISTERED");
					break;

				case ACU_SYS_EVT_PROSODY_IP_CARD_IP_CONFLICT:
					LOG4CXX_INFO (logger, logHeader_ << "RX <- ACU_SYS_EVT_PROSODY_IP_CARD_IP_CONFLICT");
					break;

				default:
					LOG4CXX_INFO (logger, logHeader_ << "acu_get_system_notification() UNKNOWN EVENT");
					break;
				} // END switch(sysEventParms.event)

			} while ( eventCode != ACU_SYS_EVT_NO_EVENT );
		}

	} //END while(!workerExit_)

}


bool Manager::getAcuSnapshot() {
	ACU_INT		rc;

	/*
	 * Interrogate with Aculab subsystem and getting system snapshot
	 */
	INIT_ACU_STRUCT(&rSnapshotParms);				// zeroise return structure
	rc = acu_get_system_snapshot(&rSnapshotParms);	// getting
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "acu_get_system_snapshot() ERROR:" << acu_error_desc(rc) );
		return false;
	} else {
		LOG4CXX_DEBUG (logger, logHeader_ << "acu_get_system_snapshot() OK." );
	}

	return true;
}


/* wait for a wait object */
ACU_OS_ERR acu_os_wait_for_wait_object2(ACU_WAIT_OBJECT* wait_object, int timeout)
{
        int     r;
        int     buffer;
        ACU_OS_ERR result = ERR_ACU_OS_NO_ERROR;

        r = poll(&wait_object->event_pfd, 1, timeout);

        if (r > 0) {
        	if (wait_object->event_pfd.revents) {
        		r = read(wait_object->event_fds[0], &buffer, 1);
        	}
        } else {
        	if( r == -1 )
                {
                        r = errno;
                } switch (r) {
                case 0:
                        result = ERR_ACU_OS_TIMED_OUT;
                        break;
                case EBADF: /* FALL THROUGH */
                case EFAULT:
                        result = ERR_ACU_OS_PARAMETER;
                        break;
                case ENOMEM:
                        result = ERR_ACU_OS_NO_MEMORY;
                        break;
                case EINTR:
                        result = ERR_ACU_OS_SIGNAL;
                        break;
                default:
                        result = ERR_ACU_OS_ERROR;
                }
        }

        return result;
}
