/**
  card.cpp
  Purpose: all operation with Aculab Prosody card (embedded or ProsodyX 1U):
  	  - initialize
  	  - run worker proccess for card
  	  - initialise ports (ports objects)

	@author: yanat
	@version: 1.05 20/02/2014
 */


#include <sstream>
#include <algorithm>
#include <vector>
#include "card.h"
// C includes
#include <string.h>

using namespace log4cxx;
using namespace std;
using namespace faxd;

ACU_OS_ERR acu_os_wait_for_wait_object(ACU_WAIT_OBJECT* wait_object, int timeout);

/*****************************************************************************\
 *
 *                       class Card methods
 *
\*****************************************************************************/

Card::Card(std::string serial, FileQueue &outQueue) :
		FSM<CardStates, CardEvents, Card>(STATE_CARD_START),
		logger(Logger::getLogger( "main")),		// internal logger initialization
		outQueue_(outQueue),
		initStatus(false),
		serialNo(serial),
		eventManagerExit_(false)
{

	/*
	 * stuffing transition table. Look fsm.h
	 */
	addTransition(STATE_CARD_START,			EVENT_CARD_INIT,	STATE_CARD_INSERVICE,	&Card::actionInit);
	addTransition(STATE_CARD_START,			EVENT_CARD_INIT,	STATE_CARD_INSERVICE,	&Card::actionRunWorker);
	addTransition(STATE_CARD_INSERVICE,		EVENT_CARD_REMOVE,	STATE_CARD_REMOVED,		&Card::actionCleanup);
	addTransition(STATE_CARD_REMOVED,		EVENT_CARD_ADD,		STATE_CARD_INSERVICE,	&Card::actionInit);
	// cleanup after EXIT signal
	addTransition(STATE_CARD_START,			EVENT_CARD_EXIT,	STATE_CARD_EXIT,		&Card::actionNoop);
	addTransition(STATE_CARD_INSERVICE,		EVENT_CARD_EXIT,	STATE_CARD_EXIT,		&Card::actionSetExit);
	addTransition(STATE_CARD_INSERVICE,		EVENT_CARD_EXIT,	STATE_CARD_EXIT,		&Card::actionWaitWorkerExit);
	addTransition(STATE_CARD_INSERVICE,		EVENT_CARD_EXIT,	STATE_CARD_EXIT,		&Card::actionCleanup);
	addTransition(STATE_CARD_REMOVED,		EVENT_CARD_EXIT,	STATE_CARD_EXIT,		&Card::actionSetExit);
	addTransition(STATE_CARD_REMOVED,		EVENT_CARD_EXIT,	STATE_CARD_EXIT,		&Card::actionWaitWorkerExit);

	/*
	 * logger prefix
	 */
	std::stringstream ssHeader;
	ssHeader << "card s/n:" << serialNo <<	" ";
	logHeader_ = ssHeader.str();

	/*
	 * try to initialize card
	 */
	proccessEvent(EVENT_CARD_INIT);	// send init even to FSM
	LOG4CXX_INFO (logger, logHeader_ << "EVENT_CARD_INIT");

	if( false == initStatus ) {
		LOG4CXX_INFO (logger, logHeader_ << "EVENT_CARD_INIT FAILED");
		proccessEvent(EVENT_CARD_EXIT);
		LOG4CXX_INFO (logger, logHeader_ << "EVENT_CARD_EXIT");
	}
}; // END Card::Card(std::string serial)


Card::~Card() {
	proccessEvent(EVENT_CARD_EXIT);
	LOG4CXX_DEBUG(logger, logHeader_ << " ~Card()");
}

string Card::getInfo() {
	stringstream ss;
	ss << endl;
	ss << "\tserial\t: " << serialNo << endl;
	ss << "\tresource id: " << rOpenParms.card_id << endl;
	ss << "\tdescription: " << rCardInfo.card_desc << endl;
	ss << "\tname\t: " << rCardInfo.card_name << endl;
	ss << "\ttype\t: " << rCardInfo.card_type << endl;
	ss << "\tversion\t: " << rCardInfo.hw_version << endl;
	ss << "\tip address: " << rCardInfo.ip_address;
	return ss.str();
}

void Card::actionInit() {
	ACU_INT	rc;

	/*
	 * trying to open card
	 */
	INIT_ACU_STRUCT(&rOpenParms);
	strcpy(rOpenParms.serial_no, serialNo.c_str()); // put char* string representation to ACU_OPEN_CARD_PARMS
	rc = acu_open_card(&rOpenParms);
	unCardId = rOpenParms.card_id; // internal card number
	if(rc) { // can't open the card
		LOG4CXX_ERROR (logger, logHeader_ << "acu_open_card() ERROR:" << acu_error_desc(rc));
		initStatus = false;
		return;
	} else { // all OK, card is opened
		LOG4CXX_INFO (logger, logHeader_ << "acu_open_card() OK. Resource ID: " << unCardId);
	}


	/*
	 *  Get Card Info
	 */
	INIT_ACU_STRUCT(&rCardInfo);
	rCardInfo.card_id = unCardId;
	rc = acu_get_card_info(&rCardInfo);	// get detailed card info
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "acu_get_card_info() " << acu_error_desc(rc));
		initStatus = false;
		return;
	} else {
		LOG4CXX_INFO (logger, logHeader_ << "acu_get_card_info() OK" << getInfo() );
	}

	/*
	 * Initialize card resources
	 */
	if(rCardInfo.resources_available & ACU_RESOURCE_SWITCH) {
		// Open Switch Driver Interface
		INIT_ACU_STRUCT(&rOpenSwitch);
		rOpenSwitch.card_id = unCardId;
		rc = acu_open_switch(&rOpenSwitch);
		if(rc) {
			LOG4CXX_ERROR (logger, logHeader_ << "acu_open_switch() " << acu_error_desc(rc));
			initStatus = false;
			return;
		} else {
			LOG4CXX_INFO (logger, logHeader_ << "acu_open_switch() OK.");
		}
	}

	if(rCardInfo.resources_available & ACU_RESOURCE_CALL) {
		// Open Call Driver Interface
		INIT_ACU_STRUCT(&rOpenCall);
		rOpenCall.card_id = unCardId;
		rc = acu_open_call(&rOpenCall);
		if(rc) {
			LOG4CXX_ERROR (logger, logHeader_ << "acu_open_call() " << acu_error_desc(rc));
			initStatus = false;
			return;
		} else {
			LOG4CXX_INFO (logger, logHeader_ << "acu_open_call() OK.");
		}
	}

	/*
	 * Open Speech Driver Interface. FAX belongs to speech resources
	 * Speech interface belongs to DSP subsystem
	 */
	INIT_ACU_STRUCT(&rOpenSpeech);
	rOpenSpeech.card_id = unCardId;
	rc = acu_open_prosody(&rOpenSpeech);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "aacu_open_prosody()  " << acu_error_desc(rc));
		initStatus = false;

	} else {
		LOG4CXX_INFO (logger, logHeader_ << "acu_open_prosody() OK.");
	}



	/*
	 * --------------------------- DSP -----------------------------------
	 * Initializing DSP resources holder, actually create DSPModules object for corresponding
	 * TiNG modules and initializing them
	 */
	if(rCardInfo.resources_available & ACU_RESOURCE_SPEECH) { // check bit mapped variable
		pModules.reset(new DSPModuleHolder(unCardId));
		if(pModules->getStatus() == false) {
			initStatus = false;
			return;
		}
	}


	/*
	 * --------------------------- PORTS ----------------------------------
	 * Initialize ports
	 * create, check availability and adding valid port to vector
	 * when port is creted, it's initialized and allocated DSP resources
	 * using DSPModuleHolder
	 */

	//Get info about ports on Card (quantity). Call control API
	INIT_ACU_CL_STRUCT(&cCardInfo);
	cCardInfo.card_id = unCardId;
	rc = call_get_card_info(&cCardInfo); // вытаскиваем информацию о карточке
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "call_get_card_info() " << acu_error_desc(rc));
		initStatus = false;
		return;
	} else {
		LOG4CXX_INFO (logger, logHeader_ << "number of ports: " << cCardInfo.ports);
	}

	for (ACU_UINT i=0; i<cCardInfo.ports; i++) {
		/*
		 * checking, if this port number configured in cfg to this card number
		 */
		int numThisPortsOnCard = std::count(cfg::cardsPorts[serialNo].begin(), cfg::cardsPorts[serialNo].end(), i);
		if(numThisPortsOnCard > 0) {
			boost::shared_ptr <Port> spPort( new Port(pModules, serialNo, unCardId, i, outQueue_) );
			if(spPort->getStatus()) {
				ports.push_back(spPort);
			} else {
				LOG4CXX_ERROR (logger, "card s/n:" << serialNo << " port:" << i << " ERROR. Not used");
			}
		}//END if

	}//END for

	/*
	 * card is initialized
	 */
	initStatus = true;
};


void Card::actionRunWorker() {
	spEventManagerThread.reset(new boost::thread(boost::bind(&Card::eventManager, this)));
	LOG4CXX_DEBUG (logger, logHeader_ << "FSM actionRunWorker() -> Card Event Manager (CEM) thread STARTED");
};


void Card::eventManager() {

	ACU_ERR rc;

	/*
	 * Create Event Queue
	 */
	ACU_ALLOC_EVENT_QUEUE_PARMS		rAllocEventQueue;
	INIT_ACU_STRUCT(&rAllocEventQueue);
	rc = acu_allocate_event_queue(&rAllocEventQueue);	// создает очередь событий
	queueId_	= rAllocEventQueue.queue_id;
	if(rc) {	LOG4CXX_ERROR (logger, logHeader_ << "CEM acu_allocate_event_queue() " << acu_error_desc(rc));
	} else {	LOG4CXX_INFO (logger, logHeader_ << "CEM acu_allocate_event_queue() OK");
	}

	/*
	 * Attach Queue to card
	 */
	ACU_QUEUE_PARMS					cSetCardQueue;
	INIT_ACU_CL_STRUCT(&cSetCardQueue);
	cSetCardQueue.queue_id		= queueId_;
	cSetCardQueue.resource_id	= unCardId;
	rc = acu_set_card_notification_queue(&cSetCardQueue);
	if(rc) {	LOG4CXX_ERROR (logger, logHeader_ << "CEM acu_set_card_notification_queue() " << acu_error_desc(rc));
	} else {	LOG4CXX_INFO (logger, logHeader_ << "CEM acu_set_card_notification_queue() OK");
	}


	/*
	 * get wait object for card using its resource id
	 */
	ACU_GET_CARD_WAIT_OBJECT_PARMS woParms;
	INIT_ACU_STRUCT(&woParms);
	woParms.card_id = unCardId;
	rc = acu_get_card_notification_wait_object(&woParms);
	if (rc){	LOG4CXX_ERROR (logger, logHeader_ << "CEM acu_get_card_notification_wait_object() " << acu_error_desc(rc));
	} else {	LOG4CXX_DEBUG (logger, logHeader_ << "CEM acu_get_card_notification_wait_object() OK ");
	}


	////WaitObject cardWaitObject(woParms.wait_object);

	/*
	 * main loop circle
	 */
	ACU_CARD_NOTIFICATION_PARMS cardEventParms;
	while(!eventManagerExit_) {
		////rc = cardWaitObject.wait(200, cmd);
		rc = acu_os_wait_for_wait_object(&woParms.wait_object, 100);

		if ( ERR_ACU_OS_NO_ERROR == rc ) {	// read some event without error
			/*
			 * get card event
			 */
			INIT_ACU_STRUCT(&cardEventParms);
			rc = acu_get_card_notification(&cardEventParms);
			if(rc) {
				LOG4CXX_ERROR (logger, logHeader_ << "CEM - acu_get_card_notification() " << acu_error_desc(rc));
				continue;
			}

			/*
			 * process events
			 */
			switch (cardEventParms.event) {
			case ACU_CARD_EVT_CARD_SURPRISE_REMOVE:
				LOG4CXX_FATAL (logger, logHeader_ << "CEM RX <- ACU_CARD_EVT_CARD_SURPRISE_REMOVE");
				proccessEvent(EVENT_CARD_REMOVE);
				break;

			case ACU_CARD_EVT_CARD_REMOVE_PENDING:
				LOG4CXX_INFO (logger, logHeader_ << "CEM RX <- ACU_CARD_EVT_CARD_REMOVE_PENDING");
				break;

			case ACU_CARD_EVT_CARD_REMOVE_COMPLETE:
				LOG4CXX_INFO (logger, logHeader_ << "CEM RX <- ACU_CARD_EVT_CARD_REMOVE_COMPLETE");
				break;

			case ACU_CARD_EVT_NO_EVENT:
				LOG4CXX_INFO (logger, logHeader_ << "CEM RX <- ACU_CARD_EVT_NO_EVENT");
				break;

			default:
				LOG4CXX_INFO (logger, logHeader_ << "CEM RX <- UNKNOWN EVENT");
				break;
			}

		} //END if ( (rc == ERR_ACU_OS_NO_ERROR) && (!exit) )
	} // END while(!workerExit_) {
};


void Card::actionCleanup() {
	ACU_INT	rc;	// retcode

	/*
	 * erase DSP Holder
	 */
	pModules.reset();
	LOG4CXX_DEBUG (logger, logHeader_ << "actionCleanup() - DSP CLOSED");

	/*
	 * close all ports
	 */
	ports.clear();
	LOG4CXX_DEBUG (logger, logHeader_ << "actionCleanup() - PORTS CLOSED");

	/*
	 * Close speech resources
	 */
	ACU_CLOSE_PROSODY_PARMS closeProsodyParms;
	INIT_ACU_STRUCT(&closeProsodyParms);
	closeProsodyParms.card_id = unCardId;
	rc = acu_close_prosody(&closeProsodyParms);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "acu_close_prosody()  " << acu_error_desc(rc));
	} else {
		LOG4CXX_INFO (logger, logHeader_ << "acu_close_prosody() OK.");
	}

	/*
	 * Card close call
	 */
	ACU_CLOSE_CALL_PARMS closeCallParms;
	INIT_ACU_STRUCT(&closeCallParms);
	closeCallParms.card_id = unCardId;
	rc = acu_close_call(&closeCallParms);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "acu_close_call() " << acu_error_desc(rc));
	} else {
		LOG4CXX_INFO (logger, logHeader_ << "acu_close_call() OK");
	}


	/*
	 * Card close switch
	 */
	ACU_CLOSE_SWITCH_PARMS closeSwitchParms;
	INIT_ACU_STRUCT(&closeSwitchParms);
	closeSwitchParms.card_id = unCardId;
	rc = acu_close_switch(&closeSwitchParms);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "acu_close_switch() " << acu_error_desc(rc));
	} else {
		LOG4CXX_INFO (logger, logHeader_ << "acu_close_switch() OK");
	}

	/*
	 * Card close card
	 */
	ACU_CLOSE_CARD_PARMS closeCardParms;
	INIT_ACU_STRUCT(&closeCardParms);
	closeCardParms.card_id = unCardId;
	rc = acu_close_card(&closeCardParms);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "acu_close_card() " << acu_error_desc(rc));
	} else {
		LOG4CXX_INFO (logger, logHeader_ << "acu_close_card() OK");
	}

	/*
	 * Free event Queue
	 */
	ACU_FREE_EVENT_QUEUE_PARMS		rFreeEventQueue;	// used for cleanup Aculab event queue before exiting
	INIT_ACU_STRUCT(&rFreeEventQueue);
	rFreeEventQueue.queue_id = queueId_;
	rc = acu_free_event_queue(&rFreeEventQueue);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "acu_free_event_queue() " << acu_error_desc(rc));
	} else {
		LOG4CXX_INFO (logger, logHeader_ << "acu_free_event_queue() OK");
	}
};

void Card::actionWaitWorkerExit() {
	/*
	 * wait for working thread termination
	 */
	spEventManagerThread->join();
	LOG4CXX_DEBUG (logger, logHeader_ << "actionWaitWorkerExit() - eventManager() TERMINATED");
};



/* wait for a wait object */
ACU_OS_ERR acu_os_wait_for_wait_object(ACU_WAIT_OBJECT* wait_object, int timeout)
{
        int     r;
        int     buffer;
        ACU_OS_ERR result = ERR_ACU_OS_NO_ERROR;

        r = poll(&wait_object->event_pfd, 1, timeout);

        if (r > 0) {
        	if (wait_object->event_pfd.revents) {
        		r = read(wait_object->event_fds[0], &buffer, 1);
        	}
        } else {
        	if( r == -1 )
                {
                        r = errno;
                } switch (r) {
                case 0:
                        result = ERR_ACU_OS_TIMED_OUT;
                        break;
                case EBADF: /* FALL THROUGH */
                case EFAULT:
                        result = ERR_ACU_OS_PARAMETER;
                        break;
                case ENOMEM:
                        result = ERR_ACU_OS_NO_MEMORY;
                        break;
                case EINTR:
                        result = ERR_ACU_OS_SIGNAL;
                        break;
                default:
                        result = ERR_ACU_OS_ERROR;
                }
        }

        return result;
}

