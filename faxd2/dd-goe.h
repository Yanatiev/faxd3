
#ifndef DD_GOE_H_
#define DD_GOE_H_

#include <log4cxx/logger.h> // log4cxx header
#include "dd.h"

namespace faxd {

	class GOEDescriptor : public DeliverDescriptor {
		log4cxx::LoggerPtr logger;	// reference to log initialized based on cfg data

	public:
		GOEDescriptor(std::string path, std::string ext, std::string name_suffix);
		~GOEDescriptor();
		bool create();

	private:
		std::string name_suffix_;
	};

}

#endif /* DD_GOE_H_ */
