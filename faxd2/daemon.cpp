#include <exception>
#include <sstream>
#include <vector>
#include <boost/filesystem.hpp>
#include <fstream>
#include <log4cxx/logger.h>
#include <log4cxx/xml/domconfigurator.h>
#include "daemon.h"
#include "config.h"
#include "manager.h"
#include <syslog.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/wait.h>

using namespace faxd;
using std::string;
using std::vector;
using std::stringstream;
namespace fs = boost::filesystem;


Daemon::Daemon(std::string& configFile, std::string& section_, std::string& pidFile) :
	//configFile_(configFile),
	cfg_(configFile),
	pidFile_(pidFile)
{	// initialize syslog
	openlog ("FAXD2", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL0);

	//check if PID file exist (process already running) - exit
	fs::path pidPath( pidFile_ );
	if(fs::exists(pidPath)) {
		stringstream msg;
		msg << "PID file " << pidFile_ << " exists. EXIT";
		syslog (LOG_ERR, msg.str().c_str());
		throw DaemonException(msg.str());
	}

	//check if spoolDir exist (where FAXD2 working directories are placed)
	fs::path spoolPath( cfg::spoolDir );
	if( !fs::exists(spoolPath) || !fs::is_directory(spoolPath) ) {
		stringstream msg;
		msg << "Can't find working directory " << cfg::spoolDir << " EXIT";
		syslog (LOG_ERR, msg.str().c_str());
		throw DaemonException(msg.str());
	}

	static std::vector < CallProperty >	* dnisPropsPtr = cfg::getDnisProps();
	for(unsigned int i=0; i< dnisPropsPtr->size(); i++) {
		if( !(*dnisPropsPtr)[i].getDeliverName().empty() ) {
			try {
				fs::create_directory(cfg::inDir + (*dnisPropsPtr)[i].getDeliverName());
			} catch (std::exception& e) {
				syslog (LOG_ERR, ("can't create: " + cfg::inDir + (*dnisPropsPtr)[i].getDeliverName() + " EXIT").c_str());
				closelog();
				return;
			}
		}
	}


	/*
	 * Run daemon
	 */
	pid_ = fork();
	if ( -1 == pid_) { // can't run child process

		// logging error reason
		syslog (LOG_ERR, "can't run daemon process");
		throw std::runtime_error("Can't run daemon");
		return;

	} else if (0 == pid_) { // we are in child


		pid_ = getpid();	// get real pid of daemon process
		umask(0);			// clear mask, permit access to all created file access bits, for problem avoiding
		setsid();			// create new process session for independence from parent
		chdir("/");			// change current path to root, for umount filesystems problems avoiding

		close(STDIN_FILENO);// close standard streams descriptors - we havn't terminal
		close(STDOUT_FILENO);
		close(STDERR_FILENO);

		//create pidfile
		std::ofstream pidStream( pidFile_.c_str() );
		if ( !pidStream ) {
			stringstream msg;
			msg << "can't create PID file " << pidFile_;
			throw std::runtime_error(msg.str().c_str());
			return;
		} else {
			pidStream << pid_;
			pidStream.close();
		}

		/*
		 * run core daemon method
		 */
		stringstream msg;
		msg << "daemon STARTED log settings - " << cfg::logFile;
		syslog (LOG_ERR, msg.str().c_str());

		worker();

		syslog (LOG_ERR, "daemon EXIT");
		/*
		 * Cleanup:
		 * delete pid file, close syslog
		 */
		//TODO cleanup spool directories
		boost::filesystem::remove(pidPath);	// delete pid file
		closelog ();						// close log
		return;

	} else { // fork return 0 - we are in parent. Exiting, We've ran the daemon
		closelog ();
		return;
	}
};

void Daemon::worker() {
	sigset_t  sigset;
	siginfo_t siginfo;

	// create empty set of signals, and stuff them
	sigemptyset(&sigset);
	sigaddset(&sigset, SIGQUIT);	// terminate by user
	sigaddset(&sigset, SIGINT);		// terminate by user from terminal
	sigaddset(&sigset, SIGTERM);	// request signal for proccess termination
	sigaddset(&sigset, SIGCHLD);	// changing status of child process
	sigaddset(&sigset, SIGUSR1);	// user signal 1
	sigaddset(&sigset, SIGUSR2);	// user signal 2
	sigprocmask(SIG_BLOCK, &sigset, NULL);

	// logger configuration
	log4cxx::xml::DOMConfigurator::configure(cfg::logFile);
	log4cxx::LoggerPtr logger(log4cxx::Logger::getLogger( "main"));

	// set PATH
	stringstream path;
	path << "PATH=" << getenv("PATH") << ":/usr/local/aculab/v6/bin";
	string spath(path.str());
	vector<char> vpath(spath.begin(), spath.end());
	vpath.push_back('\0');
	putenv(&vpath[0]);
	LOG4CXX_INFO(logger, "SET ENVIRONMENT " << &vpath[0]);

	putenv("ACULAB_ROOT=/usr/local/aculab/v6");
	LOG4CXX_INFO(logger, "SET ENVIRONMENT ACULAB_ROOT=" << getenv("ACULAB_ROOT"));

	putenv("TiNGTYPE=LINUX");
	LOG4CXX_INFO(logger, "SET ENVIRONMENT TiNGTYPE=" << getenv("TiNGTYPE"));


	/*
	 * running
	 */
	try {
		faxd::Manager mgr(cfg_);
		/*
		 * infinity circle (process signals)
		 */
		bool exit = false;
		while(!exit) {
			sigwaitinfo(&sigset, &siginfo); //wait for signal
			switch(siginfo.si_signo) {
			case SIGQUIT:
				exit = true;
				break;
			}
		}

	} catch (...) { LOG4CXX_INFO(logger, "MGR exception"); }
	LOG4CXX_INFO(logger, "After MGR");

	LOG4CXX_INFO(logger, "FAXD2. QUIT");
}

