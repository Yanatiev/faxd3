/*
 * channel-fsm.h
 *
 *  Created on: Jan 11, 2015
 *      Author: root
 */

#ifndef CHANNEL_FSM_H_
#define CHANNEL_FSM_H_


#include <vector>
#include <log4cxx/logger.h> // log4cxx header
#include <boost/shared_ptr.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include "config.h"
#include "port.h"
#include "dsp-mgmt.h"
#include "fax.h"
#include "fsm.h"

enum Events {
	EVENT_RX_INIT				= 0,
	EVENT_WAIT_FOR_INCOMING		= 1,
	EVENT_INCOMING_CALL_DET		= 2,
	EVENT_WAIT_FOR_ACCEPT		= 3,
	EVENT_CONNECTED				= 4,
	EVENT_REMOTE_DISCONNECT		= 5,
	EVENT_IDLE					= 6,
	EVENT_TX_INIT				= 7,
	EVENT_WAIT_FOR_OUTGOING		= 8,
	EVENT_OUTGOING_RINGING		= 9,
	EVENT_EXIT					= 10
};


enum States {
	STATE_START,
	STATE_RX_INIT,
	STATE_RX_WAIT,
	STATE_RX_INCOMING_DETECTED,
	STATE_RX_WAIT_FOR_ACCEPT,
	STATE_CONNECTED,
	STATE_REMOTE_DISCONNECT,
	STATE_TX_INIT,
	STATE_TX_WAIT,
	STATE_TX_RINGING,
	STATE_EXIT
};


namespace faxd {
	class Port;

	class Channel : public FSM<States, Events, Channel>{
		friend class Port;
		friend class Fax;

	public:
		Channel(boost::shared_ptr<DSPModuleHolder> p_dspholder, Port* p_port, int ts);

	private:
		log4cxx::LoggerPtr logger;	// reference to log initialized based on cfg data
		std::string	logHeader_;		// suffix for logging messages
		faxd::Port 		*pPort;		//!< pointer to owner TDM port for accessing to it's properties
		faxd::Fax		fax;		//!< All FAX data and methods
		boost::shared_ptr<DSPModuleHolder>	spDSPHolder;
		boost::shared_ptr<DSPPool>			spChannelDSP; //!< allocated DSP resource to port DSPPool with capacity of 1
		int 			ts;			//!< timeslot. absolute timeslot id on TDM
		ACU_INT 	port_idx;				//!< port consequent number 0, 1, 2, ...
		ACU_PORT_ID port_id;				//!< port internal ID
		ACU_INT 	dspmodule_idx;			//!< DSP Module consequent number 0, 1, 2, ...
		ACU_INT		tingstream_id;				//!< TiNG stream id: 64,...
		ACU_INT		tingstream_start_channel;	//!< TiNG offset channel in stream (DSP channel, serving TDM channel)
		ACU_INT		tingstream_num_channel;		//!< TiNG number of allocated channel in stream (in our case - 1)
		tSMModuleId		TiNG_module_id;			//!< opened DSP module data structure. A value obtained from sm_open_module()
		tSMChannelId	TiNG_channel_id;		//!< Channel::SPEECH_AllocateResource() -> sm_channel_alloc_placed()
		ACU_INT			stream;		//!< CONE STONE PARAMETER!!!  = port_idx+32 => port's stream number in internal Matrix.
		ACU_CALL_HANDLE	handle;		//!< handle from call_openin(&cOpenIn), some reference to internal data corresponded to this timeslot
//		boost::recursive_mutex	CS;				//!< for operating like Critical Section
		long			nCounter;		//!< time to call disconnect in seconds
//		ACU_LONG		nCallState;		//!< status of current call
		std::string		ani;			//!< calling number (from)
		std::string		dnis;			//!< called number (to)

		boost::scoped_ptr<boost::thread>	spWorkerThread;	//!< thread object for running channelHandler

		/*
		 * attributes for sending file
		 */
		std::string sendFileName_;		//!< tiff filename with path and extension


		/*
		 * FSM actions
		 */
		void actionNoop();
		void actionOpenIn();
		void actionCallDetails();
		void actionSendRinging();
		void actionCallAccept();
		void actionDisconnect();
		void actionRelease();
		void actionOpenOut();
		void actionRxFax();
		void actionTxFax();

		/**
		 * @brief cast pointer to channel object to unsigned long long int handler for further using by Aculab drivers
		 */
		ACU_ACT convertToHandler() {return (ACU_ACT)this;};

		/**
		 * @brief worker thread on Channel. It starts fax receiving
		 */
		void workerRx();

		/**
		 * @brief SPEECH methods
		 */
		void SPEECH_AllocateResource();
		void SPEECH_SwitchTDMtoResource();
		void SPEECH_UnSwitchTDMfromResource();
		void SPEECH_ReleaseResource();

		std::string getAni()	{return ani;};
		std::string getDnis()	{return dnis;};
		std::string toString();
	};

}
#endif /* CHANNEL_FSM_H_ */
