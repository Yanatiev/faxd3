/*
 * faxd.cpp
 *
 *  Created on: Feb 15, 2014
 *      Author: root
 */

#include <iostream>
#include <vector>
#include <string>

#include <log4cxx/logger.h>
#include <log4cxx/xml/domconfigurator.h>
// C includes
#include <res_lib.h>
#include <acu_type.h>
#include <string.h>
// for polling stdin
#include <stdio.h>
#include <sys/poll.h>

#include "daemon.h"
#include "manager.h"

using namespace std;
using namespace faxd;
using namespace log4cxx;
using namespace log4cxx::xml;
using namespace log4cxx::helpers;

int main(int argc, char** argv)
{
	string cfg;
	string pidfile("/var/run/faxd2.pid");
	switch (argc) {
	case 3:
		pidfile.assign(argv[2]);
	case 2:
		cfg.assign(argv[1]);
		break;
	default:
		return -1;
	}

	try {
		faxd::Daemon(cfg, pidfile);
	} catch (const DaemonException& de) {
		return -1;
	} catch (const ConfigException& ce) {
		return -1;
	}

	return 0;
}
