/*
 * manager.h
 *
 *  Created on: Mar 13, 2014
 *      Author: root
 */

#ifndef MANAGER_H_
#define MANAGER_H_

#include <string>
#include <boost/shared_ptr.hpp>
#include "filequeue.h"
#include "card.h"
//#include "environment/cfg.h"
#include "config.h"
#include "fsm.h"
#include <log4cxx/logger.h>

enum ManagerStates {
	STATE_MGR_NEW,
	STATE_MGR_START,
	STATE_MGR_EXIT
};

enum ManagerEvents {
	EVENT_MGR_START,
	EVENT_MGR_EXIT
};

namespace faxd {

	class Manager: public FSM<ManagerStates, ManagerEvents, Manager> {
		Config &cfg_;
		log4cxx::LoggerPtr	logger;						// logger object
		std::string			logHeader_;					// suffix for logging messages
		FileQueue			outQueue;					// file queue for outbound FAXes
		std::map<std::string, boost::shared_ptr<Card> > cards;	// founded configured cards. Key - string serial no, shared ptr to Card object

		boost::scoped_ptr<boost::thread>	spWorkerThread;	//!< thread object for running channelHandler
		volatile bool						workerExit_;	//!< signal to event manager thread to exit

		ACU_SNAPSHOT_PARMS					rSnapshotParms;	//!< Aculab Prosody system snapshot.

		/**
		 * @brief run worker thread
		 */
		void actionRunWorker();

		/**
		 * @brief set exit_ flag to true - signal to worker thread to exit
		 */
		void actionSetExit();

		/**
		 * join() worker thread, after actionSetExit()
		 */
		void actionWaitWorkerExit();

		/**
		 * @brief worker thread on Manager. Main target - capture system events
		 */
		void worker();

		bool getAcuSnapshot();

	public:

		Manager(Config &cfg);
		~Manager();
	};

}


#endif /* MANAGER_H_ */
