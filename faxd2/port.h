/**
	@file port.h
    Purpose: all operation with Aculab Prosody TDM port
  	  - initialize
  	  - run worker thread
  	  - initialize ports (ports objects)

	@author: yanat
	@version: 1.01
	@date 20/02/2014
 */

#ifndef PORT_H_
#define PORT_H_

#include <vector>
#include <log4cxx/logger.h> 		// log4cxx header
//#include <boost/thread/mutex.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/thread/thread.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/bind.hpp>
#include "config.h"
#include "filequeue.h"
//#include "aux.h"
#include "dsp-mgmt.h"
#include "waitobject.h"
#include "channel-fsm.h"
//Aculab includes
#include <cl_lib.h>
#include <acu_type.h>

namespace faxd {
	class Channel;
/**
 * @class Port port.h
 * @brief Holder T1/E1 port
 * @detailed
 */
	class Port {
		/* environment components */
		log4cxx::LoggerPtr	logger;		// reference to log initialized based on cfg data
		std::string			logHeader_;	// suffix for logging messages
		FileQueue			&outQueue_;	// reference to file queue for outbound faxes

		bool initStatus;				//!< true - initialization OK, false - ERROR
		boost::shared_ptr<DSPModuleHolder> spDSPHolder;			//!< pointer to DSP resource manger
		std::vector< boost::shared_ptr<Channel> > vspChannels;	//!< channel set completing in Port::Port in main thread

		//WaitObject command;	//!< command pipe. Signalled from Card. exit, ... and so on

		/* Aculab structures for port manipulating (open/close)*/
		OPEN_PORT_PARMS			cOpenPort;	//!< structure for port opening with some info (port/queue id, ...).
		CLOSE_PORT_PARMS		cClosePort;	//!< structure for port closing with a call_close_port()
		PORT_INFO_PARMS			cPortInfo;	//!< vector of B-channels

		Port(Port const&);		//!<< hidden copy constructor to protect unique Port object

		boost::scoped_ptr<boost::thread>	pEventManagerThread; //!< thread object for running eventManager
		volatile bool						eventManagerExit_;		//!< signal to event manager thread to exit
		/**
		 * @brief Event controller on port. It starts in thread
		 * @detailed
		 * 		- it runs in thread
		 * 		- register port event queue
		 * 		- proccess all event on port. Gives commands to Channels
		 * 		- thread start in Card, joined in ~Card
		 */
		void eventManager();

		/**
		 * @brief analog TDM_OpenIncomingCallsThread, but in main thread
		 */
		void initChannels();

	public:
		/* Port attributes, correspond PORT_DATA from LoadTester */

		//WaitObject	done_event;	//< Pdone_event - is a signal from port TDM_PortEventManagerThread to PortThread. Not USED
		std::string cardSerialNo;	//!< parent card serial in string representation
		ACU_INT		port;		//< port order index (0, 1, 2, ...). Correspond to port_ix from constructor
		ACU_PORT_ID	port_id;	//< internal port id OPEN_PORT_PARMS::port_id
		ACU_INT		numcallsin;	//< maximum inbound calls per port (deprecated)
		ACU_INT		numcallsout;//< maximum outbound calls per port (deprecated)
		boost::recursive_mutex CS;		//< for operating like Critical Section
		ACU_INT		module;		//< current TiNG DSP index (0, 1, 2)
		ACU_INT		TiNG_stream;//< DSP module stream (64,66,68,70, 80,82,84,86, ?)
		ACU_INT		First_TiNG_timeslot;	//!< base timeslot in TiNG stream. 128 per stream
											//!< For ISDN E1 increment - 30 (0,30,60,90,120)
											//!< For ISDN T1 increment - 23 (0,23,46,69,...)
		ACU_EVENT_QUEUE		queue_id;		//!< Event queue, unique per port. Binds with a driver

		ACU_INT		calls_in_progress;		//!< MUST BE UNUSED counter of active timeslots.


/**
 * @brief TDM port initialization
 * @detailed using card_id and port_ix constructor open port with a Aculab Call Control API
 * 		opened port must be closed in destructor. (Look to Port::~Port)
 * 		If port can't be opened and has not TDM type of line => initStatus will be FALSE!
 *
 * @param	[in]	config	reference to ininitialized config holder
 * @param	[in]	logger	reference to ininitialized logger
 * @param	[in]	cardId	internal ID of prosody card
 * @param	[in]	port_ix	port index {0, 1, 2, ...}
 * @return Port object
 */
		Port(boost::shared_ptr<DSPModuleHolder> sp_dspholder, std::string card_sn, ACU_CARD_ID card_id, ACU_INT port_ix, FileQueue &outQ);
/**
 * @brief Close TDM port
 */
		~Port();
/**
 * @brief get initialization status
 * @return initStatus
 */
		bool getStatus() { return initStatus; };
		ACU_CARD_ID getCardId() { return cOpenPort.card_id;};

	}; // END class Port
}

#endif /* PORT_H_ */
