
#include <boost/property_tree/json_parser.hpp>
//#include <boost/optional.hpp>
#include <boost/foreach.hpp>
//#include <cassert>
#include <exception>
#include <iostream>
#include <sstream>
#include <string>
#include <sstream>
#include <syslog.h>

#include "config.h"

using namespace faxd;
using namespace boost::property_tree;
using std::string;
using std::map;
using std::vector;
using std::cout;
using std::endl;


ConfigException::ConfigException() :
	message_("JSON parse exception")
{};

ConfigException::ConfigException(const std::string& message) :
	message_(message)
{};

ConfigException::~ConfigException() throw() {};

CallProperty::CallProperty(string regexStr) :
		dnisRegex_(regexStr),
		modem_(MODEM_V34),
		ecm_(1),
		trace_(false),
		keepTmp_(false),
		deliverName_("")	// empty delivery queue name in config adds nothing
	{};

bool CallProperty::isMatched(const string& dnis) {
	return boost::regex_match(dnis, dnisRegex_);
};

std::vector < CallProperty > Config::dnisProps;
string Config::logFile;
string Config::serverId;
map< string, vector<int> > Config::cardsPorts;
map< string,string > Config::cardsId;
int Config::TDMNumCallsPerPort;
int Config::streamsPerDSP;
int Config::CallDisconnectTime;
int Config::Companding;
int Config::FAXLogToFile;
string Config::outQueue;
string Config::spoolDir;
string Config::inDir;
string Config::badDir;
string Config::outDir;
string Config::tmpDir;
string Config::infoExt;
string Config::faxExt;
DeliverType Config::deliverType;
string Config::deliverGoeDir;
string Config::deliverTifDir;
boost::property_tree::ptree Config::pt;

bool Config::configure(string configFile) {

	/*
	 * reading config from file
	 */
	std::stringstream ss;

	std::ifstream file( configFile.c_str() );
	if ( file ) {
			ss << file.rdbuf();
			file.close();
	} else {
		//std::cout << "CAN'T OPEN CONFIG FILE: " << FAXD_CFG_FILE <<". EXITING"<< std::endl;
		return false;
	}

	/*
	 * parse JSON formatted config file to config objects;
	 */
	try {
		 //boost::property_tree::ptree pt;
		 boost::property_tree::read_json(ss, pt);

		 /*
		  * get global variables
		  */
		 serverId			= pt.get<string>("serverid", "9999");
		 logFile			= pt.get<string>("logcfg", "/etc/faxd2/Log4cxxConfig.xml");
		 outQueue			= pt.get<string>("outQueue", "");
		 spoolDir			= pt.get<string>("spoolDir", "./");
		 inDir	= spoolDir + "in/";
		 badDir	= spoolDir + "bad/";
		 outDir	= spoolDir + "out/";
		 tmpDir	= spoolDir + "tmp/";
		 infoExt			= pt.get<std::string>("infoExt", ".info");
		 faxExt				= pt.get<std::string>("faxExt", ".tiff");
		 TDMNumCallsPerPort	= pt.get<int>("TDMNumCallsPerPort", 24);
		 streamsPerDSP		= pt.get<int>("streamsPerDSP", 1);
		 CallDisconnectTime	= pt.get<int>("CallDisconnectTime", 600);
		 Companding			= pt.get<int>("Companding", 1);
		 FAXLogToFile		= pt.get<int>("FAXLogToFile", 1);

		/*
		* Getting all card specific info
		*/
		BOOST_FOREACH(boost::property_tree::ptree::value_type& card, pt.get_child("cards")) {
			// вытаскиваем сериал очередной карты
			string serialNo = card.second.get<string>("serial");
			cardsId[serialNo] =  card.second.get<string>("id");
	        	// если этого ключа нет - выбросится исключение
			try {
			// поскольку второй узел листа, то гуляем по конретной карте, вытаскиваем узлы под proto
				BOOST_FOREACH(boost::property_tree::ptree::value_type& port, card.second.get_child("active_ports")) {
					int portNum = atoi(port.second.data().c_str());
					cardsPorts[serialNo].push_back(portNum);
				}
			} catch (boost::property_tree::ptree_bad_path const& bpe) {
				//std::cout<< "NO active_ports for CARD " << serialNo << std::endl;
			}
		}//END BOOST_FOREACH

		/*
		 * Getting all DNIS specific info
		 */
		BOOST_FOREACH(boost::property_tree::ptree::value_type& dnis, pt.get_child("dnis")) {

			// pattern
			CallProperty cp( dnis.second.get<std::string>("pattern") );

			// modem type
			string modemStr = dnis.second.get<std::string>("modem");
			if(modemStr == "v17")
				cp.setModem(MODEM_V17);
			else if(modemStr == "v29")
				cp.setModem(MODEM_V29);
			else if(modemStr == "v27ter")
				cp.setModem(MODEM_V27);
			else
				cp.setModem(MODEM_V34);

			// ecm. if key is not present - catching exception and consider ecm = 1 (set in constructor)
			try {
				int ecm = dnis.second.get<int>("ecm");
				cp.setEcm(ecm);
			} catch (boost::property_tree::ptree_bad_path const& bpe) {}

			// trace. if key is not present - catching exception and consider trace == false (set in constructor)
			try {
				bool trace = dnis.second.get<bool>("trace");
				cp.setTrace(trace);
			} catch (boost::property_tree::ptree_bad_path const& bpe) {}

			// keepTmp. if key is not present - catching exception and consider keepTmp_ == false (set in constructor)
			try {
				bool keepTmp = dnis.second.get<bool>("keepTmp");
				cp.setKeepTmp(keepTmp);
			} catch (boost::property_tree::ptree_bad_path const& bpe) {}

			// keepTmp. if key is not present - catching exception and consider keepTmp_ == false (set in constructor)
			try {
				string deliverQueueName = dnis.second.get<std::string>("deliver");
				cp.setDeliverName(deliverQueueName + "/");
			} catch (boost::property_tree::ptree_bad_path const& bpe) {}

			dnisProps.push_back(cp);
		}

		/*
		 * adding last "default" or "stub" entry with default settings
		 */
		CallProperty cp(".*");
		cp.setModem(MODEM_V34);
		cp.setEcm(1);
		dnisProps.push_back(cp);

		/*
		 * parse "deliver" part of the configuration
		 */
		string typeStr			= pt.get<string>(CFG_SECTION_DELIVER ".type", "bad");
		if(typeStr == CFG_SECTION_DELIVER_TYPE_GOE) {
			deliverType		= DELIVER_TYPE_GOE;
			deliverGoeDir	= pt.get<string>(CFG_SECTION_DELIVER ".goeRemoteDir", "");
			deliverTifDir	= pt.get<string>(CFG_SECTION_DELIVER ".tifRemoteDir", "");
		} else if (typeStr == CFG_SECTION_DELIVER_TYPE_VOXFAX){
			deliverType		= DELIVER_TYPE_VOXFAX;
			deliverGoeDir	= "";
			deliverTifDir	= "";
		} else {
			throw ConfigException("problem in section \"deliver\"");
		}

	} catch (std::exception const& e) {
		openlog ("FAXD2", LOG_CONS | LOG_PID | LOG_NDELAY, LOG_LOCAL0);
		//std::cout << "JSON parse exception" <<std::endl;
		//std::cerr << e.what() << std::endl;

		std::stringstream msg;
		msg << "JSON parse exception " << e.what();
		syslog (LOG_ERR, msg.str().c_str());
		closelog ();						// close log
		return false;
	}
	return true;
};

int Config::getInt(std::string key, int defaultValue) {
	try {
		return (pt.get<int>(key, defaultValue));
	} catch (boost::property_tree::ptree_error const &err) {
		return defaultValue;
	}
};

std::string Config::getStr(std::string key, std::string defaultValue) {
	try {
		return (pt.get<string>(key, defaultValue));
	} catch (boost::property_tree::ptree_error const &err) {
		return defaultValue;
	}
};

modem Config::getModemByDnis(std::string dnis) {
	for(unsigned int i=0; i<dnisProps.size(); i++) {
		if(dnisProps[i].isMatched(dnis))
			return dnisProps[i].getModem();
	}
	assert(false); // wrong behavior - success pattern MUST be in vector
	return MODEM_V34;
};

CallProperty* Config::getDnisSettings(std::string dnis) {
	for(unsigned int i=0; i<dnisProps.size(); i++) {
		if(dnisProps[i].isMatched(dnis))
			return &dnisProps[i];
	}
	assert(false); // wrong behavior - success pattern MUST be in vector
	return NULL;
};

string Config::toString() {
	std::stringstream ss;
	ss << std::endl;
	ss << "\t\t" << "logFile\t= " << logFile << std::endl;
	ss << "\t\t" << "spoolDir\t= " << spoolDir << std::endl;
	ss << "\t\t" << "TDMNumCallsPerPort\t= " << TDMNumCallsPerPort << std::endl;
	ss << "\t\t" << "Companding\t= " << (Companding ? "Mu-law" : "A-law") << std::endl;
	ss << "\t\t" << "FAXLogToFile\t= " << FAXLogToFile << std::endl;

	std::map< string, vector<int> >::iterator i;
	for(i=cardsPorts.begin(); i!=cardsPorts.end(); ++i) {
		ss << "\t\tCard Serial:" << i->first << ". Configured active ports: ";
		vector<int>::iterator j;
		for(j=i->second.begin(); j!=i->second.end(); ++j)
			ss << (*j) << " ";
		ss << std::endl;
	}

	if(deliverType == DELIVER_TYPE_GOE) {
		ss << "\t\t" << "deliver type of inbound faxes\t= " << CFG_SECTION_DELIVER_TYPE_GOE << std::endl;
		ss << "\t\t" << "goe remote dir\t= " << deliverGoeDir << std::endl;
		ss << "\t\t" << "tif remote dir (base)\t= " << deliverTifDir << std::endl;
	} else {
		ss << "\t\t" << "deliver type of inbound faxes\t= " << CFG_SECTION_DELIVER_TYPE_VOXFAX << std::endl;
	}
	ss << std::endl;
	return ss.str();
};

Config::Config(std::string configFile) throw (ConfigException) {
	if (configure(configFile) == false) {
		ConfigException ce;
		throw ce;
	}
};

