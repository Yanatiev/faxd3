/*
 * dsp-mgmt.h
 *
 *  Created on: Mar 19, 2014
 *      Author: root
 */

#ifndef DSP_MGMT_H_
#define DSP_MGMT_H_

#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <boost/thread/mutex.hpp>
#include <log4cxx/logger.h> // log4cxx header
#include <log4cxx/xml/domconfigurator.h>
#include "config.h"			// for constants
// C includes (Aculab headers)
#include <res_lib.h>
#include <acu_type.h>
#include <sw_lib.h>
#include <cl_lib.h>
#include <smdrvr.h>	//TiNG
#include <smrtp.h>	//TiNG


namespace faxd {
	class DSPPool;
	class DSPModule;

/**
 * @class DSPStream
 * @brief Represents DSP stream. It's used for accounting allocated DSP resources
 * @details Each DSP (TiNG) on card consist of two streams. Each stream has 128 channels. Each channel may be used in
 * 	voice processing. I.e using switch functionality some time slot (channel of T1/E1 port) may be switched to
 * 	certain DSP channel, so after that media stream will be processed on DSP.<br/>
 * 	Inside card (matrix switch) each channel TDM or DSP has its own coordinates - [stream #]:[channel #]<br/>
 * 	Stream number is unique inside a card.<br/>
 * 	DSP stream numbering:<br/>
 * 	- 64,65,66,67,68,69,70,71 - for first 4 DSPs, and 80,81,82,83,84,85,86,87 for other 4 DSPs (maximum DSPs - 8)<br/><br/>
 * 	Busy/free channels in DSP streams are represented as a string[TING_MAX_CHANNELS_PER_STREAM], where ' ' - means free TiNG
 * 	channel, 'A' - allocated channel.
 */
	class DSPStream {
		bool initStatus;		//!< true - initialization OK, false - ERROR
		log4cxx::LoggerPtr logger;

		ACU_INT		dsp_id;		//!< stream's owner DSP ID
		DSPModule	*pModule;	//!< pointer to DSPModule owner
		ACU_INT stream_id;		//!< ID of TiNG module stream on ProsodyX-1U stream ID is:64,65,66,67,68,69,70,71 - for first 4 DSPs, and 80,81,82,83,84,85,86,87
		std::string channels;	//!< string marked with current status of DSP channel
	public:
		const static unsigned char free			= ' ';
		const static unsigned char allocated	= 'A';

		DSPStream(DSPModule *p_module, ACU_INT module, ACU_INT stream_id);		//!< @brief clear all channels with a free status
		/**
		 * @brief return number of free channels
		 */
		ACU_INT getFreeChannels();
		/**
		 * @brief return shared_ptr to new DSPPool object, mark channels array with a allocated signs
		 * @details If resource occupation failed (for example not enough solid sequence with enough length),
		 * DSPPool initStatus will be failed
		 * @param	[in]	number of allocated channels
		 * @return	shared_ptr to new DSPPool object.
		 */
		boost::shared_ptr<DSPPool> allocate(int numChannels);
		/**
		 * @brief free previously allocated DSP channels
		 * @param startChannel	start channel index
		 * @param numChannels	number of channels
		 */
		void release(ACU_INT startChannel, ACU_INT numChannels);

		/**
		 * @return internal index of owner DSP module
		 */
		ACU_INT getModuleIdx() {return dsp_id;};
		/**
		 * @return pointer to owner DSP module
		 */
		DSPModule* getModule() {return pModule;};
		/**
		 * @return index of stream
		 */
		ACU_INT getStreamId() {return stream_id;};
		/**
		 * @return string representation of DSP stream for debugging
		 */
		std::string toString();
	};



/**
 * @class DSPpool card.h
 * @brief quantum of DSP resource allocated for call
 * @details actually it represent 3 parameters:
 * 		- DSP module consequent number (index)
 * 		- DSP stream ID.
 * 			Each stream consist of channel or TiNG timeslots.
 * 			Each module has some quantity of streams (it may be a DSP module CPU core, I don't now)
 * 			each stream has it unique integer ID. For example, on ProsodyX-1U stream ID is:
 * 			64,65,66,67,68,69,70,71 - for first 4 DSPs, and 80,81,82,83,84,85,86,87 - for 4 DSP,
 * 			that is placed on AC5270 extension module. Total max 8 DSPs, and 16 streams.
 * 			Strange, but in Aculab examples, only odd (not even) streams is used for channel (timeslot)
 * 			allocation.
 * 		- TiNG channel index.
 * 			This entities is responsible for single voice stream termination and processing in TiNG
 * 			modules.
 * 		- Destruction DSP pools destruct automatically. In proccess of destruction they are release
 * 			DSPStream resources
 */
	class DSPPool {
		friend class DSPModuleHolder; // for accessing to DSPStream directly

		bool initStatus;		//!< true - initialization OK, false - ERROR

		ACU_INT dsp_id;			//!< DSP module consequent number (index)
		DSPStream* pStream;		//!< pointer to parent DSP stream
		ACU_INT startChannel;	//!< TiNG 1st channel in sequence
		ACU_INT numChannels;	//!< number of channels
	public:
		/**
		 * @brief occupied some pool of channels in DSP module stream
		 * @param	[in]	numChannels	number of consequent channels, allocated
		 */
		DSPPool(ACU_INT dsp_id, DSPStream* ps, ACU_INT startChannel, ACU_INT numChannels);
		/**
		 * @brief releases DSPStream resources
		 */
		~DSPPool();
		bool getStatus() {return initStatus;};
		DSPStream* getDSPStream() {return pStream;};
		ACU_INT getDSPModuleIdx() {return pStream->getModuleIdx();};
		ACU_INT getStartChannel() {return startChannel;};
		ACU_INT getNumChannels() {return numChannels;};

		void setStatus(bool status) {initStatus = status;};
		void setStartChannel(ACU_INT start) {startChannel = start;}
		void setNumChannels(ACU_INT num) {numChannels = num;}
		/**
		 * @brief free DSP resource (using DSPStream* pStream), negate iniStatus
		 */
		void release();
		std::string toString();
	};

/**
 * @class DSPModule card.h
 * @brief DSP resource placed on card
 * @details Initialize DSP resources
 */
	class DSPModule {
		ACU_INT				module;			//!< module index number (in order, returned sm_get_card_info())
		tSMModuleId			module_id;		//!< Aculab TiNG module data structure
		tSMVMPTxToneSetId	tone_set_id;

		bool initStatus;						//!< DSP module initialization status. false, if it's not initialized correctly
		SM_OPEN_MODULE_PARMS	sOpenModule;	//!< data structure for module initialization.
		SM_VMPTX_CREATE_TONESET_PARMS sm_vmptx_toneset; //!< Toneset for FAX functions

		log4cxx::LoggerPtr logger;	// log initialized based on cfg data

	public:
		static const unsigned int MAX_STREAMS_PER_DSP = 2;
		boost::shared_ptr<DSPStream> pStreams[MAX_STREAMS_PER_DSP];		//!< each DSP has the 2 TiNG streams. I.e. 2*128 = 256 channels

		/**
		 * @brief initialize DSP module on card
		 * @details
		 * 	- open DSP module
		 * 	- creates toneset for FAX processing setup
		 * 	- initializes each of 2 TiNG streams
		 * @param mIndex	consequent index {0,1,...}
		 * @param cardId	Integer Card ID
		 */
		DSPModule(ACU_INT mIndex, ACU_INT cardId);
		~DSPModule();
		/**
		 * @brief	returns initialization status
		 * @return	initialization status. true - if OK, false in case of ERRORs
		 */
		bool getStatus() { return initStatus; };
		/**
		 * @brief	return integer module consequent ID
		 */
		ACU_INT getModuleIdx() { return module; };
		/**
		 * @brief	returns pointer to internal module structure in drivers
		 */
		tSMModuleId getTSMModuleId() { return module_id; };

	};


/**
 * @class DSPModuleHolder card.h
 * @brief Holder DSP resource placed on card. It utilizes all DSP resources of card
 * @details
 * 	- contains DSPModules
 * 	- allocates and destroys DSP pools (TiNG streams)
 */
		class DSPModuleHolder {
			bool initStatus;			//!< card status (false if card not present, initialized or not support FAX features)
			ACU_INT cardId;				//!< internal ProsodyX Card ID (not consequent index)
			log4cxx::LoggerPtr logger;	//!< log initialized based on cfg data

			boost::mutex moduleHolderMutex;	//!< mutex for locking allocate operation

			std::vector< boost::shared_ptr<DSPModule> > modules;//!< DSP modules. Vector of boost::shared_ptr
			std::vector< boost::shared_ptr<DSPPool> > pools;	//!< DSP pools (group of TiNG stream channels)
																//!< allocated by manager

			//ACU_OPEN_PROSODY_PARMS		rOpenSpeech;	//!< data structure for speech (fax) functionality initialization
			SM_CARD_INFO_PARMS			sCardInfo;		//!< data about TiNG (DSP resources)
		public:
			/**
			 * @brief DSP subsystem on specific card
			 * @param card_id integer card id. It's getting from Aculab driver subsystem during initialization
			 */
			DSPModuleHolder(ACU_INT card_id);
			~DSPModuleHolder();
			bool getStatus() { return initStatus; };

			/**
			 * @brief allocate DSP resource (using DSPStream pointer in DSPPool).
			 * Adding DSPPool to std::vector< boost::shared_ptr<DSPPool> > pools
			 * Thread safe.
			 */
			boost::shared_ptr<DSPPool> allocate(int numChannels);
			/**
			 * @brief free DSP resource (using DSPStream pointer in DSPPool).
			 * Delete DSPPool from local vector.
			 * Thread safe.
			 */
			void release(boost::shared_ptr<DSPPool> pPool);
		};

} // END namespace


#endif /* DSP_MGMT_H_ */
