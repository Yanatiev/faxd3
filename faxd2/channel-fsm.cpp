#include "channel-fsm.h"
#include <sstream>
#include <iomanip>

using std::vector;
using namespace log4cxx;
using namespace faxd;


Channel::Channel(boost::shared_ptr<DSPModuleHolder> p_dspholder, Port* p_port, int ts) :
		FSM<States, Events, Channel>(STATE_START),
		logger(Logger::getLogger( "main")),
		pPort(p_port),
		fax(this),
		spDSPHolder(p_dspholder),
		ts(ts)
{
	port_idx	= p_port->port;		// port consequent number
	port_id		= p_port->port_id;	// port internal ID
	stream		= pPort->port + 32;	// !!! stream index in communication Matrix !!!
	/*
	 *  allocate DSP resource for channel
	 *  In fact for each TDM channel we need allocate one TiNG channel
	 *  When call is processed, these channels are switched to each other
	 */
	spChannelDSP = spDSPHolder->allocate(1);
	if( spChannelDSP->getStatus() ) { // if status true
		dspmodule_idx		= spChannelDSP->getDSPModuleIdx();								// DSP Module consequent number
		TiNG_module_id		= spChannelDSP->getDSPStream()->getModule()->getTSMModuleId();	// opened DSP module data structure
		tingstream_id		= spChannelDSP->getDSPStream()->getStreamId();		// TiNG stream id: 64,...
		tingstream_start_channel	= spChannelDSP->getStartChannel();			// TiNG offset channel in stream
		tingstream_num_channel		= spChannelDSP->getNumChannels();			// TiNG number of allocated channel in stream
		LOG4CXX_DEBUG (logger, "card s/n:" << pPort->cardSerialNo  << " port:" << pPort->port << ":" << ts << ". Allocate DSP channel - SUCCESS.");
		LOG4CXX_TRACE (logger, "card s/n:" << pPort->cardSerialNo  << " port:" << pPort->port << ":" << ts << "Channel::Channel(...). Allocate DSP resource - SUCCESS." << toString() << spChannelDSP->toString());
	} else {
		dspmodule_idx		= 0;				// DSP Module consequent number
		TiNG_module_id		= NULL;				// opened DSP module data structure
		tingstream_id		= 0;				// TiNG stream id: 64,...
		tingstream_start_channel	= 0;		// TiNG offset channel in stream
		tingstream_num_channel		= 0;		// TiNG number of allocated channel in stream
		LOG4CXX_DEBUG (logger, "card s/n:" << pPort->cardSerialNo  << " port:" << pPort->port << ":" << ts <<
				". Allocate DSP channel - FAILED.");
	}

	/*
	 * stuffing transition table. Look fsm.h
	 */
	addTransition(STATE_START,					EVENT_RX_INIT,				STATE_RX_INIT,				&Channel::actionOpenIn);
	addTransition(STATE_RX_INIT,				EVENT_WAIT_FOR_INCOMING,	STATE_RX_WAIT,				&Channel::actionNoop);
	addTransition(STATE_RX_WAIT,				EVENT_INCOMING_CALL_DET,	STATE_RX_INCOMING_DETECTED,		&Channel::actionCallDetails);
	addTransition(STATE_RX_WAIT,				EVENT_INCOMING_CALL_DET,	STATE_RX_INCOMING_DETECTED,		&Channel::actionSendRinging);
	addTransition(STATE_RX_INCOMING_DETECTED,	EVENT_WAIT_FOR_ACCEPT,		STATE_RX_WAIT_FOR_ACCEPT,	&Channel::actionCallAccept);
	addTransition(STATE_RX_WAIT_FOR_ACCEPT,		EVENT_CONNECTED,			STATE_CONNECTED,			&Channel::actionRxFax);
	addTransition(STATE_CONNECTED,				EVENT_REMOTE_DISCONNECT,	STATE_REMOTE_DISCONNECT,	&Channel::actionDisconnect);
	addTransition(STATE_REMOTE_DISCONNECT,		EVENT_IDLE,					STATE_RX_INIT,					&Channel::actionRelease);
	addTransition(STATE_REMOTE_DISCONNECT,		EVENT_IDLE,					STATE_RX_INIT,					&Channel::actionOpenIn);
	// cleanup after EXIT signal
	addTransition(STATE_START,					EVENT_EXIT,					STATE_EXIT,					&Channel::actionNoop);
	addTransition(STATE_RX_INIT,				EVENT_EXIT,					STATE_EXIT,					&Channel::actionRelease);
	addTransition(STATE_RX_WAIT,				EVENT_EXIT,					STATE_EXIT,					&Channel::actionRelease);
	addTransition(STATE_RX_INCOMING_DETECTED,	EVENT_EXIT,					STATE_EXIT,					&Channel::actionRelease);
	addTransition(STATE_RX_WAIT_FOR_ACCEPT,		EVENT_EXIT,					STATE_EXIT,					&Channel::actionRelease);
	addTransition(STATE_REMOTE_DISCONNECT,		EVENT_EXIT,					STATE_EXIT,					&Channel::actionRelease);
	addTransition(STATE_CONNECTED,				EVENT_EXIT,					STATE_EXIT,					&Channel::actionDisconnect);
	addTransition(STATE_CONNECTED,				EVENT_EXIT,					STATE_EXIT,					&Channel::actionRelease);

	/*
	 * logger preambula
	 */
	std::stringstream ssHeader;
	ssHeader << "card s/n:" << pPort->cardSerialNo <<	" port:" << port_idx << ":" << ts << " CHANNEL - ";
	logHeader_ = ssHeader.str();
};


void Channel::actionNoop() {};


void Channel::actionOpenIn() {
	int				rc;
	IN_XPARMS		cOpenIn;	// structure for call_openin()

	INIT_ACU_CL_STRUCT(&cOpenIn);
	cOpenIn.net = port_id;							// port ID
	cOpenIn.ts  = ts;								// timeslot number
	cOpenIn.queue_id 			= pPort->queue_id; 	// port queue ID
	cOpenIn.app_context_token	= convertToHandler(); // convert channel pointer to Aculab handler
	cOpenIn.cnf = CNF_REM_DISC; // look Call_control_API_guide.pdf pg.50
	/*
	 * turning on call in functionality on port. create special call handler
	 * now call to this timeslot is associated with our channel object in Aculab drivers.
	 * If successful, the value in the handle field will contain a unique call identifier. This value must be used
	 * for all subsequent operations relating to this call. The call handle supplied by the driver will always be non-zero
	 */
	rc = call_openin(&cOpenIn);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "FSM actionOpenIn() -> call_openin() ERROR " << acu_error_desc(rc));
	} else {
		handle		= cOpenIn.handle;		// call descriptor bounded to channel
		LOG4CXX_DEBUG (logger, logHeader_ << " FSM actionOpenIn() -> call_openin(). New call handler OK");
	}
};


void Channel::actionCallDetails() {
	int							rc;
	DETAIL_XPARMS				cCallDetails;

	INIT_ACU_CL_STRUCT(&cCallDetails);
	cCallDetails.handle = handle;		// channel descriptor from driver side
	cCallDetails.timeout = 500;			// Call control API Guide "Not used in V6, but retained in structure for historic reasons"
	rc = call_details(&cCallDetails);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "FSM actionCallDetails(). ERROR " << rc);
	}

	// get phone numbers
	ani  = cCallDetails.originating_addr;
	dnis = cCallDetails.destination_addr;
	LOG4CXX_DEBUG (logger, logHeader_ << "FSM actionCallDetails() -> call_details()\tANI:" << ani << " DNIS:" << dnis);
};


void Channel::actionSendRinging() {
	int							rc;
	INCOMING_RINGING_XPARMS		in_ringing;

	INIT_ACU_STRUCT(&in_ringing);
	in_ringing.handle = handle;
	rc = xcall_incoming_ringing(&in_ringing);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "FSM actionSendRinging() -> xcall_incoming_ringing() " << ". ERROR " << rc);
	} else {
		LOG4CXX_DEBUG (logger, logHeader_ << "FSM actionSendRinging() -> xcall_incoming_ringing() " << ". OK");
	}
};


void Channel::actionCallAccept() {
	int rc;
	ACCEPT_XPARMS accept_xparms;

	INIT_ACU_STRUCT(&accept_xparms);
	accept_xparms.handle = handle;
	rc = xcall_accept(&accept_xparms);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "FSM actionCallAccept() -> xcall_accept(). ERROR " << rc);
	} else {
		LOG4CXX_DEBUG (logger, logHeader_ << "FSM actionCallAccept() -> xcall_accept(). OK");
	}
};


void Channel::actionDisconnect() {
	int				rc;
	CAUSE_XPARMS	cCallDisconnect;

	/*
	 * break FAX session if it running
	 */
	LOG4CXX_DEBUG (logger, logHeader_ << "FSM actionDisconnect() -> fax.Interrupt()");
	fax.interrupt();

	/*
	 * wait for worker thread to terminate
	 */
	spWorkerThread->join();
	LOG4CXX_DEBUG (logger, logHeader_ << "FSM actionDisconnect() -> FAX worker thread FINISHED");

	/*
	 *  disconnect call
	 */
	INIT_ACU_STRUCT(&cCallDisconnect);
	cCallDisconnect.handle = handle;
	// После этого обязательно запустить call_release для возврата десриптора драйверу
	rc = call_disconnect(&cCallDisconnect);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "FSM actionDisconnect() -> call_disconnect(). ERROR " << rc);
	} else {
		LOG4CXX_INFO (logger, logHeader_ << "FSM actionDisconnect() -> call_disconnect(). OK");
	}
};


void Channel::workerRx() {
	SPEECH_AllocateResource();
	SPEECH_SwitchTDMtoResource();		// CONNECT Port stream(port+32)/ts <-> TiNG stream/TiNG ts

	fax.receiveFAXCallFlow();

	SPEECH_UnSwitchTDMfromResource();	// DISCONNECT Port stream(port+32)/ts <-> TiNG stream/TiNG ts
	SPEECH_ReleaseResource();
};



void Channel::actionRelease() {
	/*
	 * release call handler
	 */
	int rc;
	CAUSE_XPARMS	cCallRelease;
	INIT_ACU_CL_STRUCT(&cCallRelease);
	cCallRelease.handle = handle;

	rc = call_release(&cCallRelease);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "FSM actionRelease() -> call_release() " << acu_error_desc(rc));
	} else {
		LOG4CXX_DEBUG (logger, logHeader_ << "FSM actionRelease() -> call_release(). OK");
		// idle_net_ts() only in case of good call release, because if card is goes down, there will be a problem
		idle_net_ts(port_id, ts);
	}


};

void Channel::actionOpenOut() {};


void Channel::actionRxFax() {
	spWorkerThread.reset(new boost::thread(boost::bind(&Channel::workerRx, this)));
	LOG4CXX_DEBUG (logger, logHeader_ << "FSM actionRxFax() -> FAX RX worker thread STARTED");
};

void Channel::actionTxFax(){ };


void Channel::SPEECH_AllocateResource() {
	int	rc;
	SM_CHANNEL_ALLOC_PLACED_PARMS		sChannelParms;

	/*
	 * TiNG_channel_id used for connect TDM channel to DSP channel
	 * 1. TiNG_channel_id allocated with sm_channel_alloc_placed() <- TiNG_module_id
	 * 2. sm_switch_channel_output() and sm_switch_channel_input() <- TiNG_channel_id, tingstream_id, tingstream_start_channel
	 * 3. sw_set_output() DSP stream:channel -> TDM stream:channel
	 * 4. sw_set_output() TDM stream:channel -> DSP stream:channel
	 */
	INIT_ACU_SM_STRUCT(&sChannelParms);
	sChannelParms.type = kSMChannelTypeFullDuplex;
	sChannelParms.module = TiNG_module_id;	 //pModuleData[CallData->module].module_id;
	rc=sm_channel_alloc_placed(&sChannelParms);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << " Channel::SPEECH_AllocateResource() ERROR:" << acu_error_desc(rc));
	} else {
		LOG4CXX_DEBUG (logger, acu_error_desc(rc) << " Channel::SPEECH_AllocateResource() OK");
	}
	TiNG_channel_id = sChannelParms.channel;

};


void Channel::SPEECH_SwitchTDMtoResource(){

	int							rc;
	SM_SWITCH_CHANNEL_PARMS		sSwitchParms;
	OUTPUT_PARMS				sOutputParms;

	/*
	 * TiNG_channel_id used for connect TDM channel to DSP channel
	 * 1. TiNG_channel_id allocated with sm_channel_alloc_placed() <- TiNG_module_id
	 * 2. sm_switch_channel_output() and sm_switch_channel_input() <- TiNG_channel_id, tingstream_id, tingstream_start_channel
	 * 3. sw_set_output() DSP stream:channel -> TDM stream:channel
	 * 4. sw_set_output() TDM stream:channel -> DSP stream:channel
	 */

	/* Assign Channel Stream and Timeslot for Output */
	INIT_ACU_SM_STRUCT(&sSwitchParms);
	sSwitchParms.channel	= TiNG_channel_id;
	sSwitchParms.st			= tingstream_id;  			//CallData->TiNG_stream;
	sSwitchParms.ts			= tingstream_start_channel; // CallData->TiNG_timeslot;

	if( 0 == cfg::Companding ) //ALAW
		sSwitchParms.type	= kSMTimeslotTypeALaw;
	else
		sSwitchParms.type	= kSMTimeslotTypeMuLaw;

	// assign timeslot to a channel
	// http://www.aculab.com/support/pdf_documents/v6_linux/TiNG/pubdoc/gen/apifn-sm_switch_channel_output.html#field-channel
	rc = sm_switch_channel_output(&sSwitchParms); // OUT
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "SPEECH sm_switch_channel_output() TiNG stream:" << tingstream_id << ":" << tingstream_start_channel << " ERROR:" << acu_error_desc(rc));
	} else {
		LOG4CXX_DEBUG (logger, logHeader_ << "SPEECH sm_switch_channel_output() TiNG stream:" << tingstream_id << ":" << tingstream_start_channel << " OK");
	}
	rc = sm_switch_channel_input(&sSwitchParms);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "SPEECH sm_switch_channel_input() TiNG stream:" << tingstream_id << ":" << tingstream_start_channel << " ERROR:" << acu_error_desc(rc));
	} else {
		LOG4CXX_DEBUG (logger, logHeader_ << "SPEECH sm_switch_channel_input() TiNG stream:" << tingstream_id << ":" << tingstream_start_channel << " OK");
	}

	/* Connect the Channel for Output (Playback) */
	INIT_ACU_STRUCT(&sOutputParms);
	sOutputParms.ist = tingstream_id; // CallData->TiNG_stream;
	sOutputParms.its = tingstream_start_channel; // CallData->TiNG_timeslot;
	sOutputParms.ost = stream;
	sOutputParms.ots = ts;
	sOutputParms.mode = CONNECT_MODE;
	rc = sw_set_output(pPort->getCardId(), &sOutputParms);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "SPEECH sw_set_output() CONNECT TiNG " << tingstream_id << ":" << tingstream_start_channel << " -> DSP " << stream << ":" << ts << " ERROR:" << rc);
	} else {
		LOG4CXX_DEBUG (logger, logHeader_ << "SPEECH sw_set_output() CONNECT TiNG " << tingstream_id << ":" << tingstream_start_channel << " -> DSP " << stream << ":" << ts << " OK");
	}

	/* Connect the Call to the Channel for Input (DTMF) */
	INIT_ACU_STRUCT(&sOutputParms);
	sOutputParms.ist = stream;
	sOutputParms.its = ts;
	sOutputParms.ost = tingstream_id;
	sOutputParms.ots = tingstream_start_channel;
	sOutputParms.mode = CONNECT_MODE;
	rc = sw_set_output(pPort->getCardId(), &sOutputParms);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "SPEECH sw_set_output() CONNECT TiNG " << tingstream_id << ":" << tingstream_start_channel << " <- DSP " << stream << ":" << ts << " ERROR:" << rc);
	} else {
		LOG4CXX_DEBUG (logger, logHeader_ << "SPEECH sw_set_output() CONNECT TiNG " << tingstream_id << ":" << tingstream_start_channel << " <- DSP " << stream << ":" << ts << " OK");
	}
};


void Channel::SPEECH_UnSwitchTDMfromResource(){
	int				rc;
	OUTPUT_PARMS	sOutputParms;

	/* Disable Switch Connection */
	INIT_ACU_STRUCT(&sOutputParms);
	sOutputParms.ost = tingstream_id;
	sOutputParms.ots = tingstream_start_channel;
	sOutputParms.mode = DISABLE_MODE;
	rc = sw_set_output(pPort->getCardId(), &sOutputParms);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "SPEECH sw_set_output() DISCONNECT TiNG " << tingstream_id << ":" << tingstream_start_channel << " ERROR:" << rc);
	} else {
		LOG4CXX_DEBUG (logger, logHeader_ << "SPEECH sw_set_output() DISCONNECT TiNG " << tingstream_id << ":" << tingstream_start_channel << " OK");
	}

	/* Disable Switch Connection */
	INIT_ACU_STRUCT(&sOutputParms);
	sOutputParms.ost = stream;
	sOutputParms.ots = ts;
	sOutputParms.mode = DISABLE_MODE;
	rc = sw_set_output(pPort->getCardId(), &sOutputParms);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "SPEECH sw_set_output() DISCONNECT DSP " << stream << ":" << ts << " ERROR:" << rc);
	} else {
		LOG4CXX_DEBUG (logger, logHeader_ << "SPEECH sw_set_output() DISCONNECT DSP " << stream << ":" << ts << " OK");
	}
};


void Channel::SPEECH_ReleaseResource(){
	int	rc;

	rc=sm_channel_release(TiNG_channel_id);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "SPEECH sm_channel_release() ERROR:" << rc);
	} else {
		LOG4CXX_DEBUG (logger, logHeader_ << "SPEECH sm_channel_release() OK");
	}
};



std::string Channel::toString() {
	std::stringstream ss;

//	ACU_INT port_idx;					// port consequent number
//	ACU_PORT_ID port_id;				// port internal ID
//	ACU_INT dspmodule_idx;				// DSP Module consequent number
//	tSMModuleId dspmodule_id;			// opened DSP module data structure
//	ACU_INT tingstream_id;				// TiNG stream id: 64,...
//	ACU_INT tingstream_start_channel;	// TiNG offset channel in stream
//	ACU_INT tingstream_num_channel;		// TiNG number of allocated channel in stream
	ss << "\n\t\t\t Port index=" << port_idx << "; Port ID=" << port_id << "; Timeslot=" << ts;
	ss << "\n\t\t\t DSP index=" << dspmodule_idx << "; TiNG stream ID=" << tingstream_id <<
			"; TiNG stream start channel=" << tingstream_start_channel <<
			"; TiNG stream number of channels=" << tingstream_num_channel;
	return ss.str();
};
