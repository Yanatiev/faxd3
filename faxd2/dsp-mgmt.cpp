/**
  dsp-mgmt.cpp
  Purpose: operations with Aculab DSP resources (modules)
  	  - initialize modules
  	  - reserve resources (TiNG streams)

	@author: yanat
	@version: 1.04 20/02/2014
 */


#include "dsp-mgmt.h"
#include <algorithm>
#include <sstream>
// C includes

using namespace log4cxx;
using namespace std;
using namespace faxd;

/*****************************************************************************\
 *
 *                       class DSPStream
 *
\*****************************************************************************/

DSPStream::DSPStream(DSPModule *p_module, ACU_INT module, ACU_INT stream_id) :
	initStatus(true),
	logger(Logger::getLogger( "main")),
	dsp_id(module),
	pModule(p_module),
	stream_id(stream_id),
	channels(Config::TING_MAX_CHANNELS_PER_STREAM, free)	// 128 white spaces
{};

ACU_INT DSPStream::getFreeChannels() {
	// counting all empty channels (character ' ')
	return std::count(channels.begin(), channels.end(), (unsigned char)free);
};

boost::shared_ptr<DSPPool> DSPStream::allocate(int numChannels) {
	// create new shared_ptr<DSPPool>
	LOG4CXX_DEBUG (logger, "DSPStream::allocate(). trying to allocate " << numChannels << " channels");

	boost::shared_ptr<DSPPool> pPool(new DSPPool(dsp_id, this, 0, numChannels) );
	// try to found empty consequent channels with enough quantity
	std::string reqFreeSpace(numChannels, free);
	size_t offset = channels.find(reqFreeSpace, 0); // try to find since start to end
	if(offset == string::npos) {
		pPool->setStatus(false); // reset status of DSPPool - pool allocation failed
		pPool->setNumChannels(0);
		LOG4CXX_DEBUG (logger, "TiNG stream " << stream_id << " allocation of " << numChannels << " channels FAILED");
	} else {
		pPool->setStatus(true);			// set status OK
		pPool->setStartChannel(offset);	// set correct offset
		string allocatedStr(numChannels, allocated);		// prepare string with allocated sign
		channels.replace(offset, numChannels, allocatedStr);// replace channels fragment by them
		LOG4CXX_TRACE (logger, "TiNG stream allocation map: " << toString());
		LOG4CXX_DEBUG (logger, "TiNG stream: " << stream_id << ". channels base: " << pPool->getStartChannel() <<
				" allocated:" << numChannels << " SUCCESS");
	}
	return pPool;
};

void DSPStream::release(ACU_INT startChannel, ACU_INT numChannels) {
	// prepare string with empty channels flag, with required length
	string releaseStr(numChannels, free);
	// clear flags in channels string, using replace functionality
	channels.replace(startChannel, numChannels, releaseStr);
	LOG4CXX_TRACE (logger, "DSPStream::release(). start channel: " << startChannel << " number: " << numChannels <<
			" TiNG stream allocation map: " << toString());
};

std::string DSPStream::toString() {
	stringstream ss;
	ss << "\n\t\t owner DSP: dsp_id = " << dsp_id <<
			"\n\t\t stream_id = " << stream_id <<
			"\n\t\t channels = [" << channels << "]";
	return ss.str();
};
/*****************************************************************************\
 *
 *                       class DSPPool
 *
\*****************************************************************************/
DSPPool::DSPPool(ACU_INT dsp_id, DSPStream* ps, ACU_INT startChannel, ACU_INT numChannels) :
		initStatus(false),
		dsp_id(dsp_id),
		pStream(ps),
		startChannel(startChannel),
		numChannels(numChannels)
{};

DSPPool::~DSPPool() {
	// release previously allocated channels on TiNG stream
	pStream->release(startChannel, numChannels);
};

std::string DSPPool::toString() {
	stringstream ss;
	ss << "\n\t\t\tDSPPool Info: Owner DSP ID - " << dsp_id << " Stream ID - " << pStream->getStreamId() <<
			" Channel offset - " << startChannel << " Num channels - " << numChannels;
	return ss.str();
};


/*****************************************************************************\
 *
 *                       class DSPModule
 *
\*****************************************************************************/
DSPModule::DSPModule(ACU_INT mIndex, ACU_INT cardId) :
		initStatus(false),
		logger(Logger::getLogger( "main"))
{
	ACU_ERR	rc;		// return code

	/*
	 * Open DSP module
	 */
	INIT_ACU_SM_STRUCT(&sOpenModule);	// zeroize struct
	sOpenModule.module_ix	= mIndex;	// его порядковый номер с 0
	sOpenModule.card_id		= cardId;	// CardID, where module is placed
	rc = sm_open_module(&sOpenModule);	// open module (DSP processor)
	if(rc) {
		LOG4CXX_ERROR (logger, "Card ID:" << cardId << "\tDSP:" << mIndex << " sm_open_module() ERROR:" << rc);
		return;
	} else {
		initStatus = true;
		LOG4CXX_INFO (logger, "Card ID:" << cardId << "\tDSP:" << mIndex << " sm_open_module() OK");
	}

	module		= mIndex;					// index
	module_id	= sOpenModule.module_id;	// pointer to struct with module info

	/*
	 * creates toneset for FAX processing setup
	 */
	INIT_ACU_SM_STRUCT(&sm_vmptx_toneset);	// empty toneset structure
	sm_vmptx_toneset.toneset = kSMVMPTxDefaultToneSet;	// init with default toneset
	sm_vmptx_toneset.module = sOpenModule.module_id;	// current module structure
	rc = sm_vmptx_create_toneset(&sm_vmptx_toneset);
	if(rc) {
		LOG4CXX_ERROR (logger, "Card ID:" << cardId << "\tDSP:" << mIndex << " sm_vmptx_create_toneset() ERROR:" << rc);
	} else {
		initStatus = true;
		LOG4CXX_INFO (logger, "Card ID:" << cardId << "\tDSP:" << mIndex << " sm_vmptx_create_toneset() OK");
	}
	tone_set_id = sm_vmptx_toneset.tone_set_id; // set received toneset structure to current module toneset

	/*
	 * initializes each of 2 TiNG streams
	 */
	ACU_INT TiNGBaseStream = (module < 4) ? kSW_LI_PX_TiNG1_0 : kSW_LI_PX_TiNG5_0;
	//pStreams[0] = boost::make_shared<DSPStream>(logger, module, TiNGBaseStream + (module*2));		// 64 66 68 70 80 82 84 86
	//pStreams[1] = boost::make_shared<DSPStream>(logger, module, TiNGBaseStream + (module*2) + 1);	// 65 67 69 71 81 83 85 87
	pStreams[0].reset( new DSPStream( this, module, TiNGBaseStream + (module*2)) );
	LOG4CXX_DEBUG (logger, "pStream[0]" << pStreams[0]->toString());
	pStreams[1].reset( new DSPStream( this, module, TiNGBaseStream + (module*2) + 1 ) );
	LOG4CXX_DEBUG (logger, "pStream[1]" << pStreams[1]->toString());
} //END DSPModule::DSPModule(log4cxx::LoggerPtr &l, ACU_INT mIndex, ACU_INT cardId

DSPModule::~DSPModule() {
	//TODO close module. If it need.
	LOG4CXX_INFO (logger, "Card ID:" << sOpenModule.card_id << "\tDSP:" << sOpenModule.module_ix << " module is CLOSED");
}


/*****************************************************************************\
 *
 *                       class DSPModuleHolder methods
 *
\*****************************************************************************/

DSPModuleHolder::DSPModuleHolder(ACU_INT card_id) :
		initStatus(false),
		cardId(card_id),
		logger(Logger::getLogger( "main"))
{
//	/*
//	 * Open Speech Driver Interface. FAX belongs to speech resources
//	 * Speech interface belongs to DSP subsystem
//	 */
//	INIT_ACU_STRUCT(&rOpenSpeech);
//	rOpenSpeech.card_id = cardId;
//	ACU_INT rc = acu_open_prosody(&rOpenSpeech);
//	if(rc) {
//		LOG4CXX_ERROR (logger, "Card ID:" << cardId << "\tDSP acu_open_prosody() ERROR: " << rc);
//		return; // exitting, initStatus still = false
//	} else {
//		LOG4CXX_INFO (logger, "Card ID:" << cardId << "\tDSP acu_open_prosody(). OK. Speech driver interface initialized.");
//	}

	/*
	 * Get Speech Module Count
	 */
	INIT_ACU_SM_STRUCT(&sCardInfo);
	sCardInfo.card = cardId;
	ACU_INT rc = sm_get_card_info(&sCardInfo);
	if(rc) {
		LOG4CXX_ERROR (logger, "Card ID:" << cardId << "\tDSP sm_get_card_info() ERROR:" << rc);
		return; // exiting, initStatus still = false
	} else {
		LOG4CXX_INFO (logger, "Card ID:" << cardId << "\tDSP sm_get_card_info() OK. Number of DSPs:" << sCardInfo.module_count);
	}

	/*
	 * Initialize DSP resources and push DSP modules to vector
	 */
	for(int i=0; i<sCardInfo.module_count; i++) { // for all DSP modules
		boost::shared_ptr<DSPModule> pMod( new DSPModule(i, cardId) ); // create new shared_ptr on module
		if(pMod->getStatus()) {	// if module initialization was successful
			initStatus = true;	// DSPModule holder works, if it has at least 1 working DSPModule
			modules.push_back(pMod);
			LOG4CXX_DEBUG (logger, "Card ID:" << cardId << "DSP module (index number: " << i << ") is added to vector of port modules");
		}
	}

	if (true == initStatus) {
		LOG4CXX_INFO (logger, "Card ID:" << cardId << "\tDSP subsystem initialization. SUCCESS\n");
	} else {
		LOG4CXX_ERROR (logger, "Card ID:" << cardId << "\tDSP subsystem initialization. FAILED\n");
	}
};


DSPModuleHolder::~DSPModuleHolder() {
	LOG4CXX_DEBUG (logger, "DSPModuleHolder::~DSPModuleHolder(). Active pools =  " << pools.size() <<
			". Card ID: " << cardId << " is destroyed");
};

boost::shared_ptr<DSPPool> DSPModuleHolder::allocate(int numChannels) {
	/*
	 * scope of locking - all method body
	 * When locked, we can't allocate and release DSP resources
	 * because they are changed
	 */
	boost::mutex::scoped_lock lock(moduleHolderMutex);

	/*
	 * defining DSPStream with smallest amount of allocated resources
	 */
	std::vector< boost::shared_ptr<DSPModule> >::iterator iModule;
	boost::shared_ptr<DSPStream> pSmallestStream;
	pSmallestStream = (*modules.begin())->pStreams[0];

	for(iModule = modules.begin(); iModule != modules.end(); ++iModule) // thru all modules
		for(unsigned int i=0; i<cfg::streamsPerDSP ;i++)	// and all streams on each one (2)
			// if next module has more free channels than previous
			if( (*iModule)->pStreams[i]->getFreeChannels() >  pSmallestStream->getFreeChannels() )
				pSmallestStream = (*iModule)->pStreams[i];

	/*
	 * allocating
	 */
	boost::shared_ptr<DSPPool> tmp = pSmallestStream->allocate(numChannels);
	LOG4CXX_TRACE (logger, "DSPModuleHolder::allocate() allocate DSP pool: " << tmp->toString() << " status - " << tmp->getStatus());
	return tmp;
};

void DSPModuleHolder::release(boost::shared_ptr<DSPPool> pPool) {
	/*
	 * scope of locking - all method body
	 * When locked, we can't allocate and release DSP resources, because they are changed
	 */
	boost::mutex::scoped_lock lock(moduleHolderMutex);

	/*
	 * release DSPStream resource
	 */
	pPool->pStream->release(pPool->getStartChannel(), pPool->getNumChannels());

	/*
	 * remove this DSPPool object from DSPModuleHolder vector
	 */
	std::vector< boost::shared_ptr<DSPPool> >::iterator it;
	it = std::find(pools.begin(), pools.end(), pPool);
	if ( it != pools.end() ) { // found, it referred to element for deletion
		LOG4CXX_DEBUG (logger, "remove DSP pool: " << (*it)->toString());
		pools.erase(it);
	}

};
