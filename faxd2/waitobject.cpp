/*
 * waitobject.cpp
 *
 *  Created on: Feb 10, 2014
 *      Author: root
 */
#include <sstream>
#include "waitobject.h"
#include <string.h>		// memcpy
#include <unistd.h>		// close file descriptors
#include <errno.h>		// errno

using namespace log4cxx;
using namespace faxd;

/*****************************************************************************\
 *
 *                       class WaitObject
 *
\*****************************************************************************/

WaitObject::WaitObject() :
		logger(Logger::getLogger( "main")),
		initStatus(false),
		signalled(ACU_OS_FALSE),
		handle_is_copy(ACU_OS_FALSE) {
	int rc;

	rc = pipe(event_fds); // create pipe
	if (rc == 0) { // if rc==0 -> success
		// first (input file descriptor) descriptor (for reading) put in structure for poll()
		event_pfd.fd = event_fds[PIPE_READ];
		// input param для poll(), bit mapped events, which is need to be waited (if 0 - all events ignored and revent returns 0)
		event_pfd.events = POLLIN; // POLLIN - data for reading is present
		event_pfd.revents = 0;	// events, returned by the kernel
		initStatus = true;
		LOG4CXX_DEBUG(logger, toString() << " CREATED");
	} else {
		LOG4CXX_ERROR(logger, toString() << " CREATE FAILED");
	}

};

WaitObject::WaitObject(ACU_WAIT_OBJECT sdk_wo) :
		logger(Logger::getLogger( "main")),
		initStatus(true),
		signalled(ACU_OS_FALSE),
		handle_is_copy(ACU_OS_FALSE)  {
	// copy 3 elements: event_fds, event_pfd, handle_is_copy
	event_fds[PIPE_READ] = sdk_wo.event_fds[PIPE_READ];
	event_fds[PIPE_WRITE] = sdk_wo.event_fds[PIPE_WRITE];
	memcpy(&event_pfd, &sdk_wo.event_pfd, sizeof(pollfd));
	handle_is_copy = sdk_wo.active;

	LOG4CXX_TRACE(logger, "Converted ACU_WAIT_OBJECT: " << toString());
};

WaitObject::WaitObject(tSMEventId &prosody_event) :
		logger(Logger::getLogger( "main")),
		initStatus(true),
		signalled(ACU_OS_FALSE),
		handle_is_copy(ACU_OS_TRUE) { // This is a prosody event
	event_fds[PIPE_READ] = 0;
	event_fds[PIPE_WRITE] = 0;
	event_pfd.fd = prosody_event.fd;
	event_pfd.events = prosody_event.mode;
	event_pfd.revents = 0;
	handle_is_copy = 1; // This is a prosody event
};


WaitObject::~WaitObject() {
	// if WO is not a copy, close file descriptors
	if( !handle_is_copy ) {
		close(event_fds[PIPE_READ]);
		close(event_fds[PIPE_WRITE]);
	}
};

void WaitObject::clearSignalled() {
	boost::mutex::scoped_lock lock(mutexWO);
	signalled = ACU_OS_FALSE;
};

ACU_OS_ERR WaitObject::wait(int timeout, int &command) {

	int r = poll(&event_pfd, 1, timeout); // wait 1 file descriptor
	if (r == 0) {
		LOG4CXX_TRACE(logger, toString() << " POLL rc=" << r << " -> TIMEOUT");
	} else if (r < 0) {
		LOG4CXX_TRACE(logger, toString() << " POLL rc=" << r << " -> ERROR");
	} else { // (r > 0)
		LOG4CXX_TRACE(logger, toString() << " POLL rc=" << r << " -> DATA READY");
	}

	ACU_OS_ERR result = ERR_ACU_OS_NO_ERROR;
	signalled = ACU_OS_FALSE;	// clear signalled

	if (r > 0) { // during timeout interval we've received something
		if (event_pfd.revents) {// we have something new in revents. If revents==0 - timeout expired
			if( !handle_is_copy ) { // if handle wasn't cloned
				boost::mutex::scoped_lock lock(mutexWO);
				unsigned char tmp;	// for avoiding incorrect casting in read() (in case of int, possible negative)
				r = read(event_fds[PIPE_READ], &tmp, 1); // reading int from pipe to local int, blocked operation by mutex
				//command = buffer;	// cone stone of PIPE !! )))
				buffer = command = (int)tmp;
				LOG4CXX_TRACE(logger, toString() << " READ value=" << buffer << " rc=" << r);
			}

			signalled = ACU_OS_TRUE; // set signaled in case of new data written in pipe
		}
	} else { // if r is negative or 0 (it means TIMEOUT or ERROR), set result to corresponding ERR code
		if( r == -1 )
			r = errno;
		switch (r)
		{
		case 0:
			result = ERR_ACU_OS_TIMED_OUT;
			break;
		case EBADF: /* FALL THROUGH */
		case EFAULT:
			result = ERR_ACU_OS_PARAMETER;
			break;
		case ENOMEM:
			result = ERR_ACU_OS_NO_MEMORY;
			break;
		case EINTR:
			result = ERR_ACU_OS_SIGNAL;
			break;
		default:
			result = ERR_ACU_OS_ERROR;
		}
	} // END if-else

	return result; // если ошибок не было возвращаем ERR_ACU_OS_NO_ERROR
};

ACU_OS_ERR WaitObject::wait(int timeout) {
	int tmp;
	return wait(timeout, tmp);
};

ACU_OS_ERR WaitObject::signal(int signalBuf) {
	// lock
	//boost::mutex::scoped_lock lock(mutexWO);

	ACU_OS_ERR error = ERR_ACU_OS_NO_ERROR;
	int rc;
	//int test = FAXD_CMD_EXIT;

	/* ensure we're not trying to set a Prosody event */
	if (!handle_is_copy) {
		{
			boost::mutex::scoped_lock lock(mutexWO);
			//buffer = signalBuf;		// cone stone of PIPE !! )))
			unsigned char tmp = (unsigned char)signalBuf;
			rc = write(event_fds[PIPE_WRITE], &tmp, 1); // write signaBuf to pipe's write descriptor
			LOG4CXX_TRACE(logger, toString() << " WRITE signalBuf=" << signalBuf << " rc=" << rc);
		}

		/* if the write failed, work out a suitable error code */
		// if 1 -> all wrote, 0 -> nothing wrote, 1 -> error, error code in errno
		if (rc != 1) {
			switch (errno) {
			case EBADF:		/* FALL THROUGH */
			case EINVAL:	/* FALL THROUGH */
			case EFAULT:	/* FALL THROUGH */
			case EFBIG:		/* FALL THROUGH */
			case EPIPE:		/* FALL THROUGH */
				error = ERR_ACU_OS_PARAMETER;
				break;
			case EINTR:
				error = ERR_ACU_OS_SIGNAL;
				break;
			case EAGAIN:	/* FALL THROUGH */
			case ENOSPC:	/* FALL THROUGH */
			case EIO:		/* FALL THROUGH */
			default:
				error = ERR_ACU_OS_ERROR;
			}
		}
	} else {
		error = ERR_ACU_OS_PARAMETER;
	}

	return error;
};

ACU_OS_ERR WaitObject::unsignal() {
	ACU_OS_ERR result = ERR_ACU_OS_NO_ERROR;
	int rc;
	int buffer = 1;

	/* only reset wait objects that were created using this library */
	if (!handle_is_copy)
	{
		/* before we clear the wait object, we need to ensure that it's been signalled - check */
		rc = poll(&event_pfd, 1, 0); // timeot = 0, we not wait, just clear already present data

		if (rc > 0)	{
			if(event_pfd.revents) {
				rc = read(event_fds[PIPE_READ], &buffer, 1);
			}
		} else {
			if( rc == -1 ) {
				rc = errno;
			}
			switch (rc) {
			case 0:
				result = ERR_ACU_OS_TIMED_OUT;
				break;
			case EBADF:		/* FALL THROUGH */
			case EFAULT:
				result = ERR_ACU_OS_PARAMETER;
				break;
			case ENOMEM:
				result = ERR_ACU_OS_NO_MEMORY;
				break;
			case EINTR:
				result = ERR_ACU_OS_SIGNAL;
				break;
			default:
				result = ERR_ACU_OS_ERROR;
			}
		}
	} else {
		result = ERR_ACU_OS_PARAMETER;
	}

	return result;
};

std::string WaitObject::toString() {
	std::stringstream ss;
	ss << "WaitObject event_fds[R]:" << event_fds[PIPE_READ] <<
			" event_fds[W]:" << event_fds[PIPE_WRITE] <<
			" poll fd:" << event_pfd.fd << " buffer:" << buffer << " status:" << initStatus;
	return ss.str();
};

/*****************************************************************************\
 *
 *                       class MultipleWaitObject
 *
\*****************************************************************************/

void WaitMultipleObject::clearSignalled() {
	// clear "signalled" property on all WaitObjects in vector
	std::vector<WaitObject*>::iterator it;
	for(it=vpWO.begin(); it!=vpWO.end(); ++it) {
		(*it)->clearSignalled();
	}
};

void WaitMultipleObject::push_back(WaitObject* pwo) {
	vpWO.push_back(pwo);
};

ACU_OS_ERR WaitMultipleObject::waitForAny(int timeout) {
	// unsignall all objects
	clearSignalled();

	// create temporary array of poll structures corresponding to quantity of WO
	int object_count = vpWO.size();
	struct pollfd* fds = new pollfd[object_count];

	// fill pollfd array with the corresponding vpWO[]->event_pfd content
	std::vector<WaitObject*>::iterator it;
	int index;
	for(it=vpWO.begin(), index=0; it!=vpWO.end(); ++it, ++index) {
		fds[index].fd = (*it)->event_pfd.fd;
		fds[index].events = (*it)->event_pfd.events;
		fds[index].revents = (*it)->event_pfd.revents;
	}

	/*
	 * Waiting for any event on any file descriptors
	 */
	ACU_OS_ERR result = ERR_ACU_OS_NO_ERROR;
	int rc = 0;
	rc = poll(fds, object_count, timeout);

	/* if at least one object has been signalled, loop through the set and fill in the
		appropriate element in the signalled array */
	if (rc > 0) {
		for (index=0; index < object_count; index++) {
			if (fds[index].revents)
			{
				fds[index].revents = 0;

				/* if the wait object is cloned from another source, don't reset it */
				if(!vpWO[index]->handle_is_copy) {
					/* reset the wait object */
					/*
					 *TODO ATTENTION, PROBLEMS!!!
					 */
					rc = read((fds[index].fd),&vpWO[index]->buffer,1);
				}
				vpWO[index]->signalled = ACU_OS_TRUE;
			}
		}
	} else {
		if( rc == -1 )
			rc = errno;
		switch (rc) {
		case 0:
			result = ERR_ACU_OS_TIMED_OUT;
			break;
		case EBADF:		/* FALL THROUGH */
		case EFAULT:
			result = ERR_ACU_OS_PARAMETER;
			break;
		case ENOMEM:
			result = ERR_ACU_OS_NO_MEMORY;
			break;
		case EINTR:
			result = ERR_ACU_OS_SIGNAL;
			break;
		default:
			result = ERR_ACU_OS_ERROR;
		}
	} // END if-else

	// cleanup
	delete[] fds;

	return result;
};
