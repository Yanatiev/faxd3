/*
 * config.h
 *
 *  Created on: Mar 16, 2014
 *      Author: root
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#include <boost/regex.hpp>
#include <boost/property_tree/ptree.hpp>
#include <map>
#include <string>
#include <vector>
#include <exception>
#include "basicconfig.h"

#define FAXD_CFG_FILE "faxd.config"

#define CFG_SECTION_DELIVER "deliver"
#define CFG_SECTION_DELIVER_TYPE_GOE "goe"
#define CFG_SECTION_DELIVER_TYPE_VOXFAX "voxfax"

namespace faxd {

	enum modem { MODEM_V34=0, MODEM_V17=1, MODEM_V29=2, MODEM_V27=3 };
	enum DeliverType { DELIVER_TYPE_GOE=0, DELIVER_TYPE_VOXFAX=1 };

/**
 * @class ConfigException config.h
 * @brief Exception which throwed in proccess of config file parsing
 */
	class ConfigException : public std::exception {
	private:
		const std::string message_;
	public:
		ConfigException();
		ConfigException(const std::string& message);

		virtual const char* what() const throw() {
			return message_.c_str();
		}
		virtual ~ConfigException() throw();
	};

/**
 * @class CallProperty
 * @brief contains call configuration environment, depending on DNIS (Calling ID)
 */
	class CallProperty {
	public:
		CallProperty(std::string regexStr);
		/**
		 * @param dnis string representation of calling ID
		 * @return true, if matched, false - elsewhere
		 */
		bool isMatched(const std::string& dnis);
		/**
		 * @return enum ModemType value
		 */
		modem getModem() {return modem_;}
		void setModem(modem modem) {modem_ = modem;}

		int getEcm() {return ecm_;};
		void setEcm(int ecm) {ecm_ = ecm;};

		int getTrace() {return trace_;};
		void setTrace(int trace) {trace_ = trace;};

		int getKeepTmp() {return keepTmp_;};
		void setKeepTmp(int keepTmp) {keepTmp_ = keepTmp;};

		std::string getDeliverName() {return deliverName_;};
		void setDeliverName(std::string deliverName) {deliverName_ = deliverName;};

	private:
		boost::regex	dnisRegex_;	//!< pattern for dnis matching
		modem			modem_;		//!< modem type
		int				ecm_;		//!< error correction 0 - disable, 1 - enable
		bool			trace_;		//!< low level trace provided with driver. Stored in temporary directory
		bool			keepTmp_;	//!< true - don't delete fax and descriptor from tmp directory. Default false
		/**
		 * string deliver queue name. It can be specific for each dnis pattern. folder with this name is placed under in/ directory. Default name - empty string.
		 * In that case all files are placed directly in .../in folder
		 */
		std::string		deliverName_;
	};

/**
 * @class Config config.h
 * @brief Main container to store all program settings
 * @details All settings variables are static. They are initialized from JSON formatted file.
 * If file initialization failed, variables are set to their default values.
 *
 * Config file example:
 * ========================================================<br/>
 *{
 *	"logcfg":"Log4cxxConfig.xml",
 *	"spoolDir":"./",
 *	"TDMNumCallsPerPort":24,
 *	"streamsPerDSP":1,
 *	"CallDisconnectTime":600,
 *	"Companding":1,
 *	"FAXLogToFile":1,
 *	"outQueue":"filequeue-out",
 *	"cards" : [
 *		{
 *		"serial"	:224418,
 *		"active_ports"	: [2]
 *		},
 *		{
 *		"serial"	:224419,
 *		"active_ports"	: [2, 3, 11]
 *		},
 *	]
 *	"dnis" : [
 *		"*832" : { "modem" : "v34" },
 *		"*333" : { "modem" : "v17" }
 *	]
 *}
 * ========================================================<br/>
 *
 * Usage example:<br/>
 * ========================================================<br/>
 * #include "config.h"<br/>
 * int main() {<br/>
 * 		if( Config::configure("faxd.config") == false )<br/>
 * 			exit(-1);<br/>
 *
 * 		int x = cfg::sendersNum<br/>
 * }<br/>
 * ========================================================<br/>
 */
	class Config : public BasicConfig {
	public:
		static std::string logFile;			//!< logger configuration file
		static std::string serverId;		//!< 4-symbol server consequent number
		static std::map< std::string, std::vector<int> > cardsPorts;	//!< map with key - string serial number and vector of enable ports
		static std::map< std::string, std::string > cardsId;			//!< map with key - string serial number and other string - consequent id of this card on server (0, 1, 2, 3)

		static int TDMNumCallsPerPort;		//!< number of B channels: ISDN PRI T1 -> 23, E1 -> 30
		static int streamsPerDSP;			//!< number of streams per DSP ProsodyX 1U - 1, ProsodyX 1U HA - 2
		static int CallDisconnectTime;		//!< number of seconds to call disconnecting
		static int Companding;				//!< 0=Alaw, 1=Mulaw
		static int FAXLogToFile;			//!< tracing internal FAX processing to File (0-OFF, 1-ON)
		static std::string outQueue;		//!< name of file queue for outgouing FAXes
		static std::string spoolDir;		//!< in spool dir must be 3 directory (in, out, tmp)
		static std::string inDir;			//!< directory (under spoolDir) for inbound FAXes after reception
		static std::string badDir;			//!< directory for bad incoming FAXes
		static std::string outDir;			//!< directory (under spoolDir) for sending FAXes
		static std::string tmpDir;			//!< directory (under spoolDir) for temporary files (.tiff, .info) at reception time
		static std::string infoExt;			//!< info file extension (default ".info")
		static std::string faxExt;			//!< FAX file extension (default ".tiff")
		static DeliverType deliverType;		//!< Type of inbound file delivery. Placed in section "delivery", contains values: "goe" or "voxfax"
		static std::string deliverGoeDir;	//!< in case of "goe" type it contains remote ftp path for .goe files
		static std::string deliverTifDir;	//!< in case of "goe" type it contains remote ftp BASE path for .tif files


		// constants
		const static int TING_MAX_CHANNELS_PER_STREAM	= 128;
		const static int TDM_PORT_MAX_TIMESLOTS			= 32;
		const static int MAX_TDM_PORTS					= 16;
		const static int MAX_DSPS						= 8;
		//Config();
		/**
		 * @brief Reads config file
		 * @details Operations:
		 * 		- reads file if it present;
		 * 		- parses it with JSON parser;
		 * 		- extracts all global settings.
		 * @param configFile configuration file name (full name with path preferable).
		 * @return if file is found and successfy JSON parser: normally parsed, true value returns, elsewhere false.
		 * In case of false, content of configured variable may be unpredictable. So best behavior - exiting from program.
		 */
		static bool configure(std::string configFile);
		static modem getModemByDnis(std::string dnis);
		static CallProperty* getDnisSettings(std::string dnis);
		static std::vector < CallProperty > * getDnisProps() {return &dnisProps;};
		static std::string toString();

		int getInt(std::string key, int defaultValue);
		std::string getStr(std::string key, std::string defaultValue);

		Config(std::string configFile) throw (ConfigException);
	private:
		static std::vector < CallProperty >	dnisProps;	//!< properties of each DNIS pattern
		static boost::property_tree::ptree pt;	//!< propety tree for storing options from config file
	};

	typedef Config cfg;

}


#endif /* CONFIG_H_ */
