#ifndef FAX_H_
#define FAX_H_

#include <log4cxx/logger.h> // log4cxx header
#include <boost/date_time/posix_time/posix_time.hpp>
#include "waitobject.h"
#include "dd.h"
//#include "channel.h"
//Aculab
#include <bfile.h>
#include <bfopen.h>
#include <smfaxapi.h>
#include <smdrvr.h>	//TiNG
#include <actiff.h>

namespace faxd {
	class Channel;

	class Fax{
		log4cxx::LoggerPtr logger;	// reference to log initialized based on cfg data
		faxd::Channel *pChannel;	// pointer to parent channel

		boost::posix_time::ptime startTime;		//!< start time of FAX receipt
		boost::posix_time::ptime endTime;		//!< end time of FAX receipt
		std::string	tiff_file;					//!< tiff file name in format: <cardSerial>-<port idx>-<ts>-HH:MM:SS.tiff
		std::string	info_file;					//!< info file name in format: <cardSerial>-<port idx>-<ts>-HH:MM:SS.info
		std::string	trace_file;					//!< info file name in format: <cardSerial>-<port idx>-<ts>-HH:MM:SS.trace
		//int		return_code;					// set in Fax::receiveFax()
		int v17;
		int v29;
		int v27ter;
		int ECM;
		int v34;
		int v8;
		int max_data_rate;
		bool trace;
		int fax_id;

		volatile int	fsmRunning_;			//!< if FSM is running, contains 1
		char subscriber[32];
		// int stream;
		int ts;
		int	faxpage;
		int page_count;
		tSMFMPrxId	rx_id;
		tSMFMPtxId	tx_id;
		faxd::WaitObject*			IdleEventHandle;
		tSMChannelId				TiNG_ChannelId;
		tSMModuleId					module_id;
		//SMFAX_T38_SDP_OPTIONS*				t38_media;
		SMFAX_SESSION 				fax_session;	//!< for smfax_create_session()


		int get_fax_data_rate_from_const(int data_rate_const, int fax_modem);
		int get_num_pages(ACTIFF_FILE *actiff_);
		std::string faxCapsToString(SMFAX_CAPS *faxCaps);
	public:
		Fax(faxd::Channel *p_channel);
		void receiveFAXCallFlow();
		//tSM_INT receiveFAX();
		faxd::FaxReceiveStatus receiveFAX();
		void sendFAXCallFlow();
		void sendFAX();
		/**
		 * @brief if Fax State Machine in running loop, it breaks FAX reception
		 * @return smfax_rude_interrupt() error code
		 */
		int interrupt();
	};
} //END namespace faxd
#endif /* FAX_H_ */
