/*
 * card.h
 *
 *  Created on: Mar 13, 2014
 *      Author: root
 */

#ifndef CARD_H_
#define CARD_H_

#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <log4cxx/logger.h> // log4cxx header
#include <log4cxx/xml/domconfigurator.h>
#include "dsp-mgmt.h"
#include "port.h"
#include "config.h"
#include "filequeue.h"
#include "fsm.h"
// C includes (Aculab headers)
#include <res_lib.h>
#include <acu_type.h>
#include <sw_lib.h>
#include <cl_lib.h>
#include <smdrvr.h>	//TiNG
#include <smrtp.h>	//TiNG

enum CardStates {
	STATE_CARD_START,
	STATE_CARD_INSERVICE,
	STATE_CARD_REMOVED,
	STATE_CARD_EXIT
};

enum CardEvents {
	EVENT_CARD_INIT,
	EVENT_CARD_REMOVE,
	EVENT_CARD_ADD,
	EVENT_CARD_EXIT
};

namespace faxd {

/**
 * @class Card card.h
 * @brief Holder or Ports and DSP resources
 * @details
 */
	class Card : public FSM<CardStates, CardEvents, Card> {
		log4cxx::LoggerPtr	logger;		// log initialized based on cfg data
		std::string			logHeader_;	// suffix for logging messages
		FileQueue 			&outQueue_;	// reference to file queue for outbound faxes
		boost::shared_ptr<DSPModuleHolder> pModules;		//!< TiNG modules dispatcher, coming before ports
															//!< for correct destruction, because port uses DSPs, not a vice versa
		std::vector< boost::shared_ptr<Port> > ports;		//!< E1/T1 ports 1..*


		bool initStatus;				//!< card status (false if card not present, initialized or not support FAX features)
		std::string 	serialNo;		//!< card's serial number
		ACU_UINT		unCardId;		//!< internal RESOURCE ID of card

		// Aculab data structures, belonging to card
		ACU_EVENT_QUEUE				queueId_;		//!< card event queue resource id
		ACU_OPEN_CARD_PARMS			rOpenParms;		// card parameters. Get them from drivers
		ACU_CARD_INFO_PARMS			rCardInfo;		//!< descriptive card information
		ACU_OPEN_SWITCH_PARMS		rOpenSwitch;	//!< data structure for switch functionality initialization
		ACU_OPEN_CALL_PARMS			rOpenCall;		//!< data structure for call functionality initialization
		ACU_OPEN_PROSODY_PARMS		rOpenSpeech;	//!< data structure for speech (fax) functionality initialization
		CARD_INFO_PARMS				cCardInfo;		//!< Info about card (ports, irq, etc...) Call Control API

		boost::scoped_ptr<boost::thread>	spEventManagerThread;	//!< thread object for running channelHandler
		volatile bool						eventManagerExit_;		//!< signal to event manager thread to exit

/**
 * @brief start TDM infrastructure
 * @details entrance point to TDM (T1/E1) standard telephony. It responsible for:
 * 		- allocating DSP resources for TDM channels. It's very tangled procedure
 * 		- Add valid and initialized port to shared_ptr<Port> vector
 * 		- create CallData structures (complete data about all call events on each timeslot) and assign
 * 		it to all channels(timeslot) on each port
 * 		- for each port creates Event Management Thread (Call control processor)
 * @param nothing
 * @return success or failure
 */
	public:
		/**
		 *
		 * @param serial - string representation of card serial number
		 */
		Card(std::string serial, FileQueue &outQueue);
		~Card();
		/**
		 * @brief get initialization status
		 * @return if card initialization success - returns true, else returns false
		 */
		bool getStatus() { return initStatus; };
		std::string getInfo();

		/**
		 * @brief full card initialize - acu_open_card(), acu_open_switch(), acu_open_call(), acu_openprosody(), call_open_port()
		 */
		void actionInit();
		/**
		 * @brief start worker thread
		 */
		void actionRunWorker();
		/**
		 * @brief deinitialize card - acu_close_port(), acu_close_prosody(), acu_close_call(), acu_close_switch(), acu_close_card()
		 */
		void actionCleanup();
		/**
		 * @brief set exit_ flag to true - signal to worker thread to exit
		 */
		void actionSetExit() {eventManagerExit_ = true;};
		/**
		 * join() worker thread, after actionSetExit()
		 */
		void actionWaitWorkerExit();

		/**
		 * @brief no operations
		 */
		void actionNoop() {};

		/**
		 * @brief worker thread on Card. Main target - capture card events
		 */
		void eventManager();
	};

} // END namespace faxd



#endif /* CARD_H_ */
