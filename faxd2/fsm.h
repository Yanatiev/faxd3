/*
Usage example:
==============================================================================================
#include <iostream>
#include "fsm.h"

enum States {state1, state2};
enum Events {event1, event2};

class A : public FSM<States, Events, A> {
public:

	// set initial state and fill transition table
	A() : FSM<States, Events, A>(state1) {
		addTransition(state1, event1, state2, &A::action1 );
		addTransition(state2, event2, state1, &A::action2 );
	};

	// define actions, they are must correspond to actions in transition table
	void action1() { std::cout << "action1() state:" << state_ << std::endl; }
	void action2() { std::cout << "action2() state:" << state_ << std::endl; }
};

int main() {
	A a;
	a.proccessEvent(event1);
	a.proccessEvent(event2);
	a.proccessEvent(event1);
}
==============================================================================================
*/

#ifndef FSM_H_
#define FSM_H_

#include <vector>
#include <boost/thread/recursive_mutex.hpp>

/**
 * @class FSM - template for inheritance. Implements Finite State Machine
 * Ts - state (enum or class must implement "==" or "=")
 * Te - event (enum or class must implement "==" or "=")
 * Ta - class for actions - method void (Ta::*ptr)()
 */
template <typename Ts, typename Te, typename Ta>
class FSM {
	// typedefs
	typedef void (Ta::*Action)();						// action method pointer type
	typedef void (FSM<Ts, Te, Ta>::*CastActionType)();	// cast type to ability compile

	struct Transition {
		Ts currentState_;
		Te event_;
		Ts nextState_;
		Action action_;
		Transition (Ts currentState, Te event, Ts nextState, Action action );
	};

public:
	/**
	 * @brief creates FSM and put it to initial state
	 */
	FSM(Ts state);

	/**
	 * @brief add transition entry to FSM
	 * @details each transition entry checked consequently with proccessEvent() method. There is ability to run few action per one transition with adding few transition rules
	 * @param currentState - state before event (enum or class must implement "==" or "=")
	 * @param event - event (enum or class must implement "==")
	 * @param nextState - state after event (enum or class must implement "==" or "=")
	 * @param action - pointer to class method void (Ta::*ptr)()
	 */
	void addTransition(Ts currentState, Te event, Ts nextState, Action action);

	/**
	 * @brief push FSM transitions and produces actions
	 * @param e - event (enum or class must implement "==")
	 */
	void proccessEvent(Te e);

	/**
	 * @return current state
	 */
	Ts getState();

private:
	std::vector<Transition> transitionTable_;
	boost::recursive_mutex	transitionMutex_;				//!< for operation in multithread applications
	Ts state_;
};

/*
 * Methods definitions
 */
template <typename Ts, typename Te, typename Ta>
FSM< Ts,  Te,  Ta>::Transition::Transition (Ts currentState, Te event, Ts nextState, Action action ) :
	currentState_(currentState),
	event_(event),
	nextState_(nextState),
	action_(action)
{};

template <typename Ts, typename Te, typename Ta>
FSM< Ts,  Te,  Ta>::FSM(Ts state) :
		state_(state)
{};

template <typename Ts, typename Te, typename Ta>
void FSM< Ts,  Te,  Ta>::addTransition(Ts currentState, Te event, Ts nextState, Action action) {
		transitionTable_.push_back( Transition(currentState, event, nextState, action) );
};

template <typename Ts, typename Te, typename Ta>
void FSM< Ts,  Te,  Ta>::proccessEvent(Te e) {
	// lock till the end of state change (with the end of state change and actions)
	boost::unique_lock<boost::recursive_mutex> scoped_lock(transitionMutex_);

	Ts stateBuf = state_;			// backup internal state

	typename std::vector<Transition>::iterator it;
	for(it=transitionTable_.begin(); it<transitionTable_.end(); ++it) {
		if ( it->currentState_ == state_ && it->event_ == e) {

			( this->*(CastActionType)it->action_)();
			//TODO process exceptions from action
			stateBuf = it->nextState_; // for ability to do few actions
		}
	}

	state_ = stateBuf;	//set new state after action complete
};

template<typename Ts, typename Te, typename Ta>
Ts FSM< Ts, Te, Ta >::getState() {
	// lock for read. If change state action is not completed (it means that state is currently changing), this method is waiting
	boost::unique_lock<boost::recursive_mutex> scoped_lock(transitionMutex_);
	return state_;
};

#endif /* FSM_H_ */
