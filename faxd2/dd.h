
#ifndef DD_H_
#define DD_H_

#include <string>
#include <boost/date_time/posix_time/posix_time.hpp>

namespace faxd {

	enum FaxReceiveStatus {
		FAX_RECEIVE_OK,
		FAX_RECEIVE_PARTIAL,
		FAX_RECEIVE_ERROR,
		FAX_FILE_CREATE_ERROR
	};

	class DeliverDescriptor {
	public:
		DeliverDescriptor(std::string path, std::string ext);
		virtual ~DeliverDescriptor() {};

		virtual bool create() = 0;	//!< must be invoked after fields completion
		std::string getName() {return fileName_;};
		void setServerId(const std::string& serverId);
		void setAni(const std::string& ani) ;
		void setCardId(int cardId);
		void setDnis(const std::string& dnis);
		void setEcm(int ecm);
		void setEnd(const boost::posix_time::ptime& end);
		//void setErrorCode(int errorCode);
		void setErrorCode(FaxReceiveStatus errorCode);
		//void setErrorDescr(const std::string& errorDescr);
		void setPages(int pages);
		void setPortId(int portId);
		void setSpeed(int speed);
		void setTiffSize(int tiffSize);
		void setStart(const boost::posix_time::ptime& start);
		void setTs(int ts);
		void setCompression(int compression);


	protected:
		std::string fileName_;		//!< descriptor file name
		std::string path_;
		std::string ext_;
		std::string serverId_;		//!< string representation of server id
		int 		cardId_;
		int 		portId_;
		int 		ts_;
		std::string ani_;
		std::string dnis_;
		boost::posix_time::ptime start_;
		boost::posix_time::ptime end_;
		int 		speed_;
		int			tiffSize_;
		int			compression_;
		int 		ecm_;
		int 		pages_;
		FaxReceiveStatus 		errorCode_;
		std::string errorDescr_;
	};

}

#endif /* DD_H_ */
