/*
 * daemon.h
 *
 *  Created on: Mar 2, 2015
 *      Author: root
 */

#ifndef DAEMON_H_
#define DAEMON_H_

#include <string>
#include <exception>
//#include <boost/filesystem.hpp>
#include "config.h"

namespace faxd {
/**
 * @class DaemonException config.h
 * @brief Exception which throwed in proccess of config file parsing
 */
	class DaemonException : public std::exception {
	public:
		virtual const char* what() const throw() {
			return "Config file parse exception";
		}

		DaemonException(std::string str) : msg_(str) {}
		~DaemonException() throw() {}
	private:
		std::string msg_;
	};

	class Daemon {
	public:
		/**
		 * @brief try to start daemon.
		 * @param configfile full path configuration file name
		 * @param pidFile full path pid file name
		 */
		Daemon(std::string& configFile, std::string& pidFile);

	private:
		Config cfg_;
		int pid_;					//!< daemon process ID
		std::string configFile_;	//!< configuration file in JSON format with full path
		std::string pidFile_;		//!< pid file with a full path (usually /var/run/faxd2.pid)

		/**
		 * @brief start daemon process. Initializes resources. Contain main loop
		 * @throws In case of any problem (can't start, configuration file not accessible) throws exception
		 */
		void worker();
	};

}

#endif /* DAEMON_H_ */
