# Don't try fancy stuff like debuginfo, which is useless on binary-only
# packages. Don't strip binary too
# Be sure buildpolicy set to do nothing
%define        __spec_install_post %{nil}
%define          debug_package %{nil}
%define        __os_install_post %{_dbpath}/brp-compress

Summary: FAX daemon for Aculab prosody device
Name: faxd2
Version: 2.12
Release: 1%{?dist}
License: GPL+
Group: Development/Tools
SOURCE0 : %{name}-%{version}.tar.gz
URL: http://unknown.url/

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

%description
FAXD - service for receiving faxes using Aculab Prosod X/Prosody X1U devices and transfering
incoming faxes to any HTTP destinations using VoxFax protocol

%{summary}

%prep
%setup -q

%build
# Empty section.

%install
rm -rf %{buildroot}
mkdir -p  %{buildroot}
# in builddir
cp -a * %{buildroot}
#ln -sf /etc/rc.d/init.d/faxd %{buildroot}/usr/local/%{name}/faxd

%post
if [ -x /sbin/chkconfig ]; then
 /sbin/chkconfig --add faxd
# for i in 3 5; do
#  ln -sf /etc/init.d/faxd /etc/rc.d/rc${i}.d/S95faxd
#  ln -sf /etc/init.d/faxd /etc/rc.d/rc${i}.d/K15faxd
# done
fi

%preun
/sbin/service faxd stop
/sbin/chkconfig --del faxd
#for i in 3 5; do
# if [ -e /etc/rc.d/rc${i}.d/S95faxd ]; then unlink /etc/rc.d/rc${i}.d/S95faxd; fi
# if [ -e /etc/rc.d/rc${i}.d/K15faxd ]; then unlink /etc/rc.d/rc${i}.d/K15faxd; fi
#done


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%config(noreplace) %{_sysconfdir}/%{name}/*
/usr/local/%{name}/*
/etc/rc.d/init.d/*

%changelog
* Tue Jun 30 2015  yanatdv@gmail.com 1.0-1
- Second Build

