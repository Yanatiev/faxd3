/*
 * waitobject.h
 *
 *  Created on: Feb 10, 2014
 *      Author: root
 */

#ifndef WAITOBJECT_H_
#define WAITOBJECT_H_

#include <vector>
#include <string>
#include <log4cxx/logger.h>
#include <boost/thread/mutex.hpp>
// C includes
#include <sys/poll.h>
#include <unistd.h>		// pipe
//Aculab includes
#include <acu_type.h>
#include <smdrvr.h>	//TiNG
#include <smrtp.h>	//TiNG


typedef int ACU_OS_BOOL;
typedef int ACU_OS_ERR;

/* boolean types */
#define ACU_OS_TRUE		1
#define ACU_OS_FALSE	0

#define PIPE_READ 0
#define PIPE_WRITE 1

/* errors */
#define ERR_ACU_OS_NO_ERROR		0
#define ERR_ACU_OS_NO_MEMORY	1 /* out of memory */
#define ERR_ACU_OS_SIGNAL		2 /* OS interrupted a blocking operation (e.g. because of a signal) */
#define ERR_ACU_OS_TIMED_OUT	3 /* wait finished without anything happening */
#define ERR_ACU_OS_PARAMETER	4 /* function given invalid parameter */
#define ERR_ACU_OS_ERROR		5 /* unexpected OS error occurred */
#define ERR_ACU_OS_WRONG_STATE	6 /* thread is in the wrong state for request */

/* commands */
#define FAXD_CMD_CFGRELOAD			3
#define FAXD_EVENT_CALL_CONNECTED	100
#define FAXD_EVENT_IDLE				101
//#define FAXD_EVENT_CALL_OUT			102
#define FAXD_CMD_NOOP				254
#define FAXD_CMD_EXIT				255
namespace faxd {
/**
 * @class WaitObject
 * @brief representation of ACU_SDK_WAIT_OBJECT with all it manipulation methods
 */
	class WaitObject {
		friend class WaitMultipleObject;

		log4cxx::LoggerPtr logger;	// reference to log initialized based on cfg data
		bool initStatus;		//!< true - initialization OK, false - ERROR
		ACU_OS_BOOL signalled;	//!< set to ACU_OS_TRUE if some event occured, else ACU_OS_FALSE

		int				event_fds[2];	//!< file descriptor for pipe
		struct pollfd	event_pfd;		//!< data structure for checking if file descriptor is ready (/usr/include/sys/poll.h)
		ACU_OS_BOOL		handle_is_copy;	//!< has handle been cloned from another source. (Prosody event. We can poll event_pfd
										//!< but can't read from event_fds. Look: WaitObject(tSMEventId prosody_event); )
		int				buffer;			//!< some value, written to pipe, can be a command

		boost::mutex mutexWO;			//!< for concurrent access to fields

		WaitObject(WaitObject const& rhs);             // prevent copying
		WaitObject& operator=(WaitObject const& rhs);  // prevent assignment
	public:
		WaitObject();							//!< new pipe initialization
		WaitObject(ACU_WAIT_OBJECT sdk_wo);		//!< convert Aculab wait object to application
		WaitObject(tSMEventId &prosody_event);
		~WaitObject();							//!< close pipe FDs, if object wasn't a copy!
		bool getStatus() {return initStatus;};	//!< return true, if initialized correctly
		void clearSignalled();					//!< clear signalled flag
		std::string toString();

		/**
		 * @brief waiting for a POLLIN event during timeout in milliseconds
		 * exit on timeout state is an error. If we read something from pipe to buffer -> signalled==ACU_OS_TRUE
		 * @param	[in]	timeout in milliseconds
		 * @return	ERR_ACU_OS_NO_ERROR (0) - if success, or other positive ERR code, look waitobject.h.
		 * 	we have something red from pipe.
		 */
		ACU_OS_ERR wait(int timeout);
		/**
		 * @brief same as the ACU_OS_ERR wait(int timeout), but in command returns read int from pipe,
		 * if ERR_ACU_OS_NO_ERROR returns, in other case
		 */
		ACU_OS_ERR wait(int timeout, int &command);
		/**
		 * @brief send signal (write to pipe something)
		 * @details we can write to pipe everything we want. In Aculab example 1 has been written
		 * 		but we can write some int command. Look to FAXD_SIGNAL_...
		 */
		ACU_OS_ERR signal(int signalBuf);
		/**
		 * @brief clear existing signal, if it present
		 */
		ACU_OS_ERR unsignal();
	};
/**
 * @class WaitObject
 * @brief representation of ACU_SDK_WAIT_OBJECT with all it manipulation methods
 */
	class WaitMultipleObject {

		std::vector<WaitObject*> vpWO;

	public:
		void clearSignalled();
		void push_back(WaitObject* pwo);			//!< add one more WaitObject. When WO pushed< it's signalled status is cleared
		ACU_OS_ERR waitForAny(int timeout);
	};

} // END namespace faxd
#endif /* WAITOBJECT_H_ */
