/**
	@file port.cpp
    Purpose: all operation with Aculab Prosody TDM port
  	  - initialize
  	  - run worker thread
  	  - initialize ports (ports objects)

	@author: yanat
	@version: 1.01
	@date 20/02/2014
 */
#include <string>
#include <bitset>
#include "port.h"
#include "waitobject.h"
// Boost
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

//#include <stdio.h>	// remove in daemon
#include <iostream>
// Acculab
#include <acu_type.h>
#include <cl_lib.h>
// C
#include <time.h>

using namespace log4cxx;
using namespace std;
using namespace faxd;

Port::Port(boost::shared_ptr<DSPModuleHolder> sp_dspholder,
			std::string card_sn, ACU_CARD_ID card_id, ACU_INT port_ix, FileQueue &outQ) :
				logger(Logger::getLogger( "main")),
				outQueue_(outQ),
				initStatus(false),
				spDSPHolder(sp_dspholder),
				//command(),
				eventManagerExit_(false),
				cardSerialNo(card_sn),
				port(port_ix),
				numcallsin(cfg::TDMNumCallsPerPort),
				numcallsout(cfg::TDMNumCallsPerPort)
{
	/*
	 * logger prefix
	 */
	std::stringstream ssHeader;
	ssHeader << "card s/n:" << cardSerialNo << " port:" << port << " ";
	logHeader_ = ssHeader.str();

	INIT_ACU_CL_STRUCT(&cOpenPort);
	cOpenPort.card_id = card_id;		// current card (internal id)
	cOpenPort.port_ix = port_ix;	// port index
	ACU_ERR rc = call_open_port(&cOpenPort); // open port for using. Look Call Control API Guide
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "INIT call_open_port() " << acu_error_desc(rc));
		return; // initStatus will be false
	} else {
		port_id = cOpenPort.port_id;
		LOG4CXX_DEBUG (logger, logHeader_ << "INIT call_open_port() OK. Resource ID:" << port_id);
	}

	/*
	 * get line type. Returns:
	 * 	- positive - type (E1, T1(CAS,ISDN), PSN - H.323/SIP)
	 * 	- 0 - if line unknown
	 * 	- negative - in case of ERROR
	 */
	rc = call_line(cOpenPort.port_id);
	if(rc < 0) { // <0 - error
		LOG4CXX_ERROR (logger, logHeader_ << "INIT UNKNOWING PORT TYPE. " << acu_error_desc(rc));
	} else {
		if(rc != L_PSN) {
			initStatus = true; // i.e port initialized and has TDM type !!!
			std::string s;
			switch (rc) {
				case L_E1		: s.assign("E1");		break;
				case L_T1_CAS	: s.assign("T1 CAS");	break;
				case L_T1_ISDN	: s.assign("T1 ISDN");	break;
				case L_BASIC_RATE: s.assign("BRI");		break;
				default:	s.assign("UNKNOWN"); // positive not in [1..4] and 0
			}
			LOG4CXX_INFO (logger, logHeader_ << "INIT Port Type:" << s);
		} else {
			LOG4CXX_INFO (logger, logHeader_ << "INIT Port Type:PSN (Packet Switched Network: IP - H323 or SIP)");
			initStatus = false;
			return;
		}
	}

	/*
	 * try to initialize a port for the calls
	 */
	rc = port_init(cOpenPort.port_id);
	if(rc) { // some errors
		initStatus = false;
		LOG4CXX_ERROR (logger, logHeader_ << "INIT port_init() ERROR:" << rc);
	} else {
		initStatus = true;
		LOG4CXX_INFO (logger, logHeader_ << "INIT port_init() OK");
	}

	/*
	 * Start eventManager in separate thread
	 */
	pEventManagerThread.reset(new boost::thread(boost::bind(&Port::eventManager, this)));
}; // END Port::Port(faxd::Config& config, log4cxx::LoggerPtr &logger, ACU_CARD_ID card_id, ACU_INT port_ix) :


Port::~Port() {

	LOG4CXX_DEBUG (logger, logHeader_ << "~Port() send FAXD_CMD_EXIT to portEventManger() thread");
	eventManagerExit_	= true;

	/*
	 * waiting for a eventManager termination (closing all channel threads, queues, ...)
	 */
	pEventManagerThread->join();

	/*
	 * Closing port in API
	 */
	INIT_ACU_CL_STRUCT(&cClosePort);
	cClosePort.port_id = cOpenPort.port_id;
	ACU_ERR rc = call_close_port(&cClosePort); // закрываем порт
	if(rc)  {
		LOG4CXX_ERROR (logger, logHeader_ << "call_close_port() " << acu_error_desc(rc));
	} else {
		LOG4CXX_DEBUG (logger, logHeader_ << "call_close_port() OK");
	}
};

void Port::eventManager() {
	ACU_ERR							rc;


	/*
	 * Create Event Queue
	 */
	ACU_ALLOC_EVENT_QUEUE_PARMS		rAllocEventQueue;
	INIT_ACU_STRUCT(&rAllocEventQueue);
	rc = acu_allocate_event_queue(&rAllocEventQueue);	// creates event queue
	queue_id = rAllocEventQueue.queue_id;				// store queue ID
	if(rc) {	LOG4CXX_ERROR (logger, logHeader_ << "PEM acu_allocate_event_queue() " << acu_error_desc(rc));
	} else {	LOG4CXX_INFO (logger, logHeader_ << "PEM acu_allocate_event_queue() OK. Resource ID:" << queue_id);
	}

	/*
	 * Attach Queue to Port for CALL EVENTS
	 */
	ACU_QUEUE_PARMS					cSetPortQueue;
	INIT_ACU_CL_STRUCT(&cSetPortQueue);
	cSetPortQueue.queue_id		= queue_id;
	cSetPortQueue.resource_id	= port_id;
	rc = call_set_port_default_handle_event_queue(&cSetPortQueue);
	if(rc) {	LOG4CXX_ERROR (logger, logHeader_ << "PEM call_set_port_default_handle_event_queue() " << acu_error_desc(rc));
	} else {	LOG4CXX_INFO (logger, logHeader_ << "PEM call_set_port_default_handle_event_queue() OK");
	}

	/*
	 *  Get a Queue Wait Object
	 */
	ACU_QUEUE_WAIT_OBJECT_PARMS		rWaitObject;
	INIT_ACU_STRUCT(&rWaitObject);
	rWaitObject.queue_id = queue_id;
	// get WAIT object for port queue using API Aculab
	rc = acu_get_event_queue_wait_object(&rWaitObject); // create composit obj (queue_id and wait object)
	if(rc) {	LOG4CXX_ERROR (logger, logHeader_ << "PEM acu_get_event_queue_wait_object() " << acu_error_desc(rc));
	} else {	LOG4CXX_INFO (logger, logHeader_ << "PEM acu_get_event_queue_wait_object() OK");
	}
	// convert wait object. This wait object signals about some event from Aculab to Application
	WaitObject QueueWaitObject(rWaitObject.wait_object);

	/*
	 * - create channels (channel threads, etc...)
	 * - mutual assign handlers between Channels and Driver
	 * - stuffing vector of channels
	 */
	initChannels();

	/*
	 * Main loop (processing all CALL Logic)
	 */
	int cmd;

	LOG4CXX_DEBUG (logger, logHeader_ << "PEM STARTED");
	ACU_EVENT_QUEUE_PARMS			rEventFromQueue;
	Channel							*CallData;			// handler from driver
	STATE_XPARMS					cCallEvent;

	while (!eventManagerExit_) {

		/*
		 * Process wait for a events from Prosody
		 */
		rc = QueueWaitObject.wait(200, cmd);
		if ( ERR_ACU_OS_NO_ERROR == rc ) {	// read some event,
			LOG4CXX_TRACE (logger, logHeader_ << "PEM - SOME EVENT RECEIVED");
			{ // lock scope (port)
				boost::unique_lock<boost::recursive_mutex> scoped_lock(CS);
				INIT_ACU_STRUCT(&rEventFromQueue);
				rEventFromQueue.queue_id = queue_id;
				// get event descriptor from queue
				rc = acu_get_event_from_queue(&rEventFromQueue);
				if(rc) {
					LOG4CXX_ERROR (logger, logHeader_ << "PEM - acu_get_event_from_queue() " << acu_error_desc(rc));
				}

				// retrieve Channel pointer from event handler
				CallData = (Channel*) rEventFromQueue.context;
				if (CallData == NULL) {
					LOG4CXX_FATAL (logger, logHeader_ << "PEM - acu_get_event_from_queue() NEW EVENT with NULL handler. EXITING");
					//exit=true;
				}

				// Get the Event using call to driver with a previously allocated handler (in initChannels), stored in Channel
				INIT_ACU_CL_STRUCT(&cCallEvent);
				cCallEvent.handle = CallData->handle;
				rc = call_event(&cCallEvent); // вытаскиваем детальное описание события (не звонка)
				if(rc) {
					LOG4CXX_ERROR (logger, logHeader_ << "PEM - call_event() " << acu_error_desc(rc));
				}
			}//END lock scope


			/*
			 * MAIN DECISION MAKING PROCCESS CONTROLLED WITH CHANNEL Finite State Machine
			 */
			switch(cCallEvent.state) {
			/*
			 * Waiting for incoming call. The driver is waiting for an incoming call on this handle.
			 */
			case EV_WAIT_FOR_INCOMING:
				LOG4CXX_INFO (logger, "card s/n:" << cardSerialNo << " port:" << port << ":" << CallData->ts << " PEM - RX <- EV_WAIT_FOR_INCOMING");
				CallData->proccessEvent(EVENT_WAIT_FOR_INCOMING);
				break;
			/*
			 * An incoming call has arrived. An incoming call has been detected on this call handle.
			 */
			case EV_INCOMING_CALL_DET:
				LOG4CXX_INFO (logger, "card s/n:" << cardSerialNo << " port:" << port << ":" << CallData->ts << " PEM - RX <- EV_INCOMING_CALL_DET");
				CallData->proccessEvent(EVENT_INCOMING_CALL_DET);
				break;

			/*
			 * The EV_WAIT_FOR_ACCEPT event will be returned in response to the call_incoming_ringing() function
			 * to indicate that ringing is in progress. The time between function and event is undefined as some
			 * tone based signaling systems may take a few seconds to send the ring tone. The application should
			 * respond with the call_accept() function after receiving this event
			 */
			case EV_WAIT_FOR_ACCEPT:
				LOG4CXX_INFO (logger, "card s/n:" << cardSerialNo << " port:" << port << ":" << CallData->ts << " PEM - RX <- EV_WAIT_FOR_ACCEPT");
				CallData->proccessEvent(EVENT_WAIT_FOR_ACCEPT);
				break;

			// unknown state
			case EV_MEDIA:
				LOG4CXX_INFO (logger, "card s/n:" << cardSerialNo << " port:" << port << ":" << CallData->ts << " PEM - RX <- EV_MEDIA");
				break;

			/*
			 * A call is connected. The driver has connected a call through to the application.
			 */
			case EV_CALL_CONNECTED:
				LOG4CXX_INFO (logger, "card s/n:" << cardSerialNo << " port:" << port << ":" << CallData->ts << " PEM - RX <- EV_CALL_CONNECTED");
				CallData->proccessEvent(EVENT_CONNECTED);
				break;

			/*
			 * The EV_REMOTE_DISCONNECT event will be returned, (if the CNF_REM_DISC option was set), whenever the
			 * far end disconnects the call. This event may occur at any time during a call. The normal response
			 * to this event would be to disconnect the call using the call_disconnect()function.
			 */
			case EV_REMOTE_DISCONNECT: // та сторона отвалилась, нужно запустить call_disconnect()
				LOG4CXX_INFO (logger, "card s/n:" << cardSerialNo << " port:" << port << ":" << CallData->ts << " PEM - RX <- EV_REMOTE_DISCONNECT");
				CallData->proccessEvent(EVENT_REMOTE_DISCONNECT);
				break;

			/*
			 * The channel is idle, The call has been terminated. The application must return
			 * the handle to the driver using the call_release() function.
			 */
			case EV_IDLE:
				LOG4CXX_INFO (logger, "card s/n:" << cardSerialNo << " port:" << port << ":" << CallData->ts << " PEM - RX <- EV_IDLE");
				CallData->proccessEvent(EVENT_IDLE);
				break;

			default:
				LOG4CXX_INFO (logger, "card s/n:" << cardSerialNo << " port:" << port << ":" << CallData->ts << " OTHER EVENT:" << cCallEvent.state);
			}//END switch
		}//END if() EXIST EVENT FROM PROSODY


//		/*
//		 * FAX TRANSMIT SECTION
//		 * get new file from outbound queue
//		 */
//		vector< boost::shared_ptr<Channel> >::iterator itChannel;
//		boost::shared_ptr<FileDescriptor> spfd;	// FOR FILEQUEUE
//		string fullNameWoExt;
//		fullNameWoExt = outQueue_.getFile(spfd);
//		if( !fullNameWoExt.empty() ) { // if there is something in queue
//
//			for(itChannel=vspChannels.begin(); itChannel!=vspChannels.end(); ++itChannel) {
//				if( (*itChannel)->getStatus() == CHANNELSTATUS_IDLE ) { // select first idle channel
//					/*
//					 * read property file
//					 */
//					std::stringstream ss;
//					std::ifstream file( (fullNameWoExt + cfg::infoExt).c_str() );
//					if ( file ) {
//						ss << file.rdbuf();
//						file.close();
//					}
//					boost::property_tree::ptree pt;
//					try { // parse property file
//						read_ini(ss, pt);	// parsing string representation of .info file to boost property tree
//					} catch (std::exception const& e) {
//						LOG4CXX_ERROR(logger, "Port::eventManagerThread() .info file: " << fullNameWoExt + cfg::infoExt << " ERROR: " << e.what());
//						// move file to bad
//						outQueue_.processBadFile(fullNameWoExt);
//						continue;
//					}
//					/*
//					 * get ani, dnis and file name and fill corresponding field in channel data
//					 */
//					string anitmp, dnistmp;
//					try {
//						anitmp = pt.get<string>("ani", "");
//						(*itChannel)->setAni( anitmp );			// ani
//						dnistmp = pt.get<string>("dnis", "");
//						(*itChannel)->setDnis( dnistmp );		// dnis
//						(*itChannel)->setSendFileName(fullNameWoExt + cfg::infoExt);// file name
//					} catch (boost::property_tree::ptree_error const &err) {
//						LOG4CXX_ERROR(logger, "send FAX name: " << fullNameWoExt + cfg::infoExt << " .Property file read ERROR (ani or dnis)");
//						outQueue_.processBadFile(fullNameWoExt);	// remove bad file
//						continue;
//					}
//					LOG4CXX_INFO(logger, "new OUT FAX. File: " << fullNameWoExt + cfg::infoExt << ", ani:" << anitmp << ", dnis:" << dnistmp);
//					/*
//					 * - set TX status on transmit channel
//					 * - open outgoing call
//					 * - signal to channel event manager to outcall
//					 */
//					(*itChannel)->setStatusTx();
//					(*itChannel)->TDM_OpenOutgoingCall();
//
//					break;
//				} // END if
//			}
//		} // END if( !fullNameWoExt.empty() )



	}// END main loop while(!exit)

	/*
	 * Signallin to all Channel FSM to exit
	 * - break (interrupt) connected threads
	 * - cleanup after exiting - release call handlers
	 */
	vector< boost::shared_ptr<Channel> >::iterator it;
	for(it=vspChannels.begin(); it!=vspChannels.end(); ++it)
		(*it)->proccessEvent(EVENT_EXIT);


	/*
	 * Free event Queue
	 */
	ACU_FREE_EVENT_QUEUE_PARMS		rFreeEventQueue;	// used for cleanup Aculab event queue before exiting
	INIT_ACU_STRUCT(&rFreeEventQueue);
	rFreeEventQueue.queue_id = queue_id;
	rc = acu_free_event_queue(&rFreeEventQueue);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "PEM acu_free_event_queue() " << acu_error_desc(rc));
	} else {
		LOG4CXX_INFO (logger, logHeader_ << "PEM acu_free_event_queue() OK");
	}
	LOG4CXX_DEBUG (logger, logHeader_ << "PEM STOPPED");

}; //END Port::eventManager()



void Port::initChannels() {
	int				rc;

	/*
	 * Get B-Channels Available...valid_vector
	 */
	INIT_ACU_CL_STRUCT(&cPortInfo);
	cPortInfo.port_id = port_id;
	rc = call_port_info(&cPortInfo); // using port_id, get all info in cPortInfo
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader_ << "PEM call_port_info() get B-channels vector. " << acu_error_desc(rc));
	} else {
		LOG4CXX_DEBUG (logger, logHeader_ << "PEM call_port_info() get B-channels vector. OK\n\t\t\tTimeslots vector: " << std::bitset<32>(cPortInfo.valid_vector).to_string());
	}

	/*
	 * running thru all valid channels vector
	 */
	// bit map 1 - working timeslot, 0 - not working. All 0 - nothing to work
	long	mask = 1L;	// sliding 1, for checking timeslot activity
	for(int ts = 0; ts < cfg::TDM_PORT_MAX_TIMESLOTS; ts++) {

		/*
		 * check this channel (bit in valid_vector) enabled
		 * Look Call_control_API_guide.pdf page 29
		 */
		if (mask & cPortInfo.valid_vector) { // check if this channel (bit in valid_vector) enabled
			// lock object CS inside while loop
			//boost::mutex::scoped_lock lock(CS); //TODO Port::initChannels() lock(CS) what for i don't know
			/*
			 * create new channel
			 * We have some dilemma:
			 * 	1. we need to create object, then take it pointer, convert it to unsigned long long int
			 * 		and pass it to Aculab driver like handler of object, responsible for this timeslot,
			 * 		invoking call_openin(&cOpenIn);
			 *
			 * 	2. From other side handler, which we received from call_openin(&cOpenIn) - cOpenIn.handle
			 * 		we need to put in our channel object. It's used in call acception
			 *
			 * 		So we need initialize port, then, after some operations, set it's Aculab handler
			 */
			boost::shared_ptr<Channel> spChannel(new Channel(spDSPHolder, this, ts));

			/*
			 * signal Channel FSM to wait for incoming (initial event)
			 */
			spChannel->proccessEvent(EVENT_RX_INIT);

			/*
			 * add channel to the vector
			 */
			vspChannels.push_back(spChannel);
		}// END if(mask ...)
		mask<<=1L;	// shift 00000000010000000... mask for the next timeslot checking
	}// END for
};// END void Port::initChannels()
