#include "dd.h"

using namespace std;
using namespace faxd;
namespace pt = boost::posix_time;

DeliverDescriptor::DeliverDescriptor(std::string path, std::string ext) :
	fileName_(""),
	path_(path),
	ext_(ext),
	serverId_(""),
	cardId_(0),
	portId_(0),
	ts_(0),
	ani_(""),
	dnis_(""),
	start_(pt::second_clock::local_time()),
	end_(pt::second_clock::local_time()),
	speed_(0),
	compression_(0),
	ecm_(0),
	pages_(0),
	errorCode_(FAX_RECEIVE_OK),
	errorDescr_("") {};

void DeliverDescriptor::setServerId(const std::string& serverId) {
	serverId_ = serverId;
};

void DeliverDescriptor::setAni(const std::string& ani) {
	ani_ = ani;
}

void DeliverDescriptor::setCardId(int cardId) {
	cardId_ = cardId;
}

void DeliverDescriptor::setDnis(const std::string& dnis) {
	dnis_ = dnis;
}

void DeliverDescriptor::setEcm(int ecm) {
	ecm_ = ecm;
}

void DeliverDescriptor::setEnd(const boost::posix_time::ptime& end) {
	end_ = end;
}

void DeliverDescriptor::setErrorCode(FaxReceiveStatus errorCode) {
	errorCode_ = errorCode;
	switch (errorCode_) {
	case FAX_RECEIVE_OK:
		errorDescr_ = "FAX_RECEIVE_OK";
		break;
	case FAX_RECEIVE_PARTIAL:
		errorDescr_ = "FAX_RECEIVE_PARTIAL";
		break;
	case FAX_RECEIVE_ERROR:
		errorDescr_ = "FAX_RECEIVE_ERROR";
		break;
	}
}

//void DeliverDescriptor::setErrorDescr() {
//	errorDescr_ = errorDescr;
//}

void DeliverDescriptor::setPages(int pages) {
	pages_ = pages;
}

void DeliverDescriptor::setPortId(int portId) {
	portId_ = portId;
}

void DeliverDescriptor::setSpeed(int speed) {
	speed_ = speed;
}

void DeliverDescriptor::setTiffSize(int tiffSize) {
	this->tiffSize_ = tiffSize;
}

void DeliverDescriptor::setCompression(int compression) {
	compression_ = compression;
}

void DeliverDescriptor::setStart(const boost::posix_time::ptime& start) {
	start_ = start;
}

void DeliverDescriptor::setTs(int ts) {
	ts_ = ts;
}

