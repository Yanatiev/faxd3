#include <sstream>
#include <iomanip>
#include <string>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/filesystem/operations.hpp>
#include <iostream>
#include <fstream>
#include <exception>

#include "channel-fsm.h"
#include "dd.h"
#include "dd-goe.h"
// C
#include "string.h"
//Aculab



using namespace faxd;

Fax::Fax(faxd::Channel *p_channel) :
		logger(log4cxx::Logger::getLogger( "main")),
		pChannel(p_channel),
		fsmRunning_(0)
{
};

void Fax::receiveFAXCallFlow() {
	namespace pt = boost::posix_time;

	std::stringstream ssHeader;
	ssHeader << "card s/n:" << pChannel->pPort->cardSerialNo <<
			" port:" << pChannel->pPort->port << ":" << pChannel->ts << " FAX RX <- ";
	std::string	logHeader = ssHeader.str();
	LOG4CXX_DEBUG (logger, " FAX receive START");

	/*
	 * Get start time of FAX receiption
	 */
	startTime = pt::second_clock::universal_time();

	/*
	 * calculate FAX's file name
	 */
	std::stringstream uuid;
	uuid << cfg::serverId << "_" << pChannel->pPort->cardSerialNo << "_" << cfg::cardsId[pChannel->pPort->cardSerialNo]
		<< "_" << pChannel->port_idx << "_" << std::setfill('0') << std::setw(2) << pChannel->ts << "_"; // "port-channel-"
	pt::time_facet*const f= new pt::time_facet("%d%H%M%S");
	uuid.imbue( std::locale(uuid.getloc(),f) );
	uuid <<  startTime;												// + "HH:MM:SS"
	std::string str;
	tiff_file	= uuid.str();
	info_file	= tiff_file;
	trace_file	= tiff_file;
	// append corresponding extensions
	tiff_file.append(".tiff");
	info_file.append(".info");
	trace_file.append(".trace");
	LOG4CXX_DEBUG (logger, logHeader << "filename: " << tiff_file);

	//set possible DATA RATES
	// for V.34

	/*
	 * set modem parameters, depending on DNIS
	 */
	v34 = v8 = ECM = v17 = v29 = 0;	// zeroize previous data
	//switch( cfg::getModemByDnis(pChannel->getDnis()) ) {
	CallProperty* cpPtr = cfg::getDnisSettings(pChannel->getDnis());
	switch( cpPtr->getModem() ) {
	case MODEM_V34 :
		v34	= v17 = v29 = v27ter = 1;
		v8	= 1;
		ECM	= 1;
		break;
	case MODEM_V17:
		v17	= 1;
	case MODEM_V29:
		v29 = 1;
	case MODEM_V27:
		v27ter = 1;
		ECM = cpPtr->getEcm();
	}

	max_data_rate = get_fax_data_rate_from_const(kSMFaxDataRate_Default, cpPtr->getModem()); // for compatibility, using only max:33600

	LOG4CXX_DEBUG (logger, logHeader <<	"modems request: v27=" << v27ter << " v29=" << v29 << " v17=" << v17 <<
			" v34=" << v34 << " ECM=" << ECM);
	LOG4CXX_DEBUG (logger, logHeader << "data rate request: " << max_data_rate << "kbps");

	//return_code = 0;
	trace 		= cpPtr->getTrace(); // cfg::FAXLogToFile;
	fax_id 		= 101;
	strcpy(subscriber, pChannel->ani.c_str());
	ts 			= pChannel->ts;		// timeslot
	faxpage 	= 0;				//old: &call_data->nCallFaxPage;
	TiNG_ChannelId	= pChannel->TiNG_channel_id;	// open chanel.
	module_id		= pChannel->TiNG_module_id;		// open module. pModuleData[call_data->module].module_id;
	//t38_media = &call_data->t38_media;

	/*
	 * Receive FAX
	 * Long procedure
	 */
	FaxReceiveStatus fsmExitCode = receiveFAX();
	endTime = pt::second_clock::universal_time();

	if( FAX_FILE_CREATE_ERROR == fsmExitCode ) { // unable to create locaf FAX file
		return;
	}

	/*
	 * Prepare Deliver Descriptor: .goe or .info standard
	 */
////	std::ofstream infoFile;

////	pt::ptime epoch(boost::gregorian::date(1970,1,1)); // http://www.boost.org/doc/libs/1_40_0/doc/html/date_time/examples.html#date_time.examples.seconds_since_epoch
////	boost::posix_time::time_duration deltaTime = endTime - startTime;

	/*
	 * create .info file in tmpDir
	 * fill it with attributes
	 * move .tiff and .info to output directory -> inDir (1st .tiff, 2nd .info)
	 * cleanup .trace, if it required
	 * section may be moved to SUCCESS switch section
	 */
	faxd::DeliverDescriptor *dd;
	if (cfg::deliverType == DELIVER_TYPE_GOE) {
		dd = new faxd::GOEDescriptor(cfg::tmpDir, ".goe", pChannel->pPort->cardSerialNo);
	} else {
		//todo change to ACU descriptor
		dd = new faxd::GOEDescriptor(cfg::tmpDir, ".goe", pChannel->pPort->cardSerialNo);
	}
	dd->setServerId(cfg::serverId);
	dd->setAni(pChannel->getAni()) ;
	dd->setCardId( atoi(cfg::cardsId[pChannel->pPort->cardSerialNo].c_str()) );
	dd->setDnis(pChannel->getDnis());
	dd->setEcm(fax_session.fax_caps.ECM);
	dd->setEnd(endTime);
	dd->setPages(page_count);
	dd->setPortId(pChannel->port_idx);
	dd->setSpeed(fax_session.fax_caps.data_rate);
	dd->setTiffSize( boost::filesystem::file_size((cfg::tmpDir + tiff_file).c_str()) );
	dd->setStart(startTime);
	dd->setTs(ts);
	dd->setCompression(fax_session.fax_caps.MR2D);
	dd->setErrorCode(fsmExitCode);

	if ( true == dd->create() ) { // if Delivery Descriptor successfully created
		LOG4CXX_DEBUG (logger, logHeader << "Delivery Descriptor created: " << cfg::tmpDir + dd->getName() + ".goe");
		try {
			if (cfg::deliverType == DELIVER_TYPE_GOE) {
				if(cpPtr->getKeepTmp()) { // prevent deletion from tmp based on settings for DNIS pattern
					boost::filesystem::copy_file( (cfg::tmpDir + tiff_file).c_str(), (cfg::inDir + cpPtr->getDeliverName() + dd->getName() + ".tif").c_str() );
					boost::filesystem::copy_file( (cfg::tmpDir + dd->getName() + ".goe").c_str(), (cfg::inDir + cpPtr->getDeliverName() + dd->getName() + ".goe").c_str() );
					boost::filesystem::rename( (cfg::tmpDir + tiff_file).c_str(), (cfg::tmpDir + dd->getName() + ".tif").c_str() );
					LOG4CXX_DEBUG (logger, logHeader << " keep in tmp files: " << dd->getName() << ".*");
				} else {
					boost::filesystem::rename( (cfg::tmpDir + tiff_file).c_str(), (cfg::inDir + cpPtr->getDeliverName() + dd->getName() + ".tif").c_str() );
					boost::filesystem::rename( (cfg::tmpDir + dd->getName() + ".goe").c_str(), (cfg::inDir + cpPtr->getDeliverName() + dd->getName() + ".goe").c_str() );
				}
			} else {
				//todo change to ACU descriptor
				boost::filesystem::rename( (cfg::tmpDir + tiff_file).c_str(), (cfg::inDir + tiff_file).c_str() );
				boost::filesystem::rename( (cfg::tmpDir + info_file).c_str(), (cfg::inDir + info_file).c_str() );
			}
		} catch (std::exception& e) {
			LOG4CXX_ERROR (logger, logHeader << e.what());
		}
	} else {
		LOG4CXX_ERROR (logger, logHeader << "Delivery Descriptor creation ERROR: " << cfg::tmpDir + dd->getName() + ".goe");
	}
	delete dd;
};


faxd::FaxReceiveStatus Fax::receiveFAX() {
	tSM_INT return_code = kSMFaxDCNRemoteNotReady;
	std::stringstream ssHeader;
	ssHeader << "card s/n:" << pChannel->pPort->cardSerialNo <<
			" port:" << pChannel->pPort->port << ":" << pChannel->ts << " FAX RX <- ";
	std::string	logHeader = ssHeader.str();

	int rc = 0, err = 0;
	SMFAX_TRANSPORT_MEDIUM fax_trans_parms;
	SMFAX_NEGOTIATE_PARMS fax_neg_parms;
	SMFAX_PAGE_PROCESS_PARMS fax_pp_parms;
	SMFAX_PAGE_ACCESS_PARMS fax_pa_parms;
	BFILE *bfp = 0;
	ACTIFF_FILE *outF;
	ACTIFF_PAGE_PROPERTIES	page_props;
	int actiff_error = 0;
//	int y = 0;
	ACTIFF_PAGE_HANDLE		pageH = 0;
	SMFAX_TRACE_PARMS		trace_parms;

	INIT_ACU_SM_STRUCT(&fax_neg_parms);
	INIT_ACU_SM_STRUCT(&fax_trans_parms);
	INIT_ACU_SM_STRUCT(&fax_session);
	INIT_ACU_SM_STRUCT(&fax_pp_parms);
	INIT_ACU_SM_STRUCT(&fax_pa_parms);
	INIT_ACU_SM_STRUCT(&trace_parms);
	page_count = 0;

	fax_session.channel	= TiNG_ChannelId;
	fax_session.module	= module_id;
	//TODO Fax::receiveFAX() code for IPCalls
	fax_session.user_options.max_percent_badlines = 0.1f;	// 10% of badline is acceptable (with ECM doesn't mean)
	fax_session.user_options.max_consec_badlines = 20;		// max number consecutive erroneous lines
	fax_session.user_options.max_modem_fb = 4;				// times to slower modem speed
	fax_session.user_options.page_retries = 3;				//
	fax_session.user_options.T38_ASN1_version = 2;			// look to SMFAX_T38_SDP_OPTIONS
	fax_session.user_options.ecm_continue_to_correct = 1;
	fax_session.user_options.drop_speed_on_ctc       = 1;   // reduce speed (in ECM enabled call)
	fax_session.user_options.SkipToneGen             = 0;	// not used
	fax_session.user_options.SkipSubsId              = 1;	// not used
	fax_session.user_options.StrictT30               = 0;	// strict T30 recommendations. Advised to be a 0

	fax_session.user_options.fax_mode = kSMFaxModeCalled; //change to calling for outgoing
	strcpy(fax_session.fax_caps.subscriber_id, subscriber);

	fax_session.fax_caps.data_rate = max_data_rate;	// speed valid for corresponding proto
	fax_session.fax_caps.v17 = v17;
	fax_session.fax_caps.v29 = v29;
	fax_session.fax_caps.v27ter = v27ter;
	fax_session.fax_caps.v34 = v34;
	fax_session.fax_caps.v8 = v8;					// ITU-T V.8 need for V.34
	fax_session.fax_caps.MR2D = 0;					// image compression Modified Read
	fax_session.fax_caps.MMRT6 = 0;					// image compression Modified Modified Read (ECM is mandatory)
	fax_session.fax_caps.ECM = ECM;					// ITU-T T.30 Annex A Error correction
	fax_session.fax_caps.Res200x200 = 1;			// inrease axis-Y resolution for hires images
	fax_session.fax_caps.polling_mode = 0;			// TX fax in answer on incoming call
	fax_session.fax_caps.ECMsize64 =0;
	fax_session.fax_caps.NoRcvrOp = 0;				// reserved for future use
	//fax_session.fax_caps.TIFFWidth = kSMFaxTIFFWidth_A3;

	fax_session.fax_id = fax_id;

	// open file for tiff writting
	outF = actiff_write_open( (cfg::tmpDir + tiff_file).c_str(), 0, &actiff_error);
	if(!outF || actiff_error) {
		LOG4CXX_ERROR (logger, logHeader << "Unable to open TIFF File for writing " << tiff_file);
		return_code = kSMFaxNoSessionCreated;
		return FAX_FILE_CREATE_ERROR;
	}

	/*
	 * enable low level FAX API trace
	 */
	if (trace) {
		// trace_parms.fax_session = &fax_session;
		rc = bfile(&bfp);
		char tmp[256];
		strncpy(tmp, (cfg::tmpDir + trace_file).c_str(), 100);
		rc = bfopen(bfp, tmp, "wtc");
		trace_parms.log_file = bfp;
		trace_parms.trace_level = TRACE_ALL;
		fax_session.trace_parms = &trace_parms;
	}

	/*
	 * This function performs the internal preparation for a new fax session. The structure passed in, as
	 * a parameter, will provide necessary information to aid the desired set-up of the fax session
	 * 	- DSP channel, speed, proto ("caps" i.e capabilities)
	 * 	- returns:
	 * 		kSMFaxStateMachineRunning
	 * 		kSMFaxstateMachineTerminated
	 */
	rc = smfax_create_session( &fax_session);
	if(rc != kSMFaxStateMachineRunning) {
		LOG4CXX_ERROR (logger, logHeader << "smfax_create_session() for " << tiff_file << " ERROR " << smfax_error_to_text(fax_session.exit_error_code));
	} else if (trace) {
		LOG4CXX_DEBUG (logger, logHeader << "smfax_create_session() OK. for " << tiff_file << " SESSION STARTED");
	}

	/*
	 * Negotiate, Receive and Store FAX
	 */
	if(rc == kSMFaxStateMachineRunning) {
		fsmRunning_	= 1;	// FAX state machine is running (flag for interrupt method)
		while(rc == kSMFaxStateMachineRunning) {
			/*
			 * negotiation phase (handshake)
			 */
			memset(&page_props, 0, sizeof(page_props));
			fax_neg_parms.fax_session	= &fax_session;
			fax_neg_parms.page_props	= &page_props;

			LOG4CXX_INFO (logger, logHeader << "smfax_rx_negotiate() START " << tiff_file << " proposed proto: " <<	faxCapsToString(&fax_neg_parms.fax_session->fax_caps) );
			/*
			 * Method waits end of ITU-T T.30/Phase B (negotiation phase)
			 * returns:
			 * 	- kSMFaxStateMachineRunning
			 * 	- kSMFaxstateMachineTerminated
			 *
			 * 	but it cans set fax_neg_parms.is_polling в kSMFaxPolling, it's means that our peer request
			 * 	TX fax from our side (poll our out queue) !!!
			 *
			 * 	But we set fax_session.fax_caps.polling_mode = 0; So we are no interesting in polling.
			 */
			//rc = smfax_rx_negotiate(&fax_neg_parms);
			rc = smfax_negotiate(&fax_neg_parms);
			if(rc != kSMFaxStateMachineRunning) {
				LOG4CXX_ERROR (logger, logHeader << "smfax_rx_negotiate() " << tiff_file << " ERROR " << smfax_error_to_text(fax_session.exit_error_code));
			} else {
				LOG4CXX_INFO (logger, logHeader << "smfax_rx_negotiate() COMPLETE " << tiff_file << " proto: " << faxCapsToString(&fax_neg_parms.fax_session->fax_caps));
				// infact clear renegotiate flag in case of renegotiation
				INIT_ACU_SM_STRUCT(&fax_pp_parms);
			}

			if(rc == kSMFaxStateMachineRunning) {
				/*
				 * if(...) for preventing "polling" behavior of our peer. We don't need TX FAX in response of
				 * incoming call
				 */
				if(kSMFaxPolling == fax_neg_parms.is_polling) {
					LOG4CXX_ERROR (logger, logHeader << "WARNING: Our peer trying to poll our TX queue. Interrupt their." << tiff_file);
					/*
					 * rude interrupt in case of REMOTE_DISCONNECT polite interrupt in case of incorrect negotiation
					 */
					rc = smfax_polite_interrupt(&fax_session);
					if(rc != kSMFaxStateMachineRunning) {
						LOG4CXX_ERROR (logger, logHeader << "smfax_polite_interrupt() ERROR" << rc << " " << tiff_file);
					}
				}

				/*
				 * while fax session still active and renegotiation after rx_page is not needed
				 */
				while( (rc == kSMFaxStateMachineRunning) && (fax_pp_parms.renegotiate==0) ) {
					faxpage = page_count+1;

					/*
					 * receive page
					 */
					fax_pp_parms.fax_session = &fax_session;
					fax_pp_parms.page_handle = &pageH;

					LOG4CXX_INFO (logger, logHeader << "page " << faxpage << " - smfax_rx_page() START "  << tiff_file );
					rc = smfax_rx_page(&fax_pp_parms);
					if((rc != kSMFaxStateMachineTerminated)&&(rc != kSMFaxStateMachineRunning)) {
						LOG4CXX_ERROR (logger, logHeader << "page " << faxpage << " - smfax_rx_page() ERROR: " << smfax_error_to_text(fax_session.exit_error_code) << " " << tiff_file );
					} else {
						LOG4CXX_INFO (logger, logHeader << "page " << faxpage << " - smfax_rx_page() COMPLETE "  << tiff_file << ", renegotiation: " << fax_pp_parms.renegotiate <<
								", protocol: " << faxCapsToString(&fax_neg_parms.fax_session->fax_caps) << " code: " << smfax_error_to_text(fax_session.exit_error_code)  );
					}
					// we need return_code after last success page,
					// for example smfax_close_session() possible complete it too
					return_code = fax_session.exit_error_code;

					/*
					 * store page
					 */
					fax_pa_parms.fax_session = &fax_session;
					fax_pa_parms.page_handle = fax_pp_parms.page_handle;
					fax_pa_parms.actiff      = outF;
					err = smfax_store_page(&fax_pa_parms);
					if(err != kSMFaxPageOK) {
						LOG4CXX_ERROR (logger, logHeader << "page " << faxpage << " - smfax_store_page() " << tiff_file << " ERROR " << err);
					} else {
						LOG4CXX_INFO (logger, logHeader << "page " << faxpage << " - smfax_store_page() " << tiff_file);
					}

					/*
					 * close page
					 */
					err = smfax_close_page(&pageH);
					if(err != kSMFaxPageOK) {
						LOG4CXX_ERROR (logger, logHeader << "page " << faxpage << " - smfax_close_page() " << tiff_file << " ERROR " << err);
					} else {
						LOG4CXX_INFO (logger, logHeader << "page " << faxpage << " - smfax_close_page() " << tiff_file);
					}

					page_count++;	// one more good page
					//LOG4CXX_DEBUG (logger, logHeader << "LOOP to receive more pages: rc==" << rc << " fax_pp_parms.renegotiate=" << fax_pp_parms.renegotiate);
				}//END while(kSMFaxStateMachineRunning == rc && !fax_pp_parms.renegotiate)

			}//END if(rc == kSMFaxStateMachineRunning)

		}//END while(rc == kSMFaxStateMachineRunning)

		fsmRunning_	= 0;	// clear FAX state machine flag

		rc = smfax_close_session(&fax_session);
		if(rc) {
			LOG4CXX_ERROR (logger, logHeader << "smfax_close_session() " << tiff_file << " ERROR: " << smfax_error_to_text(fax_session.exit_error_code));
		} else {
			LOG4CXX_INFO (logger, logHeader << "smfax_close_session() " << tiff_file);
		}
	}//END if(rc == kSMFaxStateMachineRunning)


	rc = actiff_close(outF, &actiff_error);
	if(rc) {
		LOG4CXX_ERROR (logger, logHeader << "actiff_close() " << tiff_file << " ERROR " << rc);
	} else {
		LOG4CXX_DEBUG (logger, logHeader << "actiff_close() " << tiff_file);
	}

	/*
	 * if trace was enabled (trace file created). Close it
	 */
	if(bfp) {
		rc = bfile_dtor(bfp);
		if(rc) {
			LOG4CXX_ERROR (logger, logHeader << "bfile_dtor() " << tiff_file << " ERROR " << rc);
		}
	}

	/*
	 * prepare return code
	 */
	if( page_count > 0 ) {
		if( kSMFaxDCNNormal == return_code || kSMFaxDCNNoDCNFromRemote == return_code )
			return FAX_RECEIVE_OK;
		else
			return FAX_RECEIVE_PARTIAL;
	} else {
		return FAX_RECEIVE_ERROR;
	};
	//return return_code;
};


int Fax::get_fax_data_rate_from_const(int data_rate_const, int fax_modem)
{
	int data_rate = 0;

	switch (data_rate_const)
	{
		case kSMFaxDataRate_Default:
			switch (fax_modem)
			{
				case MODEM_V34:
					data_rate = 33600;
					break;
				case MODEM_V17:
					data_rate = 14400;
					break;
				case MODEM_V29:
					data_rate = 9600;
					break;
				case MODEM_V27:
					data_rate = 4800;
					break;
			}
			break;
		case kSMFaxDataRate_2400:
			data_rate = 2400;
			break;
		case kSMFaxDataRate_4800:
			data_rate = 4800;
			break;
		case kSMFaxDataRate_7200:
			data_rate = 7200;
			break;
		case kSMFaxDataRate_9600:
			data_rate = 9600;
			break;
		case kSMFaxDataRate_12000:
			data_rate = 12000;
			break;
		case kSMFaxDataRate_14400:
			data_rate = 14400;
			break;
		case kSMFaxDataRate_16800:
			data_rate = 16800;
			break;
		case kSMFaxDataRate_19200:
			data_rate = 19200;
			break;
		case kSMFaxDataRate_21600:
			data_rate = 21600;
			break;
		case kSMFaxDataRate_24000:
			data_rate = 24000;
			break;
		case kSMFaxDataRate_26400:
			data_rate = 26400;
			break;
		case kSMFaxDataRate_28800:
			data_rate = 28800;
			break;
		case kSMFaxDataRate_31200:
			data_rate = 31200;
			break;
		case kSMFaxDataRate_33600:
			data_rate = 33600;
			break;
	}
	return data_rate;
}

void Fax::sendFAXCallFlow() {
	tSM_INT return_code = kSMFaxDCNRemoteNotReady;

	LOG4CXX_DEBUG (logger, "Fax::sendFAXCallFlow() START");
	/*
	 * set modem parameters, depending on DNIS
	 */
	v34 = v8 = ECM = v17 = v29 = 0;	// zeroize previous data
	CallProperty* cpPtr = cfg::getDnisSettings(pChannel->getDnis());
	switch( cpPtr->getModem() ) {
	case MODEM_V34 :
		v34	= v17 = v29 = v27ter = 1;
		v8	= 1;
		ECM	= 1;
		break;
	case MODEM_V17:
		v17	= 1;
	case MODEM_V29:
		v29 = 1;
	case MODEM_V27:
		v27ter = 1;
		ECM = cpPtr->getEcm();
	}

	max_data_rate = get_fax_data_rate_from_const(kSMFaxDataRate_Default, cpPtr->getModem()); // for compatibility, using only max:33600
	LOG4CXX_DEBUG (logger, "Fax::sendFAXCallFlow() MODEMS TO REQUEST DURING NEGOTIATION: v27="
				<< v27ter << " v29=" << v29 << " v17=" << v17 << " v34=" << v34 << " ECM=" << ECM);
	LOG4CXX_DEBUG (logger, "DATA RATE TO REQUEST DURING NEGOTIATION: " << max_data_rate << "kbps");

	strcpy(subscriber, pChannel->ani.c_str());

	sendFAX();

	if (return_code == kSMFaxDCNNormal) {
			LOG4CXX_DEBUG (logger, "Fax::sendFAXCallFlow() FAX send SUCCESS port/ts" << pChannel->pPort->port << "/" << pChannel->ts);
			//TODO check for correct call disconnect
//			pChannel->TDM_DisconnectCall();
	} else {
		LOG4CXX_ERROR (logger, "Fax::sendFAXCallFlow() FAX send FAILED port/ts" << pChannel->pPort->port << "/" << pChannel->ts <<
				" ERROR " << return_code );
	}
};

void Fax::sendFAX() {
	tSM_INT return_code = kSMFaxDCNRemoteNotReady;

	int rc = 0, err = 0;
	SMFAX_SESSION fax_session;
	SMFAX_TRANSPORT_MEDIUM fax_trans_parms;
	SMFAX_NEGOTIATE_PARMS fax_neg_parms;
	SMFAX_PAGE_PROCESS_PARMS fax_pp_parms;
	SMFAX_PAGE_ACCESS_PARMS fax_pa_parms;
	BFILE *bfp = 0;
	ACTIFF_FILE *inF;
	ACTIFF_PAGE_PROPERTIES app, page_props;
	int actiff_error = 0;
	int pgTotal = 0;
//	int y = 0;
	int pgIdx = 0;
	ACTIFF_PAGE_HANDLE  pageH = 0;
	SMFAX_TRACE_PARMS					trace_parms;
	//CANCEL_FAX_STRUCT  *tmp_struct = calloc(1, sizeof (CANCEL_FAX_STRUCT));
	//ACU_OS_THREAD *cancel_thread;

	//INIT_ACU_SM_STRUCT(&fax_neg_parms);
	INIT_ACU_SM_STRUCT(&fax_trans_parms);
	INIT_ACU_SM_STRUCT(&fax_session);
	INIT_ACU_SM_STRUCT(&fax_pp_parms);
	INIT_ACU_SM_STRUCT(&fax_pa_parms);
	INIT_ACU_SM_STRUCT(&trace_parms);

	fax_session.channel	= TiNG_ChannelId;
	fax_session.module	= module_id;

	fax_session.user_options.max_percent_badlines = 0.1f;
	fax_session.user_options.max_consec_badlines = 20;
	fax_session.user_options.max_modem_fb = 4;
	fax_session.user_options.page_retries = 3;
	fax_session.user_options.T38_ASN1_version = 2;
	fax_session.user_options.ecm_continue_to_correct = 1;
	fax_session.user_options.drop_speed_on_ctc       = 1;   // true or false
	fax_session.user_options.SkipToneGen             = 0;
	fax_session.user_options.SkipSubsId              = 1;
	fax_session.user_options.StrictT30               = 0;

	fax_session.user_options.fax_mode = kSMFaxModeCalled; //change to calling for outgoing
	strcpy(fax_session.fax_caps.subscriber_id, subscriber);

	fax_session.fax_caps.data_rate = max_data_rate;
	fax_session.fax_caps.v17 = v17;
	fax_session.fax_caps.v29 = v29;
	fax_session.fax_caps.v27ter = v27ter;
	fax_session.fax_caps.v34 = v34;
	fax_session.fax_caps.v8 = v8;
	fax_session.fax_caps.MR2D = 0;
	fax_session.fax_caps.MMRT6 = 0;
	fax_session.fax_caps.ECM = ECM;
	fax_session.fax_caps.Res200x200 = 1;
	fax_session.fax_caps.polling_mode = 0;
	fax_session.fax_caps.ECMsize64 =0;
	fax_session.fax_caps.NoRcvrOp = 0;

	fax_session.fax_id = fax_id;

	inF = actiff_read_open( (pChannel->sendFileName_).c_str(), &actiff_error);
	if(inF && !actiff_error) {
		pgTotal = get_num_pages(inF);
		if(pgTotal < 1)	{
			LOG4CXX_DEBUG (logger, "Fax::receiveFAX() Unable to open TIFF File for writing " << pChannel->sendFileName_);
			return;
		}
	}
	memset(&app, 0, sizeof(app));
	actiff_page_properties(inF, &app);
	fax_session.fax_caps.TIFFWidth = kSMFaxTIFFWidth_A4;
	if(app.image_width == 2048)
	{
		fax_session.fax_caps.TIFFWidth = kSMFaxTIFFWidth_B4;
	}
	//TODO sendFAX() trace session code

	rc = smfax_create_session( &fax_session);
	if(rc != kSMFaxStateMachineRunning) {
		LOG4CXX_ERROR (logger, "Fax::sendFAX() smfax_create_session() for " << pChannel->sendFileName_ << " ERROR " << smfax_error_to_text(fax_session.exit_error_code));
	} else if (trace != 0) {
		LOG4CXX_DEBUG (logger, "Fax::sendFAX() smfax_create_session() OK. for " << pChannel->sendFileName_ << " TRACE STARTED");
	}
	/*
	 * now FAX machine is running. Process a FAX
	 */
	if(rc == kSMFaxStateMachineRunning) {

		LOG4CXX_DEBUG (logger, "Fax::sendFAX() smfax_create_session() for " << pChannel->sendFileName_ << " FAX SESSION STARTED current rc=" << rc);

		fsmRunning_	= 1;	// FAX state machine is running (flag for interrupt method)
		while(rc == kSMFaxStateMachineRunning) {
			/*
			 * negotiation phase
			 */
			LOG4CXX_DEBUG (logger, "Fax::sendFAX() smfax_rx_negotiate() START  " << pChannel->sendFileName_);
			memset(&page_props, 0, sizeof(page_props));
			fax_neg_parms.fax_session	= &fax_session;
			fax_neg_parms.page_props	= &page_props;
			/*
			 * fax handshake
			 */
			rc = smfax_tx_negotiate(&fax_neg_parms);
			//rc = smfax_negotiate(&fax_neg_parms);
			LOG4CXX_DEBUG (logger, "Fax::receiveFAX() smfax_rx_negotiate() COMPLETE  " << pChannel->sendFileName_);
			if(rc != kSMFaxStateMachineRunning) {
				LOG4CXX_ERROR (logger, "Fax::sendFAX() smfax_rx_negotiate() for " << pChannel->sendFileName_ << " ERROR " << smfax_error_to_text(fax_session.exit_error_code));
			}


			if(kSMFaxStateMachineRunning == rc) {
				if(kSMFaxPolling == fax_neg_parms.is_polling)
				{
					LOG4CXX_ERROR (logger, "WARNING: Polled session established. " << pChannel->sendFileName_);
					LOG4CXX_ERROR (logger, "WARNING: Issuing polite interrupt.. " << pChannel->sendFileName_);
					rc = smfax_polite_interrupt(&fax_session);
					if(rc) {
						LOG4CXX_ERROR (logger, "smfax_rx_negotiate " << rc << " " << pChannel->sendFileName_);
					}
					LOG4CXX_DEBUG (logger, "POLITE INTERRUPT  " << pChannel->sendFileName_);
				}
				LOG4CXX_DEBUG (logger, "NEGOTIATE COMPLETED " << pChannel->sendFileName_);

				//for(; kSMFaxStateMachineRunning == rc && !fax_pp_parms.renegotiate; )
				for(; kSMFaxStateMachineRunning == rc && ( (pgIdx-1) < pgTotal); pgIdx++)
				{

					fax_pa_parms.fax_session = &fax_session;
					fax_pa_parms.page_handle = &pageH;
					fax_pa_parms.page_index  = pgIdx;
					fax_pa_parms.page_props  = &page_props;
					fax_pa_parms.actiff      = inF;
					err = smfax_load_page(&fax_pa_parms);
					if(kSMFaxPageOK != err) {
						LOG4CXX_ERROR (logger, "smfax_load_page() " << pgIdx << " file:" << pChannel->sendFileName_ << " ERROR");
					}

					LOG4CXX_INFO(logger, "start transmit page " << pgIdx << " file:" << pChannel->sendFileName_);

					//*fax_param->faxpage = pgIdx+1;

					//Setup for transmitting the page
					fax_pp_parms.is_last_page = ((pgIdx+1) == pgTotal) ? kSMFaxLastPage : kSMFaxNotLastPage;
					fax_pp_parms.fax_session  = &fax_session;
					fax_pp_parms.page_handle  = &pageH;
					rc = smfax_tx_page(&fax_pp_parms);
					if( (rc != kSMFaxStateMachineTerminated)&&(rc != kSMFaxStateMachineRunning) ) {
						LOG4CXX_ERROR (logger, "smfax_tx_page() " << pgIdx << " file:" << pChannel->sendFileName_ << " ERROR");
					}
					LOG4CXX_INFO(logger, "Transmiting page " << pgIdx << " file:" << pChannel->sendFileName_ << " COMPLETE");

					err = smfax_close_page(&pageH);
					if(err != kSMFaxPageOK) {
						LOG4CXX_ERROR (logger, "smfax_close_page() " << pgIdx << " file:" << pChannel->sendFileName_ << " ERROR");
					}
				} // END for(; kSMFaxStateMachineRunning == rc && ( (pgIdx-1) < pgTotal); pgIdx++)
			} // END if(kSMFaxStateMachineRunning == rc)
		}// END while(rc == kSMFaxStateMachineRunning)

		fsmRunning_	= 0;	// clear FAX state machine flag

		rc = smfax_close_session(&fax_session);
		if(rc) {
			LOG4CXX_ERROR (logger, "smfax_close_session() " << pChannel->sendFileName_ << " ERROR " << rc);
		}
		LOG4CXX_DEBUG (logger, "smfax_close_session() " << pChannel->sendFileName_);
	}//END if(rc == kSMFaxStateMachineRunning)
	rc = actiff_close(inF, &actiff_error);
	if(rc) {
		LOG4CXX_ERROR (logger, "actiff_close() " << pChannel->sendFileName_ << " ERROR " << rc);
	}
	if(bfp) {
		rc = bfile_dtor(bfp);
		if(rc) {
			LOG4CXX_ERROR (logger, "bfile_dtor() " << pChannel->sendFileName_ << " ERROR " << rc);
		}
	}
	LOG4CXX_DEBUG (logger, "CLOSE TIFF " << pChannel->sendFileName_);
	return_code = fax_session.exit_error_code;
}

int Fax::interrupt() {
	int rc=0;
	if (fsmRunning_ == 1) {
		rc = smfax_rude_interrupt(&fax_session);
	}
	return rc;
}

int Fax::get_num_pages(ACTIFF_FILE *actiff_){
	int pg_count = 0;
	if(!actiff_) return -1;

	/*~~~~~~~~~~ACULAB FAX API COMMENT~~~~~~~~~~~~~~~~~~~~~~~~
	  Loop through ACTIFF_FILE structure and count the number
	  of pages present
	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	while(actiff_seek_page(actiff_, pg_count) == 0){
		pg_count ++;
	}
	return pg_count;
}

std::string Fax::faxCapsToString(SMFAX_CAPS *faxCaps) {
	std::stringstream ss;
	ss << "v34=" << faxCaps->v34 << " v17=" << faxCaps->v17 << " v29=" << faxCaps->v29 << " v27ter=" << faxCaps->v27ter <<
			" ecm=" << faxCaps->ECM << " speed=" << faxCaps->data_rate;
	return ss.str();
};
