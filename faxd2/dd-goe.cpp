#include <iostream>
#include <fstream>
#include <string>
// for randomizer
#include <stdlib.h>
#include <time.h>

#include "dd-goe.h"

using namespace std;
using namespace faxd;

namespace pt = boost::posix_time;

GOEDescriptor::GOEDescriptor(std::string path, std::string ext, std::string name_suffix) :
	DeliverDescriptor(path, ext),
	name_suffix_(name_suffix),
	logger(log4cxx::Logger::getLogger( "main"))
{};

GOEDescriptor::~GOEDescriptor() {};

bool GOEDescriptor::create() {

	/*
	 * generate 14-digit token (used inside .goe and in file name)
	 */
	stringstream token;
	string chars(
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"1234567890");
	srand (time(NULL) + (portId_ << 5) + ts_);
	for(int i = 0; i < 14 - name_suffix_.size(); ++i) {
		token << chars[random()%(chars.size() - 1)];
	}
	token << name_suffix_;

	/*
	 * get new filename (with full path, but without extension) for goe and tif files
	 */
	stringstream name;
	//name << path_;
	pt::time_facet*const f= new pt::time_facet("%Y%m%d%H%M%S");
	name.imbue( std::locale(name.getloc(),f) );
	name <<  end_;
	string digits("0123456789");
	srand (time(NULL));
	for(int i = 0; i < 3; ++i) {
		name << digits[random()%(digits.size() - 1)];
	}
	name << "_" << token.str(); // append token to filename
	fileName_ = name.str();

	/*
	 * add fields to file
	 */
	std::ofstream infoFile;
	try {
		infoFile.open ( (path_ + fileName_ + ".goe").c_str(), std::fstream::out | std::fstream::trunc );
	} catch (ios_base::failure ex) {
		return false;
	}

	infoFile << "[header]" << endl;
	infoFile << "InputChannel        = EFN" << endl;
	infoFile << "MessageID           =" << endl;
	infoFile << "SubscriberID        =" << endl;
	infoFile << "SubscriberGroup     = 0" << endl;
	infoFile << "Node                = " << serverId_ << endl;
	infoFile << "Pna                 = 9" << endl;
	infoFile << "OriginCLI           = " << ani_ << endl;
	infoFile << "OriginAddress       = " << endl;
	infoFile << "StatusCode          = " << errorCode_ << endl;
	infoFile << "SubmissionTime      = " << (end_ - boost::posix_time::ptime(boost::gregorian::date(1970,1,1))).total_milliseconds() << endl;
	infoFile << "Subject             = " << endl;
	infoFile << "GeneralStatus       = 1" << endl;
	infoFile << "MessageType         = 1" << endl;
	infoFile << "InputSeqNumber      = 1" << endl;
	infoFile << "CoverPage           = 1" << endl;
	infoFile << "DeliveryAdvice      = " << endl;
	infoFile << "NotificationAddress = " << endl;
	infoFile << "MailboxDelivery     = 0" << endl;
	infoFile << "DidInfo             = " << dnis_ << endl;
	infoFile << endl;
	infoFile << "[address_0]" << endl;
	infoFile << "Type                 = 2" << endl;
	infoFile << "AddressID            = 0" << endl;
	infoFile << "From                 = " << endl;
	infoFile << "Attention            = " << endl;
	infoFile << "SubscriberRef        = " << ((errorCode_== FAX_RECEIVE_PARTIAL) ? "##PR6014##" : "") << endl;
	infoFile << "SourceMinicode       = " << endl;
	infoFile << "SourceMinicodeAdd    = 0" << endl;
	infoFile << "DeptBilling          = " << endl;
	infoFile << "DefferredTime        = 0" << endl;
	infoFile << "CancellationTime     = 0" << endl;
	infoFile << "ExpectedDeliveryTime = 0" << endl;
	infoFile << "DeliveryClass        = 0" << endl;
	infoFile << "Priority             = 0" << endl;
	infoFile << "CurrentStatus        = 1" << endl;
	infoFile << "TypeOfService        = 1" << endl;
	infoFile << "RoutingCode          = 0" << endl;
	infoFile << "DocumentFormatID     = 0" << endl;
	infoFile << "LogoID               = " << endl;
	infoFile << "MainRecipientAddr    = " << dnis_ << endl;
	infoFile << "CurrentAddrOrder     = 0" << endl;
	infoFile << "ActAltAddrNum        = 0" << endl;
	infoFile << "AdditionalFlags      = 0" << endl;
	infoFile << "OrigSpecAddrType     = 0" << endl;
	infoFile << "OrigSpecAddr         =  " << endl;
	infoFile << endl;
	infoFile << "[bodypart_0]" << endl;
	infoFile << "Type       = 1" << endl;
	infoFile << "Order      = 0" << endl;
	infoFile << "Token      = " << token.str() << endl;
	infoFile << "CurrentAddrOrder     = 0" << endl;
	infoFile << "ActAltAddrNum        = 0" << endl;
	infoFile << "AdditionalFlags      = 0" << endl;
	infoFile << "OrigSpecAddrType     = 0" << endl;
	infoFile << "OrigSpecAddr         = " << endl;
	infoFile << endl;
	infoFile << "[bodypart_0]" << endl;
	infoFile << "Type       = 1" << endl;
	infoFile << "Order      = 0" << endl;
	infoFile << "Token      = " << token.str() << endl;
	infoFile << "Group      = 51" << endl;
	infoFile << "Node       = " << serverId_ << endl;
	infoFile << "Pages      = " << pages_ << endl;
	infoFile << "Duration   = " << (end_ - start_).total_seconds() << endl;
	infoFile << "Resolution = 70" << endl;
	stringstream ss;
	ss << speed_;
	infoFile << "Speed      = " << ss.str().substr(0, ss.str().length()-2) << endl;
	infoFile << "Size       = " << (int)tiffSize_<< endl;
	infoFile << "Pathname   = " << endl;
	infoFile << "AsciiSize  = 0" << endl;
	infoFile << "AsciiBody  = " << endl;
	infoFile << "MirrorPath = " << endl;
	infoFile << "" << endl;

	infoFile.close();
	return true;
}
