/*
 * basicconfig.h
 *
 *  Created on: Jun 24, 2014
 *      Author: yanat
 */

#ifndef BASICCONFIG_H_
#define BASICCONFIG_H_

#include <string>

namespace faxd {

/**
 * @class BasicConfig basicconfig.h
 * @brief Interface to any Config holder objects
 * @details provide basic interface to get integer and string variables by key
 */
	class BasicConfig {
	public:
		/**
		 * @brief interface (virtual method with 0 signature)
		 * @param key			std::string parameter name
		 * @param defaultValue	int default value, if parameter with "key" name is not found
		 * @return	int value, or defaultValue
		 */
		virtual int getInt(std::string key, int defaultValue) = 0;
		/**
		 * @brief interface (virtual method with 0 signature)
		 * @param key			std::string parameter name
		 * @param defaultValue	std::string default value, if parameter with "key" name is not found
		 * @return	std::string value, or defaultValue
		 */
		virtual std::string getStr(std::string key, std::string defaultValue) = 0;
	};
}

#endif /* BASICCONFIG_H_ */
