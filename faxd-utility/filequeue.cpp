/*
 * filequeue.cpp
 *
 *  Created on: Jun 25, 2014
 *      Author: root
 */

#include <sstream>
#include <boost/filesystem.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
// for check file existance
#include <sys/stat.h>
#include <iostream>
#include "filequeue.h"


using std::cout;
using std::endl;
using std::string;
using std::stringstream;
using namespace faxd;
using namespace std;
using namespace log4cxx;
using namespace boost::posix_time;
namespace fs = boost::filesystem;

using boost::posix_time::ptime;
using boost::posix_time::minutes;



FileDescriptor::FileDescriptor(std::string shortFaxName, std::string shortInfoName) :
		status_(NEW)
{
	shortFaxName_ = shortFaxName;
	shortInfoName_ = shortInfoName;
};

FileDescriptor::FileDescriptor(const FileDescriptor& fd) :
		shortFaxName_(fd.shortFaxName_),
		shortInfoName_(fd.shortInfoName_),
		status_(fd.status_),
		waitTill_(fd.waitTill_)
{};

std::string FileDescriptor::toString() {
	stringstream ss;
	ss << "status: " << FileStatusStr[status_];
	if(status_ == DELAYED)
		ss << " delayed till: " << waitTill_;
	return ss.str();
};


FileQueue::FileQueue(BasicConfig& cfg, std::string queueName) :
		cfg_(cfg),
		logger(Logger::getLogger( "main")),
		queueName_(queueName),
		scanThreadExit_(false),
		scanThread_(boost::bind(&FileQueue::scanWorker, this))
{
	faxExt_				= cfg.getStr(queueName_ + ".faxExt", ".fax");
	infoExt_			= cfg.getStr(queueName_ + ".infoExt", ".info");
	badDir_				= cfg.getStr(queueName_ + ".badDir", "/var/spool/faxd/unnamedQueue/bad");
	workingDir_			= cfg.getStr(queueName_ + ".workingDir", "/var/spool/faxd/unnamedQueue/in");
	delayWaitMinutes_	= cfg.getInt(queueName_ + ".delayWaitMinutes", 5);
	scanPeriod_			= cfg.getInt(queueName_ + ".scanPeriod", 0);	// 0 means - manually scan, running from main thread, not from separate scan thread

};

FileQueue::~FileQueue() {
	/*
	 * terminate worker thread
	 */
	scanThreadExit_ = true;
	scanThread_.join();

	/*
	 * cleanup file queue (map)
	 */
	queue_.clear();
}

void FileQueue::test() {
	cout << "hello from library: faxd-utility" << endl;
	cout << cfg_.getStr("test", "test4") << endl;
};

void FileQueue::addFile(string fullNameWoExt, std::string faxName, std::string infoName) {
	{
		boost::mutex::scoped_lock lock(qMutex_);	// LOCK
		queue_[fullNameWoExt] = boost::shared_ptr<FileDescriptor>(new FileDescriptor(faxName, infoName));
	}
	LOG4CXX_DEBUG(logger, "FileQueue::addFile(\"" << fullNameWoExt << "\")" << " ADDED. FD " << queue_[fullNameWoExt]->toString());
};



void FileQueue::eraseFile(std::string fullNameWoExt) {
	boost::mutex::scoped_lock lock(qMutex_);	// LOCK

	/*
	 * erase files from filesystem
	 */
	fs::path fileFax( fullNameWoExt + faxExt_ );
	if(fs::exists(fileFax))
		fs::remove(fileFax);

	fs::path fileInfo( fullNameWoExt + infoExt_ );
	if(fs::exists(fileInfo))
		fs::remove(fileInfo);

	if( queue_.erase(fullNameWoExt) )
		LOG4CXX_DEBUG(logger, "FileQueue::eraseFile(\"" << fullNameWoExt << "\")" << " ERASED");
};

string FileQueue::getFile(boost::shared_ptr<FileDescriptor> & spfd) {
	boost::mutex::scoped_lock lock(qMutex_);	// LOCK

	map<string, boost::shared_ptr<FileDescriptor> >::iterator it;
	for (it=queue_.begin(); it!=queue_.end(); ++it) {
		/*
		 * try to find new file
		 */
		if( it->second->status_== NEW ) {
			it->second->status_ = SENDING;
			spfd = it->second;
			return it->first;
		}
		/*
		 * try to find delayed file with expired delay timer
		 */
		if( it->second->status_ == DELAYED ) {
			// check for wait timer expiration
			ptime now = second_clock::local_time();
			if( now > it->second->waitTill_ ) {
				LOG4CXX_TRACE(logger, "FileQueue::getFile(). Trying to resend files: " << it->first);
				it->second->status_ = SENDING;
				spfd = it->second;
				return it->first;
			}
		}
	}
	return "";
};

int FileQueue::getNumFilesToSend() {
	boost::mutex::scoped_lock lock(qMutex_);	// LOCK

	int counter = 0;
	map<string, boost::shared_ptr<FileDescriptor> >::iterator it;
	for(it=queue_.begin(); it!=queue_.end(); ++it) {
		// if it new file in queue
		if (it->second->status_ == NEW)
			counter++;
		// if delayed timer on file expired
		if ( (it->second->status_ == DELAYED) && (second_clock::local_time() > it->second->waitTill_ ) )
			counter++;
	}
	return counter;
};

void FileQueue::delayFile(std::string fullNameWoExt) {
	boost::mutex::scoped_lock lock(qMutex_);	// LOCK

	map<string, boost::shared_ptr<FileDescriptor> >::iterator it = queue_.find( fullNameWoExt );
	if( it != queue_.end() ) { // file was found in queue
		it->second->status_		= DELAYED;
		it->second->waitTill_	= second_clock::local_time() + minutes(delayWaitMinutes_);
	}
};

void FileQueue::processBadFile(std::string fullNameWoExt) {
	boost::mutex::scoped_lock lock(qMutex_);	// LOCK

	/*
	 * move file to BAD folder
	 */
	stringstream dstFax, dstInfo;
	dstFax << badDir_ << "/" << queue_[fullNameWoExt]->getShortFaxName();
	dstInfo << badDir_ << "/" << queue_[fullNameWoExt]->getShortInfoName();
	try{
		boost::filesystem::rename( (fullNameWoExt+faxExt_).c_str(), dstFax.str().c_str() );
	} catch (const boost::filesystem::filesystem_error& e) {
		LOG4CXX_ERROR(logger, "FileQueue::rescan() Can't remove file: " << fullNameWoExt + faxExt_ << " to BAD");
	}
	try{
		boost::filesystem::rename( (fullNameWoExt + infoExt_).c_str(), dstInfo.str().c_str() );
	} catch (const boost::filesystem::filesystem_error& e) {
		LOG4CXX_ERROR(logger, "FileQueue::rescan() Can't remove file: " << fullNameWoExt + infoExt_ << " to BAD");
	}
	/*
	 * erase from queue
	 */
	eraseFile(fullNameWoExt);
};

int FileQueue::rescan() {
	/*
	 * main directory scan circle
	 */
	fs::path dir(workingDir_.c_str());
	fs::directory_iterator endIter;

	if ( fs::exists(dir) && fs::is_directory(dir)) {

		/*
		 * walking through directory
		 */
		for( fs::directory_iterator dirIter(dir) ; dirIter != endIter ; ++dirIter) {
			/*
			 * if new entry file with the .info extension
			 */
			if ( fs::is_regular_file(dirIter->status()) ) {
				// in dirIter->path().filename() filename, without directory
				// in dirIter->path().directory_string() we have filename with directory
				// if can't find file from directory in queue, and it's extension is ".info"
				if(dirIter->path().extension() == infoExt_) {					// contain correct extension

					/*
					 * check if this file not in queue
					 */
					string fullNameWoExt(dirIter->path().directory_string(), 0, dirIter->path().directory_string().size()-infoExt_.size());
					//todo migration fro centos 6 to 7
					//string shortNameWoExt(dirIter->path().filename(), 0, dirIter->path().filename().size() - infoExt_.size());
					string shortNameWoExt(dirIter->path().filename().string(), 0, dirIter->path().filename().string().size() - infoExt_.size());

					if( queue_.find( fullNameWoExt ) == queue_.end() ) { // not found
						/*
						 * check for .tiff corresponding to .info is present in directory
						 */
						struct stat sb;
						if( stat((fullNameWoExt + faxExt_).c_str(), &sb) == -1 ) {
							LOG4CXX_ERROR(logger, "FileQueue::rescan() Cannot access file: " << fullNameWoExt + faxExt_);
							// move .info file to BADs
							try{
								boost::filesystem::rename( (fullNameWoExt+infoExt_).c_str(),
										(badDir_+"/"+shortNameWoExt+infoExt_).c_str() );
								LOG4CXX_INFO(logger, "BAD FILE " << fullNameWoExt + infoExt_ << "MOVE TO " << badDir_+"/");
							} catch (const boost::filesystem::filesystem_error& e) {
								//LOG4CXX_ERROR(logger, "FileQueue::rescan() Can't remove file: " << fullNameWoExt + infoExt_ << " to BAD");
								LOG4CXX_ERROR(logger, "FileQueue::rescan() Can't remove file: " <<
										fullNameWoExt + infoExt_ << " to BAD: " << badDir_+"/"+shortNameWoExt+infoExt_);
							}
						} else {
							LOG4CXX_TRACE(logger, "FileQueue::rescan() files: " <<
									fullNameWoExt << " " << faxExt_ << "|" << infoExt_ << " READY to queue");
							// add file to queue
							addFile(fullNameWoExt, shortNameWoExt + faxExt_, shortNameWoExt + infoExt_);
						}
					} else {
						LOG4CXX_TRACE(logger, "FileQueue::rescan() file: " << fullNameWoExt << " already in list");
					}
				}//END if() check extension
			}
		}
	}//END if

	return 0;
}

string FileQueue::toString() {
	stringstream ss;
	ss << "FileQueue \"" << queueName_ << "\" parameters:" << endl;
	ss << "\tfax extension:\t" << faxExt_ << endl;
	ss << "\tinfo extension:\t" << infoExt_ << endl;
	ss << "\tworking directory:\t" << workingDir_ << endl;
	ss << "\tbad files directory:\t" << badDir_ << endl;
	ss << "\tscan period (sec):\t" << scanPeriod_ << endl;
	ss << "\tdelay bad transmits period (min):\t" << delayWaitMinutes_ << endl;
	return ss.str();
};

void FileQueue::scanWorker() {
	// exit, if scanPeriod_ is not set
	if(scanPeriod_ == 0) return;

	LOG4CXX_DEBUG(logger, "FileQueue \"" << queueName_ << "\". Scan worker thread STARTED.");

	while (!scanThreadExit_) {
		rescan();
		sleep(scanPeriod_);
	}

	LOG4CXX_DEBUG(logger, "FileQueue \"" << queueName_ << "\". Scan worker thread STOPPED.");
};
