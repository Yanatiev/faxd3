/**
 * filequeue.h
 *
 *  Created on: Jun 25, 2014
 *      Author: yanat
 *
 *      Demand:
 *      project properties -> C/C++ General -> Paths and Symbols -> Symbols
 *      	(+) BOOST_THREAD_POSIX
 *
 *      project properties -> C/C++ General -> Paths and Symbols -> Libraries
 *			(+) log4cxx	(logger library)
 *			(+) pthread	(Posix threads for logger)
 *			(+) boost_filesystem (boost file operations)
 *			(+) boost_thread-mt (boost threads)
 */

#ifndef FILEQUEUE_H_
#define FILEQUEUE_H_

#include <string>
#include <log4cxx/logger.h>
#include <boost/shared_ptr.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/thread.hpp>
#include "basicconfig.h"

namespace faxd {
class FileQueue;

/**
* @brief possible statuses of file entry
*/
enum FileStatus {
	NEW,		//!< just added to file queue to send. Not yet processed
	SENDING,	//!< in process of sending (locked by sender thread)
	DELAYED		//!< sending was failed, sending snoozed till time in waitTill_
};

static const char * FileStatusStr[] = {"NEW", "SENDING", "DELAYED"};


/**
* @brief file descriptors for any files in transmit queue
*/
class FileDescriptor {
friend class FileQueue;
public:
	FileDescriptor(std::string shortFaxName, std::string shortInfoName);
	FileDescriptor(const FileDescriptor& fd);	// copy constructor
	std::string toString();
	std::string getShortFaxName() {return shortFaxName_;};
	std::string getShortInfoName() {return shortInfoName_;};

private:
	std::string					shortFaxName_;		//!< send file name xxxxx.tiff
	std::string					shortInfoName_;		//!< send file name xxxxx.info
	FileStatus					status_;		//!< current status
	boost::posix_time::ptime 	waitTill_;		//!< file transfer operation suspended to this time, cause to previous sending failed
};





/**
 * @brief file queue for files with their descriptors (AV pairs files) manipulation
 */
	class FileQueue {
	public:
		FileQueue(BasicConfig& cfg, std::string queueName);

		void test();

		//FileQueue();
		~FileQueue();

		/**
		 * @brief erase element in file queue and files n filesystem
		 * @details
		 * 		- usually it's used in case of file successfully sent
		 * 		- delete in inbound file queue both files (.info, .tiff). File extensions are defined in Config
		 * 		- delete file entry in file queue
		 * @param fullNameWoExt - key string, returned by getFile
		 */
		void eraseFile(std::string fullNameWoExt);

		/**
		 * @brief return key (filename with path, without extension), mark it in queue as SENDING
		 * @details
		 * 		- return file full name without extension <br/>
		 * 		- change element status in queue as SENDING <br/>
		 * 		- return empty string in case of empty queue <br/>
		 * 		- thread safe operation <br/>
		 * @return if there are files in queue return
		 */
		std::string getFile(boost::shared_ptr<FileDescriptor> & spfd);

		/**
		 * @brief get number of files with status NEW, or DELAYED with expired delay timer
		 * @return 0 or more
		 */
		int getNumFilesToSend();
		/**
		 * @brief delay transmission of file
		 * @details If receiver daemon not accessible, files freeze in transmission queue. Time of freeze defined in
		 * 	parameter cfg::delayWaitMinutes. Status of this file is set to DELAYED. Every call of getFile() checks if
		 * 	timeout expired, then file becomes active to transmit, i.e. status = SENDING
		 * @param fullNameWoExt - key string - name of the file (without extension) need to be DELAYED
		 */
		void delayFile(std::string fullNameWoExt);

		/**
		 * @brief move bad descriptor to bad queue
		 * @details BAD queue defined in cfg::senderBadDir
		 * @param fullNameWoExt - .info file moved to bad queue.
		 */
		void processBadFile(std::string fullNameWoExt);

		/**
		 * @brief	scan for a new files
		 * @details	scans for a new files, check them against queue_, push it to queue, if they are not exists. Thread safe
		 */
		int rescan();

		/**
		 * @return string representation of FileQueue configuration parameters
		 */
		std::string toString();

		std::string getFaxExt() {return faxExt_;};		//!< FAX image extension getter
		std::string getInfoExt() {return infoExt_;};	//!< FAX descriptor file extension getter

	private:
		BasicConfig& 		cfg_;		//!< config object with basic funtionality
		log4cxx::LoggerPtr	logger;		//!< logger

		std::map<std::string, boost::shared_ptr<FileDescriptor> > queue_;	//!< pair file name with path without extension (key), file descriptor(value)
		boost::mutex		qMutex_;	//!< for concurrent access to queue_ map

		std::string queueName_;		//!< each queue parameter is get with <queueName_>.<parameter> from config
		std::string faxExt_;		//!< FAX image extension ".tiff"
		std::string infoExt_;		//!< FAX info file extension ".info"
		std::string badDir_;		//!< directory for bad FAXes
		std::string workingDir_;	//!< directory with files, processed in sender queue
		int delayWaitMinutes_;		//!< delay for minutes in case of send failure
		int scanPeriod_;			//!< working directory rescan period in seconds

		volatile bool				scanThreadExit_;//!< flag to thread exit
		boost::thread				scanThread_;	//!< separate thread, which scan files folder periodically

		void addFile(std::string fullNameWoExt, std::string faxName, std::string infoName);	//!< adding new FileDescriptor entry to queue
		void scanWorker(); //!< @brief run in thread, scans file folder in period of scanPeriod_, exit, if scanThreadExit_ is true
	};
}


#endif /* FILEQUEUE_H_ */
