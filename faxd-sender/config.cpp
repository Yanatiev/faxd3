
#include <boost/property_tree/json_parser.hpp>
//#include <boost/optional.hpp>
#include <boost/foreach.hpp>
#include <exception>
#include <iostream>
#include <sstream>
#include <string>

#include "config.h"

using namespace faxd;
using namespace boost::property_tree;
using std::string;
using std::endl;

string Config::logfile;
string Config::postUrl;
int Config::httpSendersNum;
int Config::waitSecToComplete;
DeliverType Config::deliverType;
string Config::deliverGoeUrl;
string Config::deliverTifUrl;
//string Config::deliverGoeDir;
//string Config::deliverTifDir;
//string Config::deliverFtpUser;
//string Config::deliverFtpPassword;
boost::property_tree::ptree Config::pt;

bool Config::configure(std::string configFile, std::string section) {
	/*
	 * reading config from file
	 */
	std::stringstream ss;

	std::ifstream file( configFile.c_str() );
	if ( file ) {
			ss << file.rdbuf();
			file.close();
	} else {
		std::cout << "CAN'T OPEN CONFIG FILE: " << configFile <<". EXITING"<< std::endl;
		return false;
	}

	/*
	 * parse JSON formatted config file to config objects;
	 */
	 try {
		 boost::property_tree::read_json(ss, pt);

		 /*
		  * get global variables
		  */
		 logfile			= pt.get<std::string>(section.append(".logcfg").c_str(), "faxd-sender.log.cfg.xml");
		 httpSendersNum		= pt.get<int>(section.append( ".httpSendersNum").c_str(), 10);
		 waitSecToComplete	= pt.get<int>(section.append( ".waitSecToComplete").c_str(), 10);
		 /*
		 * parse "deliver" part of the configuration
		 */
		string typeStr			= pt.get<string>(section.append( ".type").c_str(), "bad");
		if(typeStr == CFG_SECTION_DELIVER_TYPE_GOE) {
			deliverType		= DELIVER_TYPE_GOE;
			deliverGoeUrl	= pt.get<string>(section.append( ".goeURL").c_str(), "<<<bad goe url>>>");
			deliverTifUrl	= pt.get<string>(section.append( ".goeTIFURL").c_str(), "<<<bad goe tif url>>>");
			//deliverGoeDir	= pt.get<string>(CFG_SECTION_DELIVER ".goeRemoteDir", "");
			//deliverTifDir	= pt.get<string>(CFG_SECTION_DELIVER ".tifRemoteDir", "");
			//deliverFtpUser	= pt.get<string>(CFG_SECTION_DELIVER ".ftpUser", "");
			//deliverFtpPassword	= pt.get<string>(CFG_SECTION_DELIVER ".ftpPassword", "");
		} else if (typeStr == CFG_SECTION_DELIVER_TYPE_VOXFAX){
			deliverType		= DELIVER_TYPE_VOXFAX;
			postUrl			= pt.get<std::string>(section.append( ".postUrl").c_str(), "http://127.0.0.1:8080/voxfax/voxfax");
		} else {
			throw ConfigException();
		}

	} catch (std::exception const& e) {
		std::cout << "JSON parse exception" << endl;
		std::cerr << e.what() << endl;
		return false;
	}
	return true;
};

std::string Config::toString() {
	std::stringstream ss;
	ss << endl;
	ss << "\t\t" << "logFile\t= " << logfile << endl;
	ss << "\t\t" << "postUrl\t= " << postUrl << endl;
	return ss.str();
};

Config::Config(std::string configFile, std::string section) throw (ConfigException) {
	if (configure(configFile, section) == false) {
		ConfigException ce;
		throw ce;
	}
};

int Config::getInt(std::string key, int defaultValue) {
	try {
		return (pt.get<int>(key, defaultValue));
	} catch (boost::property_tree::ptree_error const &err) {
		return defaultValue;
	}
};

std::string Config::getStr(std::string key, std::string defaultValue) {
	try {
		return (pt.get<string>(key, defaultValue));
	} catch (boost::property_tree::ptree_error const &err) {
		return defaultValue;
	}
};





