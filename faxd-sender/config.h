#ifndef CONFIG_H_
#define CONFIG_H_


#include <map>
#include <string>
#include <vector>
#include <exception>
#include <boost/property_tree/ptree.hpp>
#include "basicconfig.h"

//#define CFG_SECTION_DELIVER "deliver"
#define CFG_SECTION_DELIVER_TYPE_GOE "goe"
#define CFG_SECTION_DELIVER_TYPE_VOXFAX "voxfax"

namespace faxd {
	enum DeliverType { DELIVER_TYPE_GOE=0, DELIVER_TYPE_VOXFAX=1 };
/**
 * @class ConfigException config.h
 * @brief Exception which throwed in proccess of config file parsing
 */
	class ConfigException : public std::exception {
		virtual const char* what() const throw() {
			return "Config file parse exception";
		}
	};
/**
 * @class Config config.h
 * @brief Main container to store all program settings
 * @details All settings variables are static. They are initialized from JSON formatted file.
 * If file initialization failed, variables are set to their default values.
 *
 * Config file must have section "deliver"
 * Example:
 * ========================================================<br/>
 *"deliver" :
 *	{
 *		"logcfg"		:"/etc/faxd2/faxd-sender.log.cfg.xml",
 *		"type"			:"goe",
 *		"goeURL"		:"ftp://127.0.0.1/",
 *		"postUrl"		:"http://195.90.46.148:8080/VoxFAX/inboundfax",
 *		"goeRemoteDir"	:"x/",
 *		"tifRemoteDir"	:"y/",
 *		"ftpUser"		:"*******",
 *		"ftpPassword"	:"*******",
 *		"faxExt"		:".tif",
 *		"infoExt"		:".goe",
 *		"workingDir"	:"/var/spool/faxd2/in",
 *		"badDir"		:"/var/spool/faxd2/bad-http",
 *		"httpSendersNum":5,
 *		"delayWaitMinutes":4,
 *		"scanPeriod"	:5
 *	},
 * ========================================================<br/>
 *
 * Usage example:<br/>
 * ========================================================<br/>
 * #include "config.h"<br/>
 * int main() {<br/>
 * 		if( Config::configure("faxd-sender.config") == false )<br/>
 * 			exit(-1);<br/>
 * 		int x = cfg::sendersNum<br/>
 * }<br/>
 * ========================================================<br/>
 */
	class Config : public BasicConfig {
	public:
		static std::string logfile;			//!< logger configuration file
		static std::string postUrl;			//!< HTTP URL of "voxfax" application
		static int httpSendersNum;				//!< quantity of sender pool threads
		static int waitSecToComplete;		//!< number seconds for complete transfer before ERROR

		static DeliverType deliverType;		//!< Type of inbound file delivery. Placed in section "delivery", contains values: "goe" or "voxfax"
		static std::string deliverGoeUrl;
		static std::string deliverTifUrl;
		//static std::string deliverGoeDir;	//!< in case of "goe" type it contains remote ftp path for .goe files
		//static std::string deliverTifDir;	//!< in case of "goe" type it contains remote ftp BASE path for .tif files
		//static std::string deliverFtpUser;
		//static std::string deliverFtpPassword;

		/**
		 * @brief convert some valueable parameters to string
		 * @return string representation of read config
		 */
		static std::string toString();


		Config(std::string configFile, std::string section) throw (ConfigException);
		/**
		 * @brief Reads config file
		 * @details Operations:
		 * 		- reads file if it present;
		 * 		- parses it with JSON parser;
		 * 		- extracts all global settings.
		 * @param configFile configuration file name (full name with path preferable).
		 * @return if file is found and successfy JSON parser: normally parsed, true value returns, elsewhere false.
		 * In case of false, content of configured variable may be unpredictable. So best behavior - exiting from program.
		 */
		static bool configure(std::string configFile, std::string section);

		/*
		 * implement interfaces from BasicConfig
		 */
		int getInt(std::string key, int defaultValue);
		std::string getStr(std::string key, std::string defaultValue);

	private:
		static boost::property_tree::ptree pt;	//!< propety tree for storing options from config file
	};

	typedef Config cfg;
}


#endif /* CONFIG_H_ */
