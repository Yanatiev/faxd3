#include <iostream>
#include "daemon.h"
#include "filequeue.h"
#include "config.h"
#include "senderspool.h"
#include <log4cxx/logger.h>
#include <log4cxx/xml/domconfigurator.h>
#include <boost/program_options.hpp>
// for polling stdin
#include <stdio.h>
#include <sys/poll.h>

using namespace log4cxx;
using namespace log4cxx::xml;
using namespace log4cxx::helpers;
using namespace faxd;
using std::cout;
using std::endl;

using std::string;

#define         RC_OK               0
#define         RC_ERROR            -1
#define         RC_PID_EXIST    	-2

int main(int argc, char** argv) {
	string cfg;
	string pidfile;//("/var/run/faxd-sender.pid");
	string section;

	/*
	 * read program options. Variants are:
	 * faxd-sender -c config.file -p pidfile -s hookup_section_name
	 * faxd-sender --config config.file --pidfile pidfile --section hookup_section_name
	 * --config - configuration file name
	 * --section - configuration file section name with delivery daemon configurution in common config or hookup file
	 * --pidfile - name of the pid file in /var/run
	 * --help - option output
	 */
	try {
		/*
     * Define and parse the program options
     */
		namespace po = boost::program_options;
		po::options_description desc("Options");
		desc.add_options()
			("help,h", "Print help messages")
			("config,c", po::value<std::string>(&cfg)->required(), "configuration file name. Mandatory")
			("section,s", po::value<std::string>(&section)->required(), "configuration section name or \"hookup\". Mandatory")
			("pidfile,p", po::value<std::string>(&pidfile)->required(), "pid file name, which will be created in /var/run. Mandatory");

		po::variables_map vm;
		try {
			po::store(po::parse_command_line(argc, argv, desc), vm); // can throw
			// print help in case of help was entered
			if ( vm.count("help")  ) {
				std::cout << desc << std::endl;
				return RC_OK;
			}
			// set real variables and fill vm structures throws on error, so do after help in case there are any problems
			po::notify(vm);

		} catch(po::error& e) {
			std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
			std::cerr << desc << std::endl;
			return RC_ERROR;
		}
	} catch(std::exception& e) {
		std::cerr << "Unhandled Exception reached the top of main: " << e.what() << ", application will now exit" << std::endl;
		return RC_ERROR;
	}

	try {
		faxd::Daemon(cfg, section, pidfile);
	} catch (const DaemonException& de) {
		return -1;
	}

	return 0;

}
