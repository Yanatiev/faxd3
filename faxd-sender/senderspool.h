/*
 * senderspool.h
 *
 *  Created on: Apr 14, 2014
 *      Author: root
 */

#ifndef SENDERSPOOL_H_
#define SENDERSPOOL_H_

#include <vector>
#include <boost/thread/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <log4cxx/logger.h>
#include "filequeue.h"
#include "config.h"

namespace faxd {

/**
 * @brief pool of workers thread. Implement HTTP send logic
 * @details
 */
	class SendersPool {
	public:
		/**
		 * @brief runs all threads.
		 * @param fq file queue object
		 */
		SendersPool(FileQueue& fq);

		/**
		 * @brief destroys all worker threads, waits termination all of them
		 */
		~SendersPool();

		/**
		 * @return string representation of SendersPool config
		 */
		std::string toString();
	private:
		FileQueue&	fq_;			//!< File Queue reference
		log4cxx::LoggerPtr	logger;	//!< local logger object

		volatile bool isExit;		//!< EXIT flag for thread

		std::vector< boost::shared_ptr<boost::thread> > spWorkers;
		/**
		 * @brief worker method
		 * @details runs in separate thread, in loop polled and sent files collected from queue
		 * @param index - simple local ID of thread, Consequently numbered.
		 */
		void worker(int index);
		void workerGoe(int index);

	};
}



#endif /* SENDERSPOOL_H_ */
