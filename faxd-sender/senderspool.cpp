#include <boost/bind.hpp>
#include <boost/date_time.hpp>
#include <curl/curl.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/algorithm/string.hpp>
#include <exception>
#include <iostream>
#include <sstream>
#include <string>
#include <map>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>

#include "senderspool.h"

using namespace faxd;
using namespace std;
using namespace log4cxx;
using namespace boost::posix_time;
using std::string;
using std::endl;

SendersPool::SendersPool(FileQueue& fq) :
	fq_(fq),
	logger(Logger::getLogger( "main")),
	isExit(false)
{
	/*
	 * CURL global initializtion
	 */
	curl_global_init(CURL_GLOBAL_ALL);

	/*
	 * run run sender thread
	 */
	for(int i=0; i < cfg::httpSendersNum; i++) {
		switch (cfg::deliverType) {
		case DELIVER_TYPE_VOXFAX:
			{
				boost::shared_ptr<boost::thread> sp( new boost::thread(boost::bind(&SendersPool::worker, this, i)) );
				spWorkers.push_back(sp);
				break;
			}
		case DELIVER_TYPE_GOE:
		default:
			{
				boost::shared_ptr<boost::thread> sp( new boost::thread(boost::bind(&SendersPool::workerGoe, this, i)) );
				spWorkers.push_back(sp);
				break;
			}
		}
//		if (cfg::deliverType == DELIVER_TYPE_GOE) {
//			boost::shared_ptr<boost::thread> sp( new boost::thread(boost::bind(&SendersPool::workerGoe, this, i)) );
//			spWorkers.push_back(sp);
//			break;
//		} else if (cfg::deliverType == DELIVER_TYPE_VOXFAX) {
//			boost::shared_ptr<boost::thread> sp( new boost::thread(boost::bind(&SendersPool::worker, this, i)) );
//			spWorkers.push_back(sp);
//			break;
//		}
	}
};

SendersPool::~SendersPool() {
	/*
	 * set EXIT flag to thread
	 */
	isExit = true;

	/*
	 * "Soft" terminating most of threads in 100ms loop per each with total
	 * time to wait all threads defined in config
	 */
	int threadTimeout	= 100;	// timeout to wait 1 thread to join in milliseconds
	boost::posix_time::time_duration timeout = boost::posix_time::milliseconds(threadTimeout);

	// stop time for thread waiting
	ptime stopTime = second_clock::local_time() + seconds(cfg::waitSecToComplete);

	vector< boost::shared_ptr<boost::thread> >::iterator it;
	while( (spWorkers.size()!=0) || (second_clock::local_time() < stopTime) ) {
		it = spWorkers.begin();
		while(it != spWorkers.end()) {
			if( (*it)->timed_join(timeout) )// join success
				spWorkers.erase(it);		// delete from vector
			else
				it++;						// get next thread
		}
	}

	/*
	 * terminating other threads
	 */
//	for(it=spWorkers.begin(); it!=spWorkers.end(); ++it) {
//		//TODO correct thread interruption
//		// http://stackoverflow.com/questions/12437395/a-simple-example-of-boost-multithreading
//		// http://stackoverflow.com/questions/7316633/boost-thread-how-to-acknowledge-interrupt
//	}

	/*
	 * cleanup CURL
	 */
	curl_global_cleanup();
	LOG4CXX_DEBUG(logger, "SendersPool::~SendersPool() all threads exits CORRECT");
};

void SendersPool::worker(int index) {
	/*
	 * main loop, while global exit flag is not set
	 */
	boost::shared_ptr<FileDescriptor> spfd;
	string fullNameWoExt;
	while(!isExit) {
		sleep(2);

		/*
		 * try to get new file to send
		 */
		fullNameWoExt = fq_.getFile(spfd);
		if(fullNameWoExt.empty()) {	// there was no new files,
			LOG4CXX_TRACE(logger, "SendersPool::worker() " << index << " nothing to send");
			continue;				// continue main loop - nothing to send
		}

		/*
		 * using curl construct request and fill it with properties (ini file)
		 */

		LOG4CXX_TRACE(logger, "SendersPool::worker() " << index << " trying to send " << spfd->getShortFaxName());
		struct curl_httppost *formpost=NULL;
		struct curl_httppost *lastptr=NULL;

		// add file (FileQueue element key)
		curl_formadd(&formpost,
			&lastptr,
			CURLFORM_COPYNAME, "sendfile",
			CURLFORM_FILE, ( fullNameWoExt + fq_.getFaxExt()).c_str(),//cfg::faxExt).c_str(),
			CURLFORM_END);
		// add filename (FileQueue element short name)
		curl_formadd(&formpost,
			&lastptr,
			CURLFORM_COPYNAME, "filename",
			CURLFORM_COPYCONTENTS, spfd->getShortFaxName().c_str(),
			CURLFORM_END);

		/*
		 * all other attributes we directly takes from .info file
		 */
		// read property file
		std::stringstream ss;
		std::ifstream file( (fullNameWoExt + fq_.getInfoExt()).c_str() );//cfg::infoExt).c_str() );
		if ( file ) {
			ss << file.rdbuf();
			file.close();
		}

		try {
			boost::property_tree::ptree pt;
			read_ini(ss, pt);	// parsing string representation of .info file to boost property tree

			boost::property_tree::ptree::iterator it;
			for(it = pt.begin(); it != pt.end(); ++it) { // walking through all properties and add them to curl post request
				curl_formadd(&formpost,
					&lastptr,
					CURLFORM_COPYNAME, it->first.c_str(),
					CURLFORM_COPYCONTENTS, pt.get<string>(it->first).c_str(),
					CURLFORM_END);
			}

		} catch (std::exception const& e) {
			LOG4CXX_ERROR(logger, "SendersPool::worker() .info file: " << fullNameWoExt + fq_.getInfoExt() << " ERROR: " << e.what());
			// move file to bad
			fq_.processBadFile(fullNameWoExt);
			continue;
		}

		/*
		 * send proccess
		 */
		CURL *curl;
		CURLcode res;
		struct curl_slist *headerlist=NULL;
		static const char buf[] = "Expect:";

		curl = curl_easy_init();
		/* initalize custom header list (stating that Expect: 100-continue is not wanted */
		headerlist = curl_slist_append(headerlist, buf);
		if(curl) {
			/* what URL that receives this POST */
			curl_easy_setopt(curl, CURLOPT_URL, cfg::postUrl.c_str());

			/* only disable 100-continue header if explicitly requested */
			curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
			curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);

			/* Perform the request, res will get the return code */
			res = curl_easy_perform(curl);

			/* always cleanup */
			curl_easy_cleanup(curl);
			/* then cleanup the formpost chain */
			curl_formfree(formpost);
			/* free slist */
			curl_slist_free_all (headerlist);

			/* Check for errors */
			if(res != CURLE_OK) {
				LOG4CXX_ERROR(logger, "SendersPool::worker() thread " << index << " sent " << spfd->getShortFaxName() << " ERROR. File delayed");
				fq_.delayFile(fullNameWoExt);
			} else {
				LOG4CXX_DEBUG(logger, "SendersPool::worker() thread " << index << " sent " << spfd->getShortFaxName() << " OK");
				fq_.eraseFile(fullNameWoExt);	// erase sent files from queue and file systems
			}
		}
	}
	LOG4CXX_DEBUG(logger, "SendersPool::worker() " << index << " EXIT signal received");
}


void SendersPool::workerGoe(int index) {
	/*
	 * main loop, while global exit flag is not set
	 */
	boost::shared_ptr<FileDescriptor> spfd;
	string fullNameWoExt;
	while(!isExit) {
		sleep(2);

		/*
		 * try to get new file to send
		 */
		fullNameWoExt = fq_.getFile(spfd);
		if(fullNameWoExt.empty()) {	// there was no new files,
			LOG4CXX_TRACE(logger, "SendersPool::workerGoe() " << index << " nothing to send");
			continue;				// continue main loop - nothing to send
		}

		/*
		 * using curl construct request and fill it with properties (ini file)
		 */

		// 20150804174015808_0CR2M6J28R23S4.goe
		// 2015    08     04    17    40  15   808     _ 0CR2M6J28R23S4      .goe
		// <year><month><day><loc hh><mm><sec><msec ?> _ <[bodypart_0]:Token>.goe
		string localGoe = fullNameWoExt + fq_.getInfoExt();//cfg::infoExt;
		string localTif = fullNameWoExt + fq_.getFaxExt();//cfg::faxExt;
		string remoteFtpGoe = cfg::deliverGoeUrl + fullNameWoExt.substr(fullNameWoExt.find_last_of("/")+1) + fq_.getInfoExt();
		boost::algorithm::trim_left(remoteFtpGoe);	// remove whitespaces from left

		string remoteFtpTif;
		stringstream ss;
		ss << cfg::deliverTifUrl;
		for(unsigned i = fullNameWoExt.find_last_of("_")+1; i< fullNameWoExt.length(); i++) {
			ss << fullNameWoExt[i] << "/";
		}
		ss << fullNameWoExt.substr(fullNameWoExt.find_last_of("_")+1);
		remoteFtpTif = ss.str()+ fq_.getFaxExt();
		boost::algorithm::trim_left(remoteFtpTif);	// remove whitespaces from left


		CURL *curl;
		CURLcode res;
		FILE *hd_src;
		struct stat file_info;
		curl_off_t fsize;


		/*
		* Send tif
		*/
		if(stat(localTif.c_str(), &file_info)) {
			LOG4CXX_DEBUG(logger, "SendersPool::worker() Couldn't open:" << localTif);
			fq_.delayFile(fullNameWoExt);
			continue;
		} else {
			LOG4CXX_DEBUG(logger, "SendersPool::workerGoe() " << index << " trying to send " << localTif );
		}
		fsize = (curl_off_t)file_info.st_size;

		/* get a FILE * of the same file */
		hd_src = fopen(localTif.c_str(), "rb");

		/* get a curl handle */
		curl = curl_easy_init();
		if(curl) {
			curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
			curl_easy_setopt(curl,CURLOPT_URL, remoteFtpTif.c_str());

			// choose proto based on URL recognition
			if ( remoteFtpTif.substr(0, 4) == "sftp" ) {
				curl_easy_setopt(curl,CURLOPT_PROTOCOLS, CURLPROTO_SFTP);
			} else if  ( remoteFtpTif.substr(0, 3) == "ftp" ) {
				curl_easy_setopt(curl,CURLOPT_PROTOCOLS, CURLPROTO_FTP);
			} else {
				LOG4CXX_ERROR(logger, "SendersPool::worker() " << "send protocol is not recognized: " << remoteFtpTif);
				fq_.delayFile(fullNameWoExt);
				continue;
			}

			curl_easy_setopt(curl,CURLOPT_FTP_CREATE_MISSING_DIRS, 1L );
			curl_easy_setopt(curl, CURLOPT_READDATA, hd_src);
			curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE, (curl_off_t)fsize);
			curl_easy_setopt(curl, CURLOPT_TIMEOUT, cfg::waitSecToComplete);
			//curl_easy_setopt(curl, CURLOPT_USERPWD, (cfg::deliverFtpUser + ":" + cfg::deliverFtpPassword).c_str());
			res = curl_easy_perform(curl);
			if(res != CURLE_OK) {
				LOG4CXX_ERROR(logger, "SendersPool::worker() " << localTif << " " << curl_easy_strerror(res) << " url:" + remoteFtpTif);
				fq_.delayFile(fullNameWoExt);
				continue;
			} else
				LOG4CXX_INFO(logger, "SendersPool::worker() " << localTif << " OK");

			curl_easy_cleanup(curl);
		}
		fclose(hd_src);

		/*
		 * send Goe
		 */

		/* get the file size of the local file */
		if(stat(localGoe.c_str(), &file_info)) {
			LOG4CXX_ERROR(logger, "SendersPool::worker() Couldn't open:" << localGoe);
			fq_.delayFile(fullNameWoExt);
			continue;
		} else {
			LOG4CXX_DEBUG(logger, "SendersPool::workerGoe() " << index << " trying to send " << localGoe );
		}
		fsize = (curl_off_t)file_info.st_size;

		/* get a FILE * of the same file */
		hd_src = fopen(localGoe.c_str(), "rb");

		/* get a curl handle */
		curl = curl_easy_init();
		if(curl) {
			curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
			curl_easy_setopt(curl,CURLOPT_URL, remoteFtpGoe.c_str());

			// choose proto based on URL recognition
			if ( remoteFtpGoe.substr(0, 4) == "sftp" ) {
				curl_easy_setopt(curl,CURLOPT_PROTOCOLS, CURLPROTO_SFTP);
			} else if  ( remoteFtpGoe.substr(0, 3) == "ftp" ) {
				curl_easy_setopt(curl,CURLOPT_PROTOCOLS, CURLPROTO_FTP);
			} else {
				LOG4CXX_ERROR(logger, "SendersPool::worker() " << "send protocol is not recognized: " << remoteFtpGoe);
				fq_.delayFile(fullNameWoExt);
				continue;
			}

			curl_easy_setopt(curl,CURLOPT_FTP_CREATE_MISSING_DIRS, 1L );
			curl_easy_setopt(curl, CURLOPT_READDATA, hd_src);
			curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE, (curl_off_t)fsize);
			curl_easy_setopt(curl, CURLOPT_TIMEOUT, cfg::waitSecToComplete);
			//curl_easy_setopt(curl, CURLOPT_USERPWD, (cfg::deliverFtpUser + ":" + cfg::deliverFtpPassword).c_str());
			res = curl_easy_perform(curl);
			if(res != CURLE_OK) {
				LOG4CXX_ERROR(logger, "SendersPool::worker() " << localGoe << " " << curl_easy_strerror(res));
				fq_.delayFile(fullNameWoExt);
				continue;
			} else
				LOG4CXX_INFO(logger, "SendersPool::worker() " << localGoe << " OK");

			curl_easy_cleanup(curl);
		}
		fclose(hd_src);

		fq_.eraseFile(fullNameWoExt);
	}
	LOG4CXX_DEBUG(logger, "SendersPool::worker() " << index << " EXIT signal received");
}

string SendersPool::toString() {
	stringstream ss;
	switch (cfg::deliverType) {
	case DELIVER_TYPE_VOXFAX:
		ss << "Deliver incoming FAXes type: VOXFAX" << endl;
		ss << "\tVOXFAX URL\t: " << cfg::postUrl << endl;
		break;
	case DELIVER_TYPE_GOE:
	default:
		ss << "Deliver incoming FAXes type: GOE" << endl;
		ss << "\tGoe info file (S)FTP base URL\t: " << cfg::deliverGoeUrl << endl;
		ss << "\tGoe tiff file (S)FTP base URL\t: " << cfg::deliverTifUrl << endl;
		//ss << "\tGoe remote DIR\t: " << cfg::deliverGoeDir << endl;
		//ss << "\tFAX remote DIR\t: " << cfg::deliverTifDir << endl;
	}
	ss << "\tLOG file\t: " << cfg::httpSendersNum << endl;
	ss << "\tnumber of sender threads:\t" << cfg::httpSendersNum << endl;
	ss << "\twait seconds to complete net operation before skip:\t" << cfg::waitSecToComplete << endl;
	return ss.str();
}
